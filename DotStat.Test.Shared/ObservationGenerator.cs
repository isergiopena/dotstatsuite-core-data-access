﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using DotStat.Db;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test
{
    public sealed class GenerationSettings
    {
        /// <summary>
        /// If set to true series dimension order will not match order of dimensions in DSD
        /// </summary>
        public bool MixDimensionOrder = false;
        /// <summary>
        /// if set to positive number N, will inject invalid observation every N time
        /// </summary>
        public int InvalidObservationRepeat = 0;
        /// <summary>
        /// When InvalidObservationRepeat > 0, If set to positive number N, will skip N records before injecting erroneous observations
        /// </summary>
        public int InvalidObservationOffset = 0;

        public int RandomizeSeed    = 0;
        public int RandomizeOffset  = 0;
    }

    public static class ObservationGenerator
    {
        public static async IAsyncEnumerable<ObservationRow> Generate(
            Dsd dsd,
            Dataflow dataFlow,
            bool isDataImport,
            int startYear = 2000,
            int endYear = 2000,
            int max = -1,
            GenerationSettings settings = null,
            bool allowNullValues = false,
            StagingRowActionEnum? action = null,
            bool includeWildCardDimensions = false,
            int batchNumber = 1,
            [EnumeratorCancellation] CancellationToken cancellationToken = default)
        {
            settings ??= new GenerationSettings();

            var nonTimeDims = dsd.Dimensions.Where(d => !d.Base.TimeDimension).ToArray();
            var hasTimeDim = dsd.Dimensions.Any(d => d.Base.TimeDimension);

            var dims = nonTimeDims
                .Select(d => new KeyValuePair<Dimension, IEnumerator<string>>(d, GetDimensionEnumerator(d)))
                .ToArray();

            if (!hasTimeDim)
            {
                endYear = startYear;
            }

            // ------------------------------------------------------------
            var random = new Random(settings.RandomizeSeed);
            var actionRandom = new Random(DateTime.Now.Millisecond);
            string nextRandomShiftedDim = null;
            // ------------------------------------------------------------

            var count = 0;
            var stop = false;
            var seriesKey = new IKeyValue[dims.Length];
            var allowedActionValues = new List<StagingRowActionEnum> { StagingRowActionEnum.Merge, StagingRowActionEnum.Delete, StagingRowActionEnum.DeleteAll };

            while (!stop)
            {
                var read = count > 0;
                var i = 0;

                action ??= allowedActionValues[actionRandom.Next(0, allowedActionValues.Count - 1)];

                if (settings.RandomizeOffset > 0 && count % ((endYear - startYear + 1) * settings.RandomizeOffset) == 0)
                {
                    nextRandomShiftedDim = dims[random.Next(dims.Length - 2)].Key.Code;
                }

                var dimCount = 0;
                var hasWildCardedDimensions = false;
                foreach (var (dim, codes) in dims)
                {
                    if (count == 0 && !codes.MoveNext())
                    {
                        stop = true;
                        break;
                    }

                    if (read && dim.Code == (nextRandomShiftedDim ?? dim.Code))
                    {
                        nextRandomShiftedDim = null;

                        if (codes.MoveNext())
                        {
                            read = false;
                        }
                        else
                        {
                            codes.Reset();
                            codes.MoveNext();
                        }
                    }

                    var code = codes.Current;
                    if (includeWildCardDimensions)
                    {
                        var number = action == StagingRowActionEnum.Delete ? random.Next(1, 101) : 0;
                        code = number > 90 ? "*" : codes.Current;
                        if (number > 90)
                            hasWildCardedDimensions = true;
                    }

                    if (!isDataImport)
                    {
                        seriesKey[i++] = new KeyValueImpl(count < dimCount ? DbExtensions.DimensionSwitchedOff : code, dim.Code);
                        dimCount++;
                    }
                    else
                    {
                        seriesKey[i++] = new KeyValueImpl(code, dim.Code);
                    }
                }

                for (var year = startYear; year <= endYear; year++)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    count++;

                    if (read || max > 0 && count > max)
                    {
                        stop = true;
                        break;
                    }

                    var returnError = count > settings.InvalidObservationOffset
                                && settings.InvalidObservationRepeat > 0
                                && (count - settings.InvalidObservationOffset) % settings.InvalidObservationRepeat == 0;

                    IList<IKeyValue> metadataAttributes = null;
                    if (!isDataImport)
                    {
                        metadataAttributes = GenerateMetadataAttributeValues(dsd?.Msd?.MetadataAttributes);
                    }

                    var attributes = GenerateAttributeValues(dsd.Attributes.Where(x =>
                            x.Base.AttachmentLevel != AttributeAttachmentLevel.DataSet), allowNullValues);

                    var timeDimValue = year.ToString();
                    if (includeWildCardDimensions)
                    {
                        var number = action == StagingRowActionEnum.Delete ? random.Next(1, 101) : 0;
                        timeDimValue = number > 90 ? null : hasTimeDim ? year.ToString() : "*";
                        if (number > 90 && hasTimeDim)
                            hasWildCardedDimensions = true;
                    }

                    yield return isDataImport
                        ? new ObservationRow(
                            batchNumber,
                            action.Value,
                            new ObservationImpl(new KeyableImpl(dataFlow?.Base, dsd.Base, seriesKey, null),
                            timeDimValue,
                            returnError ? "error" : count.ToString(),
                            returnError ? null : attributes,
                            crossSectionValue: null),
                            hasWildCardedDimensions)
                        : new ObservationRow(
                            batchNumber,
                            action.Value,
                            new ObservationImpl(new KeyableImpl(dataFlow?.Base, dsd.Base, seriesKey, null),
                            timeDimValue,
                            null,
                            metadataAttributes,
                            crossSectionValue: null),
                            hasWildCardedDimensions);
                }
            }
        }

        //public static async IAsyncEnumerable<ObservationRow> Generate(
        //    Dsd dataflow, 
        //    bool isDataImport, 
        //    int startYear=2000, 
        //    int endYear=2000, 
        //    int max = -1, 
        //    GenerationSettings settings = null, 
        //    bool allowNullValues= false,
        //    StagingRowActionEnum? action = null,
        //    bool includeWildCardDimensions = false,
        //    int batchNumber = 1,
        //    [EnumeratorCancellation] CancellationToken cancellationToken = default)
        //{
        //    settings ??= new GenerationSettings();
            
        //    var nonTimeDims = dataflow.Dimensions.Where(d => !d.Base.TimeDimension).ToArray();
        //    var hasTimeDim  = dataflow.Dimensions.Any(d => d.Base.TimeDimension);
            
        //    var dims = nonTimeDims
        //        .Select(d => new KeyValuePair<Dimension, IEnumerator<string>>(d, GetDimensionEnumerator(d)))
        //        .ToArray();

        //    if (!hasTimeDim)
        //    {
        //        endYear = startYear;
        //    }
            
        //    // ------------------------------------------------------------
        //    var random          = new Random(settings.RandomizeSeed);
        //    var actionRandom = new Random(DateTime.Now.Millisecond);
        //    string nextRandomShiftedDim  = null;
        //    // ------------------------------------------------------------

        //    var count = 0;
        //    var stop = false;
        //    var seriesKey = new IKeyValue[dims.Length];
        //    var allowedActionValues = new List<StagingRowActionEnum> { StagingRowActionEnum.Merge, StagingRowActionEnum.Delete, StagingRowActionEnum.DeleteAll };

        //    while (!stop)
        //    {
        //        var read = count > 0;
        //        var i = 0;

        //        action ??= allowedActionValues[actionRandom.Next(0, allowedActionValues.Count - 1)];

        //        if (settings.RandomizeOffset > 0 && count % ((endYear - startYear + 1) * settings.RandomizeOffset) == 0)
        //        {
        //            nextRandomShiftedDim = dims[random.Next(dims.Length - 2)].Key.Code;
        //        }

        //        var dimCount = 0;
        //        var hasWildCardedDimensions = false;
        //        foreach (var (dim, codes) in dims)
        //        {
        //            if (count == 0 && !codes.MoveNext())
        //            {
        //                stop = true;
        //                break;
        //            }

        //            if (read && dim.Code == (nextRandomShiftedDim ?? dim.Code))
        //            {
        //                nextRandomShiftedDim = null;

        //                if (codes.MoveNext())
        //                {
        //                    read = false;
        //                }
        //                else
        //                {
        //                    codes.Reset();
        //                    codes.MoveNext();
        //                }
        //            }

        //            var code = codes.Current;
        //            if (includeWildCardDimensions)
        //            {
        //                var number = action == StagingRowActionEnum.Delete ? random.Next(1, 101) : 0;
        //                code = number > 90 ? "*" : codes.Current;
        //                if(number > 90) 
        //                    hasWildCardedDimensions = true;
        //            }

        //            if (!isDataImport)
        //            {
        //                seriesKey[i++] = new KeyValueImpl(count < dimCount ? DbExtensions.DimensionSwitchedOff : code, dim.Code);
        //                dimCount++;
        //            }
        //            else
        //            {
        //                seriesKey[i++] = new KeyValueImpl(code, dim.Code);
        //            }
        //        }

        //        for (var year = startYear; year <= endYear; year++)
        //        {
        //            cancellationToken.ThrowIfCancellationRequested();
        //            count++;

        //            if (read || max > 0 && count > max)
        //            {
        //                stop = true;
        //                break;
        //            }

        //            var returnError = count > settings.InvalidObservationOffset
        //                        && settings.InvalidObservationRepeat > 0
        //                        && (count - settings.InvalidObservationOffset) % settings.InvalidObservationRepeat == 0;
                    
        //            IList<IKeyValue> metadataAttributes = null;
        //            if (!isDataImport)
        //            {
        //                metadataAttributes = GenerateMetadataAttributeValues(dataflow.Dsd?.Msd?.MetadataAttributes);
        //            }

        //            var attributes = GenerateAttributeValues(dataflow.Dsd.Attributes.Where(x =>
        //                    x.Base.AttachmentLevel != AttributeAttachmentLevel.DataSet), allowNullValues);

        //            var timeDimValue = year.ToString();
        //            if (includeWildCardDimensions)
        //            {
        //                var number = action == StagingRowActionEnum.Delete ? random.Next(1, 101) : 0;
        //                timeDimValue = number > 90 ? null : hasTimeDim ? year.ToString() : "*";
        //                if (number > 90 && hasTimeDim)
        //                    hasWildCardedDimensions = true;
        //            }

        //            yield return isDataImport
        //                ? new ObservationRow(
        //                    batchNumber,
        //                    action.Value, 
        //                    new ObservationImpl(new KeyableImpl(dataflow.Base, dataflow.Dsd.Base, seriesKey, null),
        //                    timeDimValue,
        //                    returnError ? "error" : count.ToString(),
        //                    returnError ? null : attributes,
        //                    crossSectionValue: null),
        //                    hasWildCardedDimensions)
        //                : new ObservationRow(
        //                    batchNumber,
        //                    action.Value, 
        //                    new ObservationImpl(new KeyableImpl(dataflow.Base, dataflow.Dsd.Base, seriesKey, null),
        //                    timeDimValue,
        //                    null,
        //                    metadataAttributes,
        //                    crossSectionValue: null),
        //                    hasWildCardedDimensions);
        //        }
        //    }
        //}

        private static IList<IKeyValue> GenerateAttributeValues(IEnumerable<Domain.Attribute> attrs, bool nullValues)
        {
            return new List<IKeyValue>(attrs.Select(attr => new KeyValueImpl(
                nullValues ? "" : attr.Codelist?.Codes.FirstOrDefault(c =>  attr.IsAllowed(c.Code))?.Code ?? $"{attr.Code} - {attr.Base.AttachmentLevel} - text",
                attr.Code
            )));
        }

        private static IList<IKeyValue> GenerateMetadataAttributeValues(IEnumerable<MetadataAttribute> attrs)
        {
            return new List<IKeyValue>(attrs.Select(attr => new KeyValueImpl(attr.Codelist?.Codes.FirstOrDefault()?.Code ?? $"{attr.Code}  - text", attr.Code)));
        }

        private static IEnumerator<string> GetDimensionEnumerator(Dimension dim)
        {
            return dim.Codelist != null
                ? dim.Codelist.Codes.Where(c => dim.IsAllowed(c.Code)).Select(c=>c.Code).ToList().GetEnumerator()
                : new RandomStringEnumerator();
        }

        private class RandomStringEnumerator : IEnumerator<string>
        {
            private static readonly Random Random = new();
            private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            
            private IEnumerable<char> RandomString(int length)
            {
                for(var i = 0; i < length; i++)
                    yield return Chars[Random.Next(Chars.Length)];
            }

            public bool MoveNext()
            {
                Current = new string(RandomString(10).ToArray());
                return true;
            }

            public void Reset()
            {}

            public string Current { get; private set; }

            object IEnumerator.Current => Current;

            public void Dispose()
            {}
        }
    }
}
