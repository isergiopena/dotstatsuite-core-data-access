﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using DotStat.Domain;
using DotStat.MappingStore;
using Estat.Sri.Mapping.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util.Io;

namespace DotStat.Test.Moq
{
    public class TestMappingStoreDataAccess : IMappingStoreDataAccess
    {
        private Dataflow _dataflow;
        private Dsd _dsd;
        private ISdmxObjectRetrievalManager _objectRetrieval;
        private readonly string _structurePath;

        public TestMappingStoreDataAccess(string structurePath = "sdmx/264D_264_SALDI+2.1.xml")
        {
            _structurePath = structurePath;
        }

        public ISdmxObjects GetSdmxObjects(string filename)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            IStructureParsingManager structureParsingManager = new StructureParsingManager();
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filename))
            {
                sdmxObjects.Merge(structureParsingManager.ParseStructures(dataLocation).GetStructureObjects(false));
            }
            return sdmxObjects;
        }

        public Dataflow GetDataflow(string dataSpace = null, string agencyId = null, string dataflowId = null, string version = null, bool throwErrorIfNotFound = true, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies, bool useCache = true)
        {
            if (useCache && _dataflow != null)
            {
                return _dataflow;
            }

            var sdmxObjects = this.GetSdmxObjects(_structurePath);
            
            _dataflow = SdmxParser.ParseDataFlowStructure(sdmxObjects);

            if (_dataflow.Dsd.Msd==null)
                SetDsdMsd(dataSpace, _dataflow.Dsd);

            return _dataflow;
        }

        public Dsd GetDsd(string dataSpace = null, string agencyId = null, string dsdId = null, string version = null, bool throwErrorIfNotFound = true, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies, bool useCache = true)
        {
            if (useCache && _dsd != null)
            {
                return _dsd;
            }

            var sdmxObjects = this.GetSdmxObjects(_structurePath);

            _dsd = SdmxParser.ParseDsdStructure(sdmxObjects);

            if (_dsd.Msd == null)
                SetDsdMsd(dataSpace, _dsd);

            return _dsd;
        }
        public ISdmxObjectRetrievalManager GetRetrievalManager(string dataSpace = null)
        {
            return _objectRetrieval = _objectRetrieval ?? new InMemoryRetrievalManager(this.GetSdmxObjects(_structurePath));
        }

        public void DeleteAllActualContentConstraints(string dataSpace, Dataflow dataflow)
        {}

        public void DeleteActualContentConstraint(string dataSpace, string agencyId, string id, string version, char targetTable, TargetVersion targetVersion)
        {}

        public void SaveContentConstraint(string dataSpace, Dataflow dataflow, IList<IKeyValuesMutable> cubeRegion, bool? includedCubeRegion, char targetTable, TargetVersion targetVersion, DateTime liveAccStartDate, long? obsCount = null)
        {}

        public void UpdateValidityOfContentConstraints(string dataSpace, Dataflow dataflow, char targetTable,
            TargetVersion targetVersion)
        {}

        public (bool,bool) HasMappingSet(string dataSpace, Dataflow dataFlow)
        {
            //TODO: Create database mock
            return (true, false);
        }

        public ISdmxObjects GetDsd(string dataSpace, string agencyId, string dsdId, string version)
        {
            var reference = new StructureReferenceImpl(agencyId, dsdId, version, SdmxStructureEnumType.Dsd);
            return this.GetRetrievalManager(dataSpace)
                .GetSdmxObjects(reference, ResolveCrossReferences.ResolveExcludeAgencies);
        }
        
        private void SetDsdMsd(string dataSpace, Dsd dsd)
        {
            if (!dsd.Base.Annotations.Any())
            {
                return;
            }

            var msdAnnotation = dsd.Base.Annotations.FirstOrDefault(x => x.Type.Equals("Metadata", StringComparison.OrdinalIgnoreCase));
            if (msdAnnotation == null || string.IsNullOrEmpty(msdAnnotation.Title))
            {
                return;
            }
            
            var reference = new StructureReferenceImpl(msdAnnotation.Title);
            var obj=this.GetRetrievalManager(dataSpace)
                .GetSdmxObjects(reference, ResolveCrossReferences.ResolveExcludeAgencies);

            dsd.Msd = new Msd(obj.MetadataStructures.FirstOrDefault());
        }

        public bool DeleteDataSetsAndMappingSets(string dataSpace, Dataflow dataFlow, bool deleteMsdObjectsOnly)
        {
            return true;
        }

        public bool CreateOrUpdateMappingSet(
            string dataSpace, 
            Dataflow dataFlow,
            char targetVersion,
            DateTime? validFrom,
            DateTime? validTo,
            MappingSetParams @params
         )
        {
            return true;
        }

        public HashSet<string> GetMigratedMappingSets(string dataSpaceId)
        {
            throw new NotImplementedException();
        }

        public ISet<IDataStructureObject> GetDataStructures(string dataSpace)
        {
            return GetRetrievalManager(dataSpace)
                .GetMaintainableObjects<IDataStructureObject>(new MaintainableRefObjectImpl(), true, false);
        }

        public ISet<IDataflowObject> GetDataflows(string dataSpace)
        {
            return GetRetrievalManager(dataSpace)
                .GetMaintainableObjects<IDataflowObject>(new MaintainableRefObjectImpl(), true, false);
        }

        public Dataflow GetDataflow(string dataSpaceId, IDataflowObject dataflowObject,
            IDataStructureObject dataStructure, IMetadataStructureDefinitionObject metadataStructureObject, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies, bool useDataFlowCache = true, bool useMsdCache = false)
        {
            var sdmxObjects = this.GetSdmxObjects(_structurePath);
            _dataflow = _dataflow ?? SdmxParser.ParseDataFlowStructure(sdmxObjects);

            if (_dataflow.Dsd.Msd == null)
                SetDsdMsd(dataSpaceId, _dataflow.Dsd);

            return _dataflow;
        }

        public Dsd GetDsd(string dataSpaceId, IDataStructureObject dataStructure, IMetadataStructureDefinitionObject metadataStructureObject, bool useDsdCache = true, bool useMsdCache = true)
        {
            var sdmxObjects = this.GetSdmxObjects(_structurePath);
            _dsd = _dsd ?? SdmxParser.ParseDsdStructure(sdmxObjects);

            if (_dsd.Msd == null)
                SetDsdMsd(dataSpaceId, _dsd);

            return _dsd;
        }

        public IEnumerable<DatasetEntity> GetNotMigratedDataSets(string dataSpaceId)
        {
            throw new NotImplementedException();
        }

        public bool IsMigrated(Dataflow dataflow, HashSet<string> migratedMappingSets)
        {
            throw new NotImplementedException();
        }

    }
}