SET NOCOUNT ON;

declare 
	@dbName varchar(100),
	@sql nvarchar(4000);

declare
	@dbs TABLE ([name] varchar(100) primary key);

----------------------------
insert into @dbs([name])
select [name] from sys.databases where name not in ('master','tempdb','msdb') and recovery_model_desc<> 'SIMPLE';
----------------------------

while (select count(*) from @dbs) > 0
BEGIN
	select top 1 @dbName = [name] from @dbs;
	delete from @dbs where [name]=@dbName;
	-------------------------------------
		
	-- change to a SIMPLE mode;
	set @sql = 'ALTER DATABASE [' + @dbName + '] SET RECOVERY SIMPLE';
	exec sp_executesql @sql;
	-- shrink log
	set @sql = 'USE [' + @dbName + ']; DBCC SHRINKFILE(2, TRUNCATEONLY)';
	exec sp_executesql @sql;
END