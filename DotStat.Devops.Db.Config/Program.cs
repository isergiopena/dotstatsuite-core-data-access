﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;
using CommandLine;
using DbUp;
using DbUp.Engine;
using DbUp.Engine.Transactions;
using DbUp.Helpers;
using DbUp.Support;
using DotStat.Devops.Db.Config.Options;
using Microsoft.Extensions.Configuration;

namespace DotStat.Devops.Db.Config
{
    class Program
    {
        private static readonly SqlScriptOptions RunAlwaysAtEndOption = new SqlScriptOptions { ScriptType = ScriptType.RunAlways, RunGroupOrder = DbUpDefaults.DefaultRunGroupOrder + 1 };

        private static int Main(string[] args)
        {
            //var testArgs = new[]
            //{
            //    "Databases:DotStat.Data.My.Design:DataMaxSize=555",
            //    "Databases:DotStat.Data.My.Design:LogMaxSize=777"
            //};

            var config = GetConfiguration(args);
            var dbConfig = config.Get<DbConfiguration>();

            return Parser.Default.ParseArguments<UpgradeOption>(args)
                .MapResult(
                    (option) => Upgrade(option, dbConfig),
                    error => 1);
        }

        protected static IConfigurationRoot GetConfiguration(string[] args, string path = "config")
        {
            var builder = new ConfigurationBuilder();
            builder.AddCommandLine(args);
            builder.AddEnvironmentVariables();

            if (System.IO.Directory.Exists(path))
            {
                foreach (var json in System.IO.Directory.GetFiles(path, "*.json"))
                    builder.AddJsonFile(json);
            }

            return builder.Build();
        }

        private static int Upgrade(UpgradeOption option, DbConfiguration dbConfig)
        {
            var upgradeEngine = DeployChanges.To
                .SqlDatabase(option.ConnectionString)
                .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                .WithScripts(new CustomSqlScriptProvider(dbConfig))
                .WithoutTransaction()
                .JournalTo(new NullJournal())
                .LogToConsole()
                .Build();

            var result = upgradeEngine.PerformUpgrade();

            return result.Successful
                ? 0
                : 1;
        }

        internal class CustomSqlScriptProvider : IScriptProvider
        {
            private readonly DbConfiguration _configuration;

            public CustomSqlScriptProvider(DbConfiguration configuration)
            {
                _configuration = configuration;
            }

            public IEnumerable<SqlScript> GetScripts(IConnectionManager connectionManager)
            {
                return connectionManager.ExecuteCommandsWithManagedConnection(GetDatabases);
            }

            private IEnumerable<SqlScript> GetDatabases(Func<IDbCommand> dbCommandFactory)
            {
                if (_configuration?.Databases == null)
                    yield break;

                using (var cmd = dbCommandFactory())
                {
                    cmd.CommandText = "select [name] from sys.databases where name not in ('master','tempdb','msdb','model')";
                    
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dbName = dr.GetString(0);

                            if (_configuration.Databases.TryGetValue(dbName, out var dbConfig))
                            {
                                var script = GetDbResizeScript(dbName, dbConfig);

                                if(script!=null)
                                    yield return script;
                            }
                        }
                    }
                }
            }

            private SqlScript GetDbResizeScript(string dbName, DbConfig dbConfig)
            {
                var sql = new StringBuilder();

                if (dbConfig.DataMaxSize > 0)
                {
                    sql.AppendLine($"alter database [{dbName}] MODIFY FILE (NAME=[{dbName}], MAXSIZE={dbConfig.DataMaxSize}MB);");
                }

                if (dbConfig.LogMaxSize > 0)
                {
                    sql.AppendLine($"alter database [{dbName}] MODIFY FILE (NAME=[{dbName}_log], MAXSIZE={dbConfig.LogMaxSize}MB);");
                }

                if (sql.Length == 0)
                    return null;

                return new SqlScript($"resize {dbName}.sql", sql.ToString(), RunAlwaysAtEndOption);
            }
        }
    }
}