using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.Exception;
using DotStat.DB.Exception;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Validation
{
    public class DatasetAttributeValidator : IDatasetAttributeValidator
    {
        private readonly IGeneralConfiguration _generalConfiguration;
        private int _maxErrorCount;
        private IEnumerator<KeyValuePair<IKeyValue, StagingRowActionEnum>> _enumeratorKeyValues;
        private IEnumerator<KeyValuePair<IKeyValue, StagingRowActionEnum>> _enumeratorKeyValuesReportedAtObservation;
        private HashSet<string> _attributeDuplicates;
        private Dictionary<string, Attribute> _datasetAttributes;
        private List<Attribute> _mandatoryDsdAttributes;
        private Dictionary<string, Attribute> _nonDatasetAttributes;
        private Dataflow _dataflow;
        private List<IValidationError> _errors;
        private bool _isValid;
        private HashSet<string> _attributesReportedAtNonObservation;

        private int _currentRow;

        public DatasetAttributeValidator(IGeneralConfiguration generalConfiguration)
        {
            _generalConfiguration = generalConfiguration ?? throw new ArgumentNullException(nameof(generalConfiguration));

            _maxErrorCount = _generalConfiguration.MaxTransferErrorAmount;

            _attributeDuplicates = new HashSet<string>();

            _errors = new List<IValidationError>();
        }

        public bool Validate(List<Attribute> reportedAttributes, List<BatchAction> batchActions, IList<DataSetAttributeRow> dataSetAttributeRowsReportedAtDataSetLevel, IList<DataSetAttributeRow> dataSetAttributeRowsReportedAtObservationLevel, Dataflow dataflow, int? maxErrorCount, bool fullValidation)
        {
            _isValid = true;
            _currentRow = 1;

            _dataflow = dataflow ?? throw new ArgumentNullException(nameof(dataflow));

            _datasetAttributes = dataflow.Dsd.Attributes.Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null)
                .ToDictionary(a => a.Code, a => a);
            
            _mandatoryDsdAttributes = dataflow.Dsd.Attributes.Where(a => a.Base.Mandatory)
                .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null).ToList();
            
            _nonDatasetAttributes = dataflow.Dsd.Attributes.Where(a => a.Base.AttachmentLevel != AttributeAttachmentLevel.DataSet && a.Base.AttachmentLevel != AttributeAttachmentLevel.Null)
                .ToDictionary(a => a.Code, a => a);

            _maxErrorCount = maxErrorCount ?? _generalConfiguration.MaxTransferErrorAmount;

            _errors.Clear();

            foreach (var batchAction in batchActions)
            {
                var allAttributes = (
                    from dataSetAttributeRow in dataSetAttributeRowsReportedAtDataSetLevel
                        .Where(a => a.BatchNumber == batchAction.BatchNumber)
                    from attribute in dataSetAttributeRow.Attributes
                    where reportedAttributes.Any(ra => ra.Code.Equals(attribute.Concept))
                    select attribute).ToList();
               
                allAttributes.AddRange(
                    from dataSetAttributeRow in dataSetAttributeRowsReportedAtObservationLevel
                        .Where(a => a.BatchNumber == batchAction.BatchNumber)
                    from attribute in dataSetAttributeRow.Attributes
                    where reportedAttributes.Any(ra => ra.Code.Equals(attribute.Concept))
                    select attribute);
                    
                MandatoryAttributesCheck(allAttributes, fullValidation, batchAction.Action);

                _attributeDuplicates.Clear();
                var attrs = (
                    from dataSetAttributeRow in dataSetAttributeRowsReportedAtDataSetLevel
                        .Where(a => a.BatchNumber == batchAction.BatchNumber)
                    from attribute in dataSetAttributeRow.Attributes
                    where reportedAttributes.Any(ra => ra.Code.Equals(attribute.Concept))
                    select new KeyValuePair<IKeyValue, StagingRowActionEnum>(attribute, dataSetAttributeRow.Action)).ToList();
                _enumeratorKeyValues = attrs.GetEnumerator();

                attrs = (
                    from dataSetAttributeRow in dataSetAttributeRowsReportedAtObservationLevel
                        .Where(a => a.BatchNumber == batchAction.BatchNumber)
                    from attribute in dataSetAttributeRow.Attributes
                    where reportedAttributes.Any(ra => ra.Code.Equals(attribute.Concept))
                    select new KeyValuePair<IKeyValue, StagingRowActionEnum>(attribute, dataSetAttributeRow.Action)).ToList();
                _enumeratorKeyValuesReportedAtObservation = attrs.GetEnumerator();
                    
                _attributesReportedAtNonObservation = new HashSet<string>();

                var enumerator = _enumeratorKeyValues;

                var reportedAtObservation = enumerator == _enumeratorKeyValuesReportedAtObservation;

                while (true)
                {
                    try
                    {
                        if (enumerator is null || !enumerator.MoveNext())
                        {
                            if (enumerator == _enumeratorKeyValues)
                            {
                                enumerator = _enumeratorKeyValuesReportedAtObservation;
                                reportedAtObservation = true;

                                if (enumerator is null || !enumerator.MoveNext())
                                {
                                    break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }

                        ValidateKeyValue(enumerator.Current.Key, reportedAtObservation, fullValidation, enumerator.Current.Value);
                    }
                    catch (SdmxValidationException e)
                    {
                        AddError(e.Error);
                    }
                    catch (System.Exception e)
                    {
                        AddError(ValidationErrorType.Undefined, enumerator?.Current.Key, e.Message);
                    }

                    if (IsMaxErrorLimitReached())
                    {
                        break;
                    }

                    _currentRow++;
                }
            }

            return _isValid;
        }

        private void AddError(ValidationErrorType errorType, IKeyValue keyValue = null, params string[] argument)
        {
            this.AddError(new KeyValueError()
            {
                Type = errorType,
                Coordinate = "",
                Value = keyValue == null ? null : $"{keyValue.Concept}:{keyValue.Code}",
                Argument = argument
            });
        }

        private void AddError(IValidationError error)
        {
            _isValid = false;

            error.Row = _currentRow;

            _errors.Add(error);
        }

        private void ValidateKeyValue(IKeyValue attributeKeyValue, bool reportedAtObservation, bool fullValidation, StagingRowActionEnum action)
        {
            if (!_attributeDuplicates.Add(attributeKeyValue.Concept))
            {
                if (reportedAtObservation && _attributesReportedAtNonObservation.Contains(attributeKeyValue.Concept))
                {
                    throw new KeyValueReadException(ValidationErrorType.AttributeReportedAtMultipleLevels, attributeKeyValue,
                        attributeKeyValue.Concept);
                }

                throw new KeyValueReadException(ValidationErrorType.DuplicatedDatasetAttribute, attributeKeyValue,
                    attributeKeyValue.Concept);
            }

            if (!reportedAtObservation)
            {
                _attributesReportedAtNonObservation.Add(attributeKeyValue.Concept);
            }

            if (!_datasetAttributes.TryGetValue(attributeKeyValue.Concept, out Attribute attribute))
            {
                if (_nonDatasetAttributes.TryGetValue(attributeKeyValue.Concept, out attribute))
                {
                    if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.Observation)
                    {
                        throw new KeyValueReadException(ValidationErrorType.ObservationAttributeAtNonObservationLevel, attributeKeyValue,
                            attributeKeyValue.Concept);
                    }

                    if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attribute.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                    {
                        throw new KeyValueReadException(ValidationErrorType.DimGroupAttributeAtDatasetLevel, attributeKeyValue, attributeKeyValue.Concept);
                    }

                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownAttributeAttachmentLevel),
                        attribute.Code,
                        attribute.Base.AttachmentLevel,
                        _dataflow.Dsd.FullId)
                    );
                }

                throw new KeyValueReadException(ValidationErrorType.AttributeNotInDsd, attributeKeyValue, attributeKeyValue.Concept,
                    _dataflow.Dsd.FullId);
            }

            if (action == StagingRowActionEnum.Delete)
                return;

            AttributeTypeCheck(attributeKeyValue, attribute, fullValidation, action);

            AttributeConstraintCheck(attributeKeyValue, attribute);
        }

        private void MandatoryAttributesCheck(IReadOnlyCollection<IKeyValue> allAttributeKeyValue, bool fullValidation, StagingRowActionEnum action)
        {
            if (!fullValidation)
                return;

            if (action == StagingRowActionEnum.Delete)
                return;

            if (_mandatoryDsdAttributes.Count == 0)
                return;

            //Non reported
            var missingMandatoryAttributes = _mandatoryDsdAttributes.Where(mandatoryAttribute =>
            !allAttributeKeyValue.Any(a => a.Concept.Equals(mandatoryAttribute.Code,
            StringComparison.InvariantCultureIgnoreCase)));

            foreach (var attribute in missingMandatoryAttributes)
            {
                throw new KeyValueReadException(ValidationErrorType.MandatoryAttributeMissing,
                    null, attribute.Code);
            }
        }

        private void AttributeTypeCheck(IKeyValue attributeKeyValue, Attribute attribute, bool fullValidation, StagingRowActionEnum action)
        {
            var isAttributeCodeEmpty = string.IsNullOrEmpty(attributeKeyValue.Code.HarmonizeSpecialCaseValues());
            if (fullValidation && action is StagingRowActionEnum.Merge or StagingRowActionEnum.Replace)
            {
                if (isAttributeCodeEmpty && attribute.Base.Mandatory)
                {
                    throw new KeyValueReadException(ValidationErrorType.MandatoryAttributeWithNullValueInStaging,
                        attributeKeyValue, attributeKeyValue.Concept);
                }
            }

            if (attribute.Base.HasCodedRepresentation())
            {
                if (!isAttributeCodeEmpty && attribute.FindMember(attributeKeyValue.Code, false) == null)
                {
                    throw new KeyValueReadException(ValidationErrorType.UnknownAttributeCodeMember, attributeKeyValue,
                        attributeKeyValue.Concept, attributeKeyValue.Code);
                }
            }
            else
            {
                var maxTextAttributeLength = attribute.Dsd.MaxTextAttributeLength ??
                                             attribute.Dsd.GetMaxLengthOfTextAttributeFromConfig(_generalConfiguration.MaxTextAttributeLength);

                if (!isAttributeCodeEmpty &&
                    maxTextAttributeLength is > 0 and <=4000 && // MaxTextAttributeLength annotation determines only the storage size, not the validation rule
                    attributeKeyValue.Code.Length > maxTextAttributeLength)
                {
                    throw new KeyValueReadException(ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit,
                        attributeKeyValue, attributeKeyValue.Concept, attributeKeyValue.Code,
                        attributeKeyValue.Code.Length.ToString(),
                        maxTextAttributeLength.ToString());
                }

                // Currently attribute can be coded or text. Additional type check should be placed here 
                // when other types will be supported. (eg. valid date, integer, double, etc.)
            }
        }
 
        private void AttributeConstraintCheck(IKeyValue attributeKeyValue, Attribute attribute)
        {
            var isAttributeCodeEmpty = string.IsNullOrEmpty(attributeKeyValue.Code.HarmonizeSpecialCaseValues());
            //validate allowed content constraint only for coded attributes
            if (attribute.Base.HasCodedRepresentation())
            {
                if (!isAttributeCodeEmpty && !attribute.IsAllowed(attributeKeyValue.Code))
                    throw new KeyValueReadException(ValidationErrorType.AttributeConstraintViolation, attributeKeyValue, attributeKeyValue.Concept + ":" + attributeKeyValue.Code);
            }
        }

        public List<IValidationError> GetErrors()
        {
            return _errors;
        }

        public string GetErrorsMessage()
        {
            var sb = new StringBuilder();

            foreach (var error in _errors)
                sb.AppendLine(error.ToString());

            return sb.ToString();
        }

        protected bool IsMaxErrorLimitReached()
        {
            return _maxErrorCount != 0 && _errors.Count >= _maxErrorCount;
        }
    }
}
