﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace DotStat.DB.Validation.TypeValidation
{
    public class MultiPatternValidator : SimpleSdmxTypeValidator
    {
        protected readonly string[] RegexPatterns;

        protected readonly Regex[] RegexObjects;

        public MultiPatternValidator(string[] regexPatterns, bool allowNullValue = true) : base(allowNullValue)
        {
            RegexPatterns = regexPatterns ?? throw new ArgumentNullException(nameof(regexPatterns));

            if (RegexPatterns.Length == 0)
            {
                throw new ArgumentException(nameof(regexPatterns));
            }
            RegexObjects = regexPatterns.Select(p => new Regex(p, RegexOptions.Compiled)).ToArray();
        }

        public override bool IsValid(string observationValue)
        {
            for (var i = 0; i < RegexObjects.Length; i++)
            {
                var regexObject = RegexObjects[i];

                if (!(AllowNullValue && string.IsNullOrEmpty(observationValue)) && !regexObject.IsMatch(observationValue ?? string.Empty))
                {
                    ValidationError = Db.Validation.ValidationErrorType.InvalidValueFormatPatternMismatch;

                    ExtraErrorMessageParameters = new[] {RegexPatterns[i]};

                    return false;
                }
            }
            
            ValidationError = Db.Validation.ValidationErrorType.Undefined;

            return true;
        }
    }
}
