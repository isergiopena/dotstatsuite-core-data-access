﻿using System;
using DotStat.Db.Validation;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;

namespace DotStat.DB.Validation.TypeValidation
{
    public class IntegerValidator : SimpleSdmxMinMaxValueValidator
    {
        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotInteger;

        public IntegerValidator(decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(minValue, maxValue, allowNullValue)
        {
        }

        protected override bool TryParseDecimalOutFunction(string observationValue, out decimal? decimalForMinMaxCheck, out bool isNegative)
        {
            decimalForMinMaxCheck = null;
            isNegative = false;

            if (!int.TryParse(observationValue, out var intValue))
            {
                return false;
            }

            isNegative = intValue < 0;
            
            decimalForMinMaxCheck = intValue;

            return true;
        }
    }
}
