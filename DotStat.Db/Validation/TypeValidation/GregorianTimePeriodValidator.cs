﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class GregorianTimePeriodValidator : ComplexSdmxTypeValidator
    {
        public GregorianTimePeriodValidator(bool allowNullValue = true) : base(
            new ISdmxTypeValidator[]
            {
                new GregorianYearValidator(allowNullValue), 
                new GregorianYearMonthValidator(allowNullValue),
                new GregorianDayValidator(allowNullValue)
            }, ComplexValidationMode.Any, allowNullValue)
        {
        }

        public override bool IsValid(string observationValue)
        {
            if (base.IsValid(observationValue))
            {
                return true;
            }

            ValidationError = ValidationErrorType.InvalidValueFormatNotGregorianTimePeriod;

            return false;
        }
    }
}
