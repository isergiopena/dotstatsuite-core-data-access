﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class BasicTimePeriodValidator : ComplexSdmxTypeValidator
    {
        public BasicTimePeriodValidator(bool allowNullValue = true) : base(
            new ISdmxTypeValidator[]
            {
                new DateTimeValidator(allowNullValue),
                new GregorianTimePeriodValidator(allowNullValue)
            }, ComplexValidationMode.Any, allowNullValue)
        {
        }

        public override bool IsValid(string observationValue)
        {
            if (base.IsValid(observationValue))
            {
                return true;
            }

            ValidationError = ValidationErrorType.InvalidValueFormatNotBasicTimePeriod;

            return false;
        }
    }
}
