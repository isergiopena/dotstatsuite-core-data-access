﻿using System;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class UriValidator : SimpleSdmxTypeValidator
    {
        public UriValidator(bool allowNullValue = true) : base (allowNullValue)
        {
        }

        public override bool IsValid(string observationValue)
        {
            ValidationError = ValidationErrorType.Undefined;

            if ((AllowNullValue && string.IsNullOrEmpty(observationValue)) || Uri.IsWellFormedUriString(observationValue, UriKind.RelativeOrAbsolute))
            {
                return true;
            }

            ValidationError = ValidationErrorType.InvalidValueFormatNotUri;

            return false;
        }
    }
}
