﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Localization;
using DotStat.Db.Helpers;
using DotStat.Domain;

namespace DotStat.Db.DB
{
    public class SqlDotStatDb : DotStatDbBase<SqlConnection>
    {
        private Version _currentDbVersion;
        private new const string ManagementSchema ="management";
        private new const string DataSchema = "data";
        private readonly string _connectionString;
        private readonly string _readOnlyConnectionString;

        public SqlDotStatDb(DataspaceInternal dataspace, Version supportedDbVersion) : 
            base(ManagementSchema, DataSchema, dataspace, supportedDbVersion )
        {
            _connectionString = dataspace.DotStatSuiteCoreDataDbConnectionString;
            //Do not use two different connection strings if readonly replica is not available
            _readOnlyConnectionString = string.IsNullOrEmpty(DataSpace.DotStatSuiteCoreDataDbConnectionStringReadOnlyReplica) ?
                dataspace.DotStatSuiteCoreDataDbConnectionString : DataSpace.DotStatSuiteCoreDataDbConnectionStringReadOnlyReplica;
        }

        public override SqlConnection GetConnection(bool readOnly = false)
        {
            return GetSqlConnection(readOnly);
        }

        public override async Task<string> GetDatabaseVersion(CancellationToken cancellationToken)
        {
            return (await ExecuteScalarSqlAsync("SELECT VERSION FROM dbo.DB_VERSION", cancellationToken)).ToString();
        }

        public override async Task<bool> ViewExists(string viewName, CancellationToken cancellationToken)
        {
            return await ExecuteScalarSqlWithParamsAsync(
                @"SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE UPPER(TABLE_NAME) = @Name",
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = viewName.ToUpper() }
            ) != null;
        }
        
        public override async Task<bool> TableExists(string tableName, CancellationToken cancellationToken)
        {
            return await ExecuteScalarSqlWithParamsAsync(
                @"SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE UPPER(TABLE_NAME) = @Name and TABLE_TYPE = 'BASE TABLE'",
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = tableName.ToUpper() }
                ) != null;
        }

        public override async Task<bool> ColumnExists(string tableName, string columnName, CancellationToken cancellationToken)
        {
            return (int) await ExecuteScalarSqlWithParamsAsync(
                       "select count(*) noOfColumns from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName AND COLUMN_NAME = @ColumnName",
                       cancellationToken,
                       new SqlParameter("TableName", SqlDbType.VarChar) { Value=tableName },
                       new SqlParameter("ColumnName", SqlDbType.VarChar) { Value=columnName }
                   ) == 1;
        }

        public override async Task DropView(string schema, string viewName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.VIEWS WHERE UPPER(TABLE_NAME) = @Name))
                BEGIN
                    DROP VIEW [{schema}].[{viewName}];
                END";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = viewName.ToUpper() }
            );
        }

        public override async Task DropTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES WHERE UPPER(TABLE_NAME) = @Name and TABLE_TYPE = 'BASE TABLE'))
                BEGIN
                    DROP TABLE [{schema}].[{tableName}];
                END";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) {Value = tableName.ToUpper()}
            );
        }

        public override async Task TruncateTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES WHERE UPPER(TABLE_NAME) = @Name and TABLE_TYPE = 'BASE TABLE'))
                BEGIN
                    TRUNCATE TABLE [{schema}].[{tableName}];
                END";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = tableName.ToUpper() }
            );
        }

        public override async Task DeleteAllFromTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES WHERE UPPER(TABLE_NAME) = @Name and TABLE_TYPE = 'BASE TABLE'))
                BEGIN
                    DELETE FROM [{schema}].[{tableName}];
                END";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = tableName.ToUpper() }
            );
        }

        public override async ValueTask<Version> GetCurrentDbVersion(CancellationToken cancellationToken)
        {
            if (_currentDbVersion == null)
            {

                var version = await GetDatabaseVersion(cancellationToken);

                if (!Version.TryParse(version, out _currentDbVersion))
                {
                    throw new NotSupportedException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DatabaseVersionIsInvalid),
                        Id, version));
                }
            }

            return _currentDbVersion;
        }


        public override async Task DropAllIndexesOfTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            await ExecuteNonQuerySqlAsync(
                    $@"DECLARE @TableName NVARCHAR(128) = '[{schema}].[{tableName}]',
 @IndexName NVARCHAR(128),
 @IsPK BIT,
 @IsUQ BIT;

DECLARE IndexCursor CURSOR FOR
SELECT name, is_primary_key, is_unique_constraint
FROM sys.indexes
WHERE object_id = OBJECT_ID(@TableName) AND [name] IS NOT NULL

OPEN IndexCursor

FETCH NEXT FROM IndexCursor INTO @IndexName, @IsPK, @IsUQ

WHILE @@FETCH_STATUS = 0
BEGIN
	IF(@IsPK = 1 OR @IsUQ = 1 ) BEGIN
		EXEC('ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + @IndexName)
	END
	ELSE BEGIN
		EXEC('DROP INDEX ' + @TableName + '.' + @IndexName)
	END

    FETCH NEXT FROM IndexCursor INTO @IndexName, @IsPK, @IsUQ
END

CLOSE IndexCursor

DEALLOCATE IndexCursor;", cancellationToken);
        }

        public override async Task CreateColumnstoreIndex(string schema, string tableName, string indexName, DataCompressionEnum dataCompression, CancellationToken cancellationToken)
        {
            await ExecuteNonQuerySqlAsync($@"IF NOT EXISTS (SELECT TOP 1 1 FROM sys.indexes
WHERE name = '{indexName}'
AND object_id = OBJECT_ID('[{schema}].[{tableName}]')) 
BEGIN
    CREATE CLUSTERED COLUMNSTORE INDEX {indexName}
    ON [{schema}].[{tableName}]
    WITH(DATA_COMPRESSION = {dataCompression})
END;", cancellationToken);

        }

        public override async Task AddTemporalTableSupport(string schema, string tableName, string historyTableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"ALTER TABLE [{schema}].[{tableName}]
ADD
        ValidFrom DATETIME2 GENERATED ALWAYS AS ROW START HIDDEN
            CONSTRAINT DF_{tableName}_ValidFrom DEFAULT SYSUTCDATETIME(),
        ValidTo DATETIME2 GENERATED ALWAYS AS ROW END HIDDEN
            CONSTRAINT DF_{tableName}_ValidTo DEFAULT CONVERT(DATETIME2, '9999-12-31 23:59:59.9999999'),
    PERIOD FOR SYSTEM_TIME(ValidFrom, ValidTo);

ALTER TABLE [{schema}].[{tableName}]
    SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [{schema}].[{historyTableName}]));";

            await ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        public override async Task RemoveTemporalTableSupport(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"IF (EXISTS (SELECT 1 FROM sys.tables WHERE UPPER([name]) = @Name AND [temporal_type] = 2))
                BEGIN
                    ALTER TABLE [{schema}].[{tableName}] SET ( SYSTEM_VERSIONING = OFF );
                END;";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = tableName.ToUpper() }
            );
        }

        private SqlConnection GetSqlConnection(bool readOnly = false)
        {
            var connString = new SqlConnectionStringBuilder(readOnly ? _readOnlyConnectionString : _connectionString)
            {
                ApplicationName = DbHelper.DbConnectionAppName
            };

            var conn = new SqlConnection(connString.ConnectionString);            
            conn.Open();

            if (System.Transactions.Transaction.Current == null)
            {
                using var cmd = conn.CreateCommand();
                cmd.CommandText = "SET TRANSACTION ISOLATION LEVEL READ COMMITTED;";
                cmd.ExecuteNonQuery();
            }

            return conn;
        }
    }
}
