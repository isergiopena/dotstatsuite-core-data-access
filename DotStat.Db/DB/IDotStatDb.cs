﻿using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using DotStat.Common.Configuration.Dto;
using DotStat.Domain;

namespace DotStat.Db.DB
{
    public interface IDotStatDb
    {
        string Id { get; set; }

        string ManagementSchema { get; }

        string DataSchema { get; }

        int DatabaseCommandTimeout { get; }

        int DataImportTimeOutInMinutes { get; }

        DataspaceInternal DataSpace { get; }

        DbConnection GetDbConnection(bool readOnly = false);
        Task<string> GetDatabaseVersion(CancellationToken cancellationToken);
        Task<int> ExecuteNonQuerySqlAsync(string command, CancellationToken cancellationToken);
        Task<int> ExecuteNonQuerySqlWithParamsAsync(string command, CancellationToken cancellationToken, params DbParameter[] parameters);
        Task<object> ExecuteScalarSqlAsync(string command, CancellationToken cancellationToken);

        Task<object> ExecuteScalarSqlWithParamsAsync(string command, CancellationToken cancellationToken, params DbParameter[] parameters);

        Task<DbDataReader> ExecuteReaderSqlAsync(string command, CancellationToken cancellationToken, CommandBehavior commandBehavior = CommandBehavior.SequentialAccess, bool tryUseReadOnlyConnection = false);

        Task<DbDataReader> ExecuteReaderSqlWithParamsAsync(string command, CancellationToken cancellationToken, CommandBehavior commandBehavior = CommandBehavior.SequentialAccess, bool tryUseReadOnlyConnection = false, params DbParameter[] parameters);

        Task<bool> ViewExists(string viewName, CancellationToken cancellationToken);

        Task<bool> TableExists(string tableName, CancellationToken cancellationToken);

        Task<bool> ColumnExists(string tableName, string columnName, CancellationToken cancellationToken);

        Task DropView(string schema, string viewName, CancellationToken cancellationToken);

        Task DropTable(string schema, string tableName, CancellationToken cancellationToken);
        
        Task TruncateTable(string schema, string tableName, CancellationToken cancellationToken);
        Task DeleteAllFromTable(string schema, string tableName, CancellationToken cancellationToken);

        ValueTask<Version> GetCurrentDbVersion(CancellationToken cancellationToken);
        Version GetSupportedDbVersion();
        TransactionScope GetTransactionScope(bool useDefaultIsolationLevel = false);
        Task DropAllIndexesOfTable(string schema, string tableName, CancellationToken cancellationToken);
        Task CreateColumnstoreIndex(string schema, string tableName, string indexName, DataCompressionEnum dataCompression, CancellationToken cancellationToken);
        Task AddTemporalTableSupport(string schema, string tableName, string historyTableName, CancellationToken cancellationToken);
        Task RemoveTemporalTableSupport(string schema, string tableName, CancellationToken cancellationToken);
    }
}