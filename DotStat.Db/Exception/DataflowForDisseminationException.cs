﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class DataFlowForDisseminationException : DotStatException
    {
        public DataFlowForDisseminationException()
        {
        }

        public DataFlowForDisseminationException(string message) : base(message)
        {
        }

        public DataFlowForDisseminationException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
