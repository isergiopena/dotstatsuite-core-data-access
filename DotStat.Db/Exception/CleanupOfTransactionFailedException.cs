﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;

namespace DotStat.Db.Exception
{
    public abstract class ChangeInArtefactException: DotStatException
    {
        public string Id { get; }
        public string ChangeDescription { get; }

        public ChangeInArtefactException(string id, string changeDescription)
        {
            Id = id;
            ChangeDescription = changeDescription;
        }

        public ChangeInArtefactException(string id, string changeDescription, System.Exception innerException) : base(null, innerException)
        {
            Id = id;
            ChangeDescription = changeDescription;
        }
    }

    [ExcludeFromCodeCoverage]
    class ChangeInDsdException: ChangeInArtefactException
    {
        public ChangeInDsdException(string id, string changeDescription) : base(id, changeDescription)
        {
        }
        public ChangeInDsdException(string id, string changeDescription, System.Exception innerException) : base(id, changeDescription, innerException)
        {
        }
        public override string Message
        {
            get
            {
                var commonMessage = LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdCommonMessage);

                return $"{string.Format(commonMessage, Id)}{ChangeDescription}";
            }
        }
    }

    [ExcludeFromCodeCoverage]
    class ChangeInMsdException : ChangeInArtefactException
    {
        public ChangeInMsdException(string id, string changeDescription) : base(id, changeDescription)
        {
        }
        public ChangeInMsdException(string id, string changeDescription, System.Exception innerException) : base(id, changeDescription, innerException)
        {
        }
        public override string Message
        {
            get
            {
                var commonMessage = LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInMsdCommonMessage);

                return $"{string.Format(commonMessage, Id)}{ChangeDescription}";
            }
        }
    }
}