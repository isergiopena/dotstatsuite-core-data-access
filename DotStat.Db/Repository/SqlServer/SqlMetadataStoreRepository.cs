﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.DB;
using DotStat.Db.Exception;
using DotStat.Db.Reader;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.DB.Repository;
using DotStat.DB.Repository.SqlServer;
using DotStat.DB.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Dataflow = DotStat.Domain.Dataflow;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlMetadataStoreRepository : SqlDataMergerBase, IMetadataStoreRepository, IDataMerger
    {
        public SqlMetadataStoreRepository(SqlDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }

        public async Task<ImportSummary> MergeStagingTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents, Dictionary<int, BatchAction> obsLevelBatchActions, IList<DataSetAttributeRow> dataSetAttributeRows, CodeTranslator translator,
        DbTableVersion tableVersion, bool includeSummary, CancellationToken cancellationToken)
        {
            var importSummary = new ImportSummary
            {
                //ObservationsCount = bulkImportResult.RowsCopied,
                Errors = new List<IValidationError>()
            };

            if (!obsLevelBatchActions.Any() && !dataSetAttributeRows.Any())
                return importSummary;

            var hasMerges = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Merge);
            var hasReplaces = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Replace);
            var hasDeletions = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Delete);
            var hasDeleteAll = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.DeleteAll);

            if (hasMerges || hasReplaces)
            {
                //Insert new series found in the import file
                await MergeStagingToDimensionsTable(referencedStructure, reportedComponents, cancellationToken);
            }

            using (TransactionScope scope = DotStatDb.GetTransactionScope())
            {
                //Use the same datetime for the LAST_UPDATED column for all changes within the same import/transfer

                var dateTime = DateTime.UtcNow;
                //Merge non dataset level components 
                foreach (var (_, batchAction) in obsLevelBatchActions)
                {
                    var action = batchAction.Action;

                    //Special case DeleteAll 
                    if (action is StagingRowActionEnum.DeleteAll)
                    {
                        await DeleteAll(referencedStructure, tableVersion, includeSummary, importSummary, translator, dateTime, cancellationToken);

                        importSummary.RequiresRecalculationOfActualContentConstraint = true;
                        if (importSummary.Errors.Count > 0)
                            return importSummary;
                    }
                    else
                    {
                        //Process observation level components (primary measure and attributes which reference all dimensions, including time)
                        var mergeResult = await MergeStagingToObservationsTable(referencedStructure, reportedComponents, tableVersion,
                            batchAction, includeSummary, translator, dateTime, cancellationToken);

                        if (mergeResult.Errors.Any())
                        {
                            importSummary.Errors.AddRange(mergeResult.Errors);
                            return importSummary;
                        }

                        importSummary.RequiresRecalculationOfActualContentConstraint = importSummary.RequiresRecalculationOfActualContentConstraint || mergeResult.RowsAddedDeleted;
                        importSummary.ObservationLevelMergeResult.Add(mergeResult);
                       
                    }
                }

                //Merge  dataset level components 
                foreach (var dataSetAttributeRow in dataSetAttributeRows)
                {
                    var action = dataSetAttributeRow.Action;
                    hasReplaces = hasReplaces || action == StagingRowActionEnum.Replace;
                    hasDeletions = hasDeletions || action == StagingRowActionEnum.Delete;
                    hasDeleteAll = hasDeleteAll || action == StagingRowActionEnum.DeleteAll;

                    //Special case DeleteAll 
                    if (action is StagingRowActionEnum.DeleteAll)
                        continue;

                    var mergeResult = await MergeStagingToDatasetTable(referencedStructure, dataSetAttributeRow.Attributes,
                                    translator, tableVersion, action, includeSummary, dateTime, cancellationToken);

                    if (mergeResult.Errors.Any())
                    {
                        importSummary.Errors.AddRange(mergeResult.Errors);
                        return importSummary;
                    }

                    importSummary.DataFlowLevelMergeResult.Add(mergeResult);
                }

                //Store delete instructions
                if (hasReplaces || hasDeletions || hasDeleteAll)
                {
                    await StoreDeleteInstructions(
                        referencedStructure,
                        reportedComponents,
                        dataSetAttributeRows,
                        tableVersion,
                        dateTime,
                        cancellationToken
                    );
                }

                //Merge action cannot set components to NULL, no cleanup needed
                //Merge & Replace actions cannot set components to NULL at series not dataset level, no cleanup needed
                if (hasReplaces || hasDeletions)
                {
                    //Clean up fully deleted observations
                    var observationsRemoved = await CleanUpFullyEmptyObservations(referencedStructure, tableVersion, cancellationToken);
                    importSummary.RequiresRecalculationOfActualContentConstraint = importSummary.RequiresRecalculationOfActualContentConstraint || observationsRemoved;
                }

                scope.Complete();
            }

            return importSummary;
        }

        public async Task<BulkImportResult> BulkInsertMetadata(
            IAsyncEnumerable<ObservationRow> observations, 
            ReportedComponents reportedComponents, 
            ICodeTranslator translator, 
            IImportReferenceableStructure referencedStructure,
            bool fullValidation, bool isTimeAtTimeDimensionSupported,
            CancellationToken cancellationToken)
        {
            await using var connection = DotStatDb.GetConnection();
            using var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null);

            bulkCopy.DestinationTableName = $"[{DotStatDb.DataSchema}].[{referencedStructure.Dsd.SqlMetadataStagingTable()}]";
            bulkCopy.BatchSize = 30000;
            bulkCopy.BulkCopyTimeout = DotStatDb.DatabaseCommandTimeout;
            if (DotStatDb.DataSpace.NotifyImportBatchSize > 0)
            {
                bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(OnSqlRowsCopied);
                bulkCopy.NotifyAfter = DotStatDb.DataSpace.NotifyImportBatchSize;
            }

            using var metadataReader = new SdmxMetadataObservationReader(observations, reportedComponents, referencedStructure.Dsd, translator, GeneralConfiguration, fullValidation, isTimeAtTimeDimensionSupported);

            //Not all dimensions might be reported
            // if staging contains calculated ROW_ID column
            if (reportedComponents.Dimensions.Count > 32)
            {
                for (var i = 0; i < metadataReader.FieldCount; i++)
                {
                    bulkCopy.ColumnMappings.Add(i, i + 1);
                }
            }
            try
            {
                await bulkCopy.WriteToServerAsync(metadataReader, cancellationToken);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // Generic exception handling for the case when the bulk copy operation fails due to the length of the value written to database
                if (!ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                    throw;

                var match = Regex.Match(ex.Message.ToString(), @"\d+");
                var index = Convert.ToInt32(match.Value) - 1;

                var fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                var sortedColumns = fi.GetValue(bulkCopy);
                var items = (object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                var itemData = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                var metaData = itemData.GetValue(items[index]);

                var maxLength = metaData.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metaData);

                var msg = string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InvalidValueLengthExceedsStorageSize),
                    metadataReader.GetName(index), // {0} - component name
                    metadataReader.CurrentIndex, // {1} - observation index
                    metadataReader.CurrentIndex + 1, // {2} row no. in a csv file
                    maxLength); // {4} - the max. length allowed by the SQL data type

                throw new BulkCopyException(msg);
            }

            var errors = metadataReader.GetErrors();
            var rowsCopied = metadataReader.GetObservationsCount();
            var batchActions = metadataReader.GetBatchActions();

            Log.Notice(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ReadingMetaSourceFinished),
                rowsCopied));

            var datasetMetadataAttributesRows = metadataReader.DatasetMetadataAttributes.Select(d => d.Value).ToList();

            return new BulkImportResult(datasetMetadataAttributesRows, errors, rowsCopied, batchActions);
        }
        
        public async Task DropMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlMetadataStagingTable(), cancellationToken);
        }

        public async Task DropMetadataStagingTables(int dsdDbId, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlMetadataStagingTable(dsdDbId), cancellationToken);
        }

        public async Task RecreateMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DropMetadataStagingTables(dsd, cancellationToken);
            await BuildMetadataStagingTables(dsd, cancellationToken);
        }

        public async Task AddIndexMetadataStagingTable(Dsd dsd, CancellationToken cancellationToken)
        {
            if (dsd?.Msd == null)
            {
                return;
            }

            var tableName = dsd.SqlMetadataStagingTable();
            var nonTimeDims = dsd.Dimensions
                .Where(x => !x.Base.TimeDimension)
                .ToArray();

            var metadataAttributesColumns = dsd.Msd.MetadataAttributes.ToColumnList(true);

            var dimensionColumns = nonTimeDims.ToColumnList();
            var timeDimColumns = dsd.TimeDimension != null
                ? "," + string.Join(",", dsd.TimeDimension.SqlColumn(), DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd())
                : null;

            var indexColumns = dsd.Dimensions.Count > 32 ? "[ROW_ID]" : $"{dimensionColumns}{timeDimColumns}";
            var indexName = dsd.Dimensions.Count > 32 ? $"[idx_ROW_ID_{dsd.Msd.DbId}]" : $"[idx_dimensions_{dsd.Msd.DbId}]";

            await DotStatDb.ExecuteNonQuerySqlAsync($@"CREATE NONCLUSTERED INDEX {indexName} 
                ON [{DotStatDb.DataSchema}].[{tableName}]({indexColumns})
                INCLUDE ({metadataAttributesColumns})", 
                cancellationToken);
        }

        public async Task AddUniqueIndexMetadataStagingTable(Dsd dsd, CancellationToken cancellationToken)
        {
            if (dsd.Msd == null)
            {
                return;
            }

            var tableName = dsd.SqlMetadataStagingTable();
            var dimensionColumns = dsd.Dimensions.ToColumnList();
            var indexColumns = dsd.Dimensions.Count > 32 ? "[ROW_ID]" : $"{dimensionColumns}";
            var indexName = dsd.Dimensions.Count > 32 ? $"[UI_ROW_ID]" : $"[UI_dimensions]";

            try
            {
                await DotStatDb.ExecuteNonQuerySqlAsync($@"CREATE UNIQUE INDEX {indexName} 
                ON [{DotStatDb.DataSchema}].[{tableName}]({indexColumns})",
                    cancellationToken);
            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 1505://duplicates in the staging table insert
                        throw new ConsumerValidationException(new List<ValidationError> { new ObservationError() { Type = ValidationErrorType.DuplicatedRowsInMetadataStagingTable } });
                    default:
                        throw;
                }
            }
        }
        
        public async Task DeleteDsdMetadata(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await TruncateMetadataTables(dsd, tableVersion, cancellationToken);
        }

        public async Task DeleteDataFlowMetadata(Dataflow dataFlow, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await TruncateMetadataTables(dataFlow, tableVersion, cancellationToken);
        }

        public async ValueTask CopyDsdMetadataToNewVersion(Dsd dsd, DbTableVersion sourceTableVersion, DbTableVersion targetTableVersion, CancellationToken cancellationToken)
        {
            if (sourceTableVersion == targetTableVersion)
            {
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");
            }

            if (dsd?.Msd == null)
            {
                return;
            }

            var dimensionColumns = dsd.Dimensions.Where(x => !x.Base.TimeDimension).ToColumnList();
            var timeDim = dsd.TimeDimension;
            var timeDimColumns = timeDim != null
                ? "," + string.Join(",", timeDim.SqlColumn(), DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd())
                : null;

            var metadataAttributes = dsd.Msd.MetadataAttributes;

            var metadataAttributeColumns = metadataAttributes.Any()
                ? "," + metadataAttributes.ToColumnList()
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO [{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataStructureTable((char)targetTableVersion)}] 
                      ({dimensionColumns}{timeDimColumns}{metadataAttributeColumns}, [{DbExtensions.LAST_UPDATED_COLUMN}])
                    SELECT {dimensionColumns}{timeDimColumns}{metadataAttributeColumns}, [{DbExtensions.LAST_UPDATED_COLUMN}]
                      FROM [{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataStructureTable((char)sourceTableVersion)}]",
                cancellationToken
            );

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO [{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataSetTable((char)targetTableVersion)}] 
                      ([DF_ID]{metadataAttributeColumns}, [{DbExtensions.LAST_UPDATED_COLUMN}] )
                    SELECT [DF_ID]{metadataAttributeColumns}, [{DbExtensions.LAST_UPDATED_COLUMN}] 
                      FROM [{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataSetTable((char)sourceTableVersion)}]
                      WHERE [DF_ID] = -1",
                cancellationToken
            );
        }

        public async ValueTask CopyDataFlowMetadataToNewVersion(Dataflow dataFlow, DbTableVersion sourceTableVersion, DbTableVersion targetTableVersion, CancellationToken cancellationToken)
        {
            if (sourceTableVersion == targetTableVersion)
            {
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");
            }

            if (dataFlow.Dsd?.Msd == null)
            {
                return;
            }

            var dimensionColumns = dataFlow.Dsd.Dimensions.Where(x => !x.Base.TimeDimension).ToColumnList();
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimColumns = timeDim != null
                ? "," + string.Join(",", timeDim.SqlColumn(), DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd())
                : null;

            var metadataAttributes = dataFlow.Dsd.Msd.MetadataAttributes;

            var metadataAttributeColumns = metadataAttributes.Any()
                ? "," + metadataAttributes.ToColumnList()
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO [{DotStatDb.DataSchema}].[{dataFlow.SqlMetadataDataFlowTable((char)targetTableVersion)}] 
                      ({dimensionColumns}{timeDimColumns}{metadataAttributeColumns}, [{DbExtensions.LAST_UPDATED_COLUMN}])
                    SELECT {dimensionColumns}{timeDimColumns}{metadataAttributeColumns}, [{DbExtensions.LAST_UPDATED_COLUMN}]
                      FROM [{DotStatDb.DataSchema}].[{dataFlow.SqlMetadataDataFlowTable((char)sourceTableVersion)}]",
                cancellationToken
            );

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataDataSetTable((char)targetTableVersion)}] 
                      ([DF_ID]{metadataAttributeColumns}, [{DbExtensions.LAST_UPDATED_COLUMN}] )
                    SELECT [DF_ID]{metadataAttributeColumns}, [{DbExtensions.LAST_UPDATED_COLUMN}] 
                      FROM [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataDataSetTable((char)sourceTableVersion)}]
                      WHERE [DF_ID]={dataFlow.DbId}",
                cancellationToken
            );
        }


        private async Task StoreDeleteInstructions(
            Dsd dsd,
            int dfId,
            string metadataTable,
            IList<DataSetAttributeRow> dataSetAttributeRows,
            DbTableVersion tableVersion,
            DateTime dateTime,
            CancellationToken cancellationToken)
        {
            var dimensions = new List<string>();

            if (dsd.Dimensions.Any())
            {
                dimensions.AddRange(dsd.Dimensions.Select(d => d.SqlColumn()));
            }
            var switchedOffDimParameters = new List<DbParameter>();
            var switchedOffDimParameters2 = new List<DbParameter>();
            for (var index = 0; index < dimensions.Count; index++)
            {
                switchedOffDimParameters.Add(new SqlParameter($"pDim{index}", DbExtensions.DimensionSwitchedOffDbValue.ToString()));
                switchedOffDimParameters2.Add(new SqlParameter($"pDim{index}", DbExtensions.DimensionSwitchedOffDbValue.ToString()));
            }

            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();
            if (dsd.TimeDimension != null)
            {
                dimensions.Add(DbExtensions.SqlPeriodStart());
                switchedOffDimParameters.Add(new SqlParameter($"pDim{DbExtensions.SqlPeriodStart()}", SqlDbType.DateTime2) { Value = supportsDateTime ? DateTime.MaxValue : DateTime.MaxValue.Date });
                switchedOffDimParameters2.Add(new SqlParameter($"pDim{DbExtensions.SqlPeriodStart()}", SqlDbType.DateTime2) { Value = supportsDateTime ? DateTime.MaxValue : DateTime.MaxValue.Date });
                dimensions.Add(DbExtensions.SqlPeriodEnd());
                switchedOffDimParameters.Add(new SqlParameter($"pDim{DbExtensions.SqlPeriodEnd()}", SqlDbType.DateTime2) { Value = supportsDateTime ? DateTime.MinValue : DateTime.MinValue.Date });
                switchedOffDimParameters2.Add(new SqlParameter($"pDim{DbExtensions.SqlPeriodEnd()}", SqlDbType.DateTime2) { Value = supportsDateTime ? DateTime.MinValue : DateTime.MinValue.Date });
            }

            var dimensionsString = string.Join(",", dimensions);
            var componentsString = dsd.Msd.MetadataAttributes.ToColumnList();

            var sqlParameters = new List<DbParameter>();
            if (dimensions.Any() && dsd.Msd.MetadataAttributes.Any())
            {
                sqlParameters = new List<DbParameter>()
                {
                    new SqlParameter("DF_ID", dfId),
                    new SqlParameter("Delete", (int) StagingRowActionEnum.Delete),
                    new SqlParameter("DT_Now", DateTime.UtcNow)
                };

                var sql = @$"INSERT INTO [{DotStatDb.DataSchema}].[{dsd.SqlDeletedMetadataTable((char)tableVersion)}]([DF_ID],[{DbExtensions.LAST_UPDATED_COLUMN}],{dimensionsString},{componentsString}) 
                    SELECT @DF_ID,@DT_Now,{dimensionsString},{componentsString} FROM [{DotStatDb.DataSchema}].[{dsd.SqlMetadataStagingTable()}] 
                    WHERE [REQUESTED_ACTION]= @Delete";

                await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sql, cancellationToken, sqlParameters.ToArray());
            }

            if (!dataSetAttributeRows.Any() ||
                dataSetAttributeRows.Any(x => x.Action == StagingRowActionEnum.DeleteAll))
            {
                return;
            }

            var datasetAttributesOfDds = dsd.Msd.MetadataAttributes.ToDictionary(x => x.HierarchicalId);
            foreach (var datasetRow in dataSetAttributeRows.Where(x => x.Action == StagingRowActionEnum.Delete))
            {
                sqlParameters = new List<DbParameter>()
                {
                    new SqlParameter("DF_ID", dfId),
                    new SqlParameter("DT_Now", dateTime)
                };

                sqlParameters.AddRange(switchedOffDimParameters);

                var components = new List<string>();
                foreach (var keyValue in datasetRow.Attributes)
                {
                    var value = !string.IsNullOrEmpty(keyValue.Code) ? DbExtensions.ColumnPresentDbValue.ToString() : null;
                    
                    var dsAttribute = datasetAttributesOfDds[keyValue.Concept]; 
                    components.Add(dsAttribute.SqlColumn());
                    sqlParameters.Add(new SqlParameter($"pComp{dsAttribute.DbId}", string.IsNullOrEmpty(value) ? DBNull.Value : value));
                }
                var sql = @$"INSERT INTO [{DotStatDb.DataSchema}].[{dsd.SqlDeletedMetadataTable((char)tableVersion)}]([DF_ID],[{DbExtensions.LAST_UPDATED_COLUMN}], {dimensionsString},{string.Join(",", components)}) 
                    SELECT {string.Join(",", sqlParameters.Select(x => "@" + x.ParameterName))}";

                await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sql, cancellationToken, sqlParameters.ToArray());
            }

            // Store fully empty rows in deletions table as a result of a partial delete or a replace to all nulls
            var attributeColumnsIsNull = dsd.Msd.MetadataAttributes.ToColumnListWithIsNull();
            var selectPresentColumns = string.Join(", ", dsd.Msd.MetadataAttributes.Select(attr => $"@PresentColumn AS {attr.SqlColumn()}"));
            
            sqlParameters = new List<DbParameter>()
            {
                new SqlParameter("DF_ID", dfId),
                new SqlParameter("DT_Now", dateTime),
                new SqlParameter("PresentColumn", DbExtensions.ColumnPresentDbValue.ToString())
            };
            sqlParameters.AddRange(switchedOffDimParameters2);
            
            var sqlCommand = $@"INSERT INTO [{DotStatDb.DataSchema}].[{dsd.SqlDeletedMetadataTable((char)tableVersion)}]([DF_ID],[{DbExtensions.LAST_UPDATED_COLUMN}],{dimensionsString},{componentsString}) 
SELECT @DF_ID,@DT_Now,{dimensionsString},{selectPresentColumns} 
FROM [{DotStatDb.DataSchema}].[{metadataTable}] 
WHERE {attributeColumnsIsNull}
UNION ALL 
SELECT @DF_ID,@DT_Now,{string.Join(",", switchedOffDimParameters.Select(x => "@" + x.ParameterName))},{selectPresentColumns} 
FROM [{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataSetTable((char)tableVersion)}] 
WHERE {attributeColumnsIsNull} AND [DF_ID]=@DF_ID";
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, sqlParameters.ToArray());
        }

        protected override ValueTask MergeDsdStagingToDimensionsTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken) {
            return new ValueTask();
        }

        public override async ValueTask<MergeResult> MergeStagingToObservationsTable(IImportReferenceableStructure referencedStructure, 
            ReportedComponents reportedComponents, DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary, 
            ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow dataFlow)
            {
                //Filter allowed content constraint
                var constraint = batchAction.Action == StagingRowActionEnum.Delete ?
                    Predicate.BuildWhereForObservationLevel(dataQuery: null, dataFlow, codeTranslator, useExternalValues: false, nullTreatment: NullTreatment.IncludeNullsAndSwitchOff, tableAlias: "ME")
                    : string.Empty;
                if (!string.IsNullOrEmpty(constraint))
                    constraint = $"AND {constraint}";

                var metadataTableName = $"[{DotStatDb.DataSchema}].[{dataFlow.SqlMetadataDataFlowTable((char)tableVersion)}]";

                return await MergeStagingToObservationsTable(dataFlow.Dsd, constraint, metadataTableName, reportedComponents,
                    batchAction, includeSummary, dateTime, cancellationToken);
            }
            else
            {
                var dsd = referencedStructure as Dsd;
                var metadataTableName = $"[{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataStructureTable((char)tableVersion)}]";

                return await MergeStagingToObservationsTable(dsd, "", metadataTableName, reportedComponents,
                    batchAction, includeSummary, dateTime, cancellationToken);
            }
        }

        private async ValueTask<MergeResult> MergeStagingToObservationsTable(Dsd dsd, string constraint, string metadataTableName, ReportedComponents reportedComponents,
            BatchAction batchAction, bool includeSummary, DateTime dateTime, CancellationToken cancellationToken)
        {
            var action = batchAction.Action;
            var batchNumber =  batchAction.BatchNumber;
            var mergeResult = new MergeResult();

            if (action is StagingRowActionEnum.Skip || action is StagingRowActionEnum.DeleteAll || dsd?.Msd == null)
                return mergeResult;

            if (!reportedComponents.MetadataAttributes.Any() && action != StagingRowActionEnum.Replace)
                return mergeResult;

            //Dimensions
            var nonTimeDims = dsd.Dimensions
               .Where(x => !x.Base.TimeDimension)
               .ToArray();

            var dimensionColumnsNoTimeDim = nonTimeDims.ToColumnList();
            
            var dimensionColumnsNoTimeDimJoin = string.Join(" AND ", nonTimeDims.Select(
                dim => dim.Codelist!=null 
                    ? string.Format("([ME].{0} = COALESCE([ST].{0}, [ME].{0}) OR ([ST].{0} = {1} AND [ME].{0} != {2}))\n", dim.SqlColumn(), DbExtensions.DimensionWildCardedDbValue, DbExtensions.DimensionSwitchedOffDbValue)
                    : string.Format("([ME].{0} = COALESCE([ST].{0}, [ME].{0}) OR ([ST].{0} = '{1}' AND [ME].{0} != '{2}'))\n", dim.SqlColumn(), DbExtensions.DimensionWildCarded, DbExtensions.DimensionSwitchedOff)
            ));
            
            var dimensionColumnsNoTimeDimInsertValues = string.Join(", ", nonTimeDims.Select(
                dim => dim.Codelist!=null 
                    ? $"COALESCE({dim.SqlColumn()}, {DbExtensions.DimensionSwitchedOffDbValue})"
                    : $"COALESCE({dim.SqlColumn()}, '{DbExtensions.DimensionSwitchedOff}')"
            ));

            string timeDimColumns = null;
            string timeDimColumnsInsertValues = null;
            string timeDimColumnsJoin = null;
            var sqlParams = new List<DbParameter>();

            if (dsd.TimeDimension != null)
            {
                timeDimColumns = ", " + string.Join(", ", dsd.TimeDimension.SqlColumn(), DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd());
                timeDimColumnsInsertValues = ", " + string.Join(", ", dsd.TimeDimension.SqlColumn(), $"COALESCE({DbExtensions.SqlPeriodStart()}, @maxDT)", $"COALESCE({DbExtensions.SqlPeriodEnd()}, @minDT)");
                timeDimColumnsJoin =
                    string.Format(" AND ([ME].{0} = COALESCE([ST].{0}, [ME].{0}) OR ([ST].{0} = @maxDTmin1Year AND [ME].{0} != @maxDT))\n", DbExtensions.SqlPeriodStart()) +
                    string.Format("AND ([ME].{0} = COALESCE([ST].{0}, [ME].{0}) OR ([ST].{0} = @minDTplus1Year AND [ME].{0} != @minDT))\n", DbExtensions.SqlPeriodEnd());

                var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

                sqlParams.Add(new SqlParameter("maxDTmin1Year", SqlDbType.DateTime2) { Value = supportsDateTime ? DateTime.MaxValue.AddYears(-1) : DateTime.MaxValue.Date.AddYears(-1) });
                sqlParams.Add(new SqlParameter("maxDT", SqlDbType.DateTime2) { Value = supportsDateTime ? DateTime.MaxValue : DateTime.MaxValue.Date });
                sqlParams.Add(new SqlParameter("minDTplus1Year", SqlDbType.DateTime2) { Value = supportsDateTime ? DateTime.MinValue.AddYears(1) : DateTime.MinValue.Date.AddYears(1) });
                sqlParams.Add(new SqlParameter("minDT", SqlDbType.DateTime2) { Value = supportsDateTime ? DateTime.MinValue : DateTime.MinValue.Date });
            }

            string updateDiffCheck = null;

            //Metadata Attributes
            var reportedAttributes = dsd.Msd.MetadataAttributes
                .Where(a =>
                    reportedComponents.MetadataAttributes.Any(r =>
                        r.HierarchicalId.Equals(a.HierarchicalId, StringComparison.InvariantCultureIgnoreCase))).ToList();

            string attrColumnsSelect = null;
            string attrColumnsInsert = null;
            string attrColumnsUpdate = null;
            switch (action)
            {
                case StagingRowActionEnum.Merge:
                    attrColumnsSelect = reportedAttributes.ToColumnList();
                    attrColumnsInsert = reportedAttributes.ToColumnList();
                    attrColumnsUpdate = string.Join(",\n", reportedAttributes
                        .Select(a => $"[ME].{a.SqlColumn()} = CASE WHEN [ST].{a.SqlColumn()} IS NOT NULL THEN [ST].{a.SqlColumn()} ELSE [ME].{a.SqlColumn()} END"));
                    updateDiffCheck = string.Join(" OR\n", reportedAttributes
                        .Select(a => $"([ST].{a.SqlColumn()} IS NOT NULL AND ([ME].{a.SqlColumn()} <> [ST].{a.SqlColumn()} {a.SqlCollate()} OR [ME].{a.SqlColumn()} IS NULL))")); 
                    break;
                case StagingRowActionEnum.Delete:
                    attrColumnsSelect = reportedAttributes.ToColumnList();
                    attrColumnsInsert = reportedAttributes.ToColumnList();
                    attrColumnsUpdate = string.Join(",\n", reportedAttributes
                        .Select(a => $"[ME].{a.SqlColumn()} = CASE WHEN [ST].{a.SqlColumn()} IS NOT NULL THEN NULL ELSE [ME].{a.SqlColumn()} END"));
                    updateDiffCheck = string.Join(" OR\n", reportedAttributes
                        .Select(a => $"([ST].{a.SqlColumn()} IS NOT NULL AND [ME].{a.SqlColumn()} IS NOT NULL)"));
                    break;
                case StagingRowActionEnum.Replace:
                    var allAttributesWithInclusion = dsd.Msd.MetadataAttributes
                        .Select(attribute => (reportedComponents.MetadataAttributes.Any(
                            r => r.HierarchicalId.Equals(attribute.HierarchicalId, StringComparison.InvariantCultureIgnoreCase)), attribute))
                        .ToList();

                    attrColumnsSelect = string.Join(",", allAttributesWithInclusion
                            .Select(a => a.Item1 ? a.attribute.SqlColumn() : $"NULL AS {a.attribute.SqlColumn()}"));
                    attrColumnsInsert = allAttributesWithInclusion.Select(a => a.attribute).ToColumnList();
                    attrColumnsUpdate = string.Join(",\n", allAttributesWithInclusion
                        .Select(a => @$"[ME].{a.attribute.SqlColumn()} =  {(a.Item1 ? $"[ST].{a.attribute.SqlColumn()}" : "NULL")}"));
                    updateDiffCheck = string.Join(" OR\n", allAttributesWithInclusion
                        .Select(a => a.Item1 ?
                            $"(([ST].{a.attribute.SqlColumn()} IS NOT NULL AND [ME].{a.attribute.SqlColumn()} IS NULL) OR ([ST].{a.attribute.SqlColumn()} IS NULL AND [ME].{a.attribute.SqlColumn()} IS NOT NULL) OR [ME].{a.attribute.SqlColumn()} <> [ST].{a.attribute.SqlColumn()} {a.attribute.SqlCollate()})" 
                            :  $"[ME].{a.attribute.SqlColumn()} IS NOT NULL"));
                    break;
            };
            
            if (!string.IsNullOrEmpty(updateDiffCheck))
                updateDiffCheck = $"AND ({updateDiffCheck})";

            //Calculate summary
            var summaryStatement = string.Empty;
            if (includeSummary)
            {
                summaryStatement = action == StagingRowActionEnum.Delete ?
                "SELECT 0 as [INSERTED], @updated as [UPDATED], @@ROWCOUNT - @updated as [DELETED], @@ROWCOUNT as [TOTAL];" :
                "SELECT @@ROWCOUNT - @updated as [INSERTED], @updated as [UPDATED], 0 AS [DELETED], @@ROWCOUNT as [TOTAL];";
            }
            else
            {
                summaryStatement = "SELECT CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END;";
            }

            var actionStatement = action == StagingRowActionEnum.Delete ?
//Delete actions
$@"WHEN MATCHED AND [ST].[REAL_ACTION] = @Merge {updateDiffCheck} THEN --Soft Delete set values to NULL
    UPDATE SET 
    {attrColumnsUpdate},
    [{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
    {(includeSummary ? ", @updated = @updated + 1" : "")} 
WHEN MATCHED AND [ST].[REAL_ACTION] = @Delete THEN DELETE; --Real delete" :
// Replace and Merge actions
$@"WHEN MATCHED {updateDiffCheck} THEN --Updates
    UPDATE SET 
    {attrColumnsUpdate},
    [{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
    {(includeSummary ? ", @updated = @updated + 1" : "")} 
WHEN NOT MATCHED THEN -- Inserts
    INSERT ({dimensionColumnsNoTimeDim}{timeDimColumns},{attrColumnsInsert}, [{DbExtensions.LAST_UPDATED_COLUMN}])
    VALUES ({dimensionColumnsNoTimeDimInsertValues}{timeDimColumnsInsertValues},{attrColumnsInsert}, @CurrentDateTime );";

            var stagingTableName = $"[{ DotStatDb.DataSchema}].[{ dsd.SqlMetadataStagingTable()}]";

            var sqlCommand = $@"
            {(includeSummary ? "declare @updated as int = 0;" : "")}

MERGE INTO {metadataTableName} [ME]
USING (
    SELECT {dimensionColumnsNoTimeDim}{timeDimColumns},{attrColumnsSelect}, [REAL_ACTION] FROM {stagingTableName} 
    WHERE [BATCH_NUMBER] = @BatchNumber AND [REAL_ACTION] <> @Skip
) [ST]
ON {dimensionColumnsNoTimeDimJoin}{timeDimColumnsJoin}
{constraint}
{actionStatement}
{summaryStatement}";

            //Add Sql parameters
            sqlParams.AddRange( new List<DbParameter>
            {
                new SqlParameter("CurrentDateTime", SqlDbType.DateTime) { Value = dateTime },
                new SqlParameter("Merge", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Merge },
                new SqlParameter("Delete", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Delete },
                new SqlParameter("Skip", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Skip },
                new SqlParameter("BatchNumber", SqlDbType.Int) { Value = batchNumber }
            });

            return await ExecuteMerge(sqlCommand, sqlParams, includeSummary, cancellationToken);
        }

        public override ValueTask<MergeResult> MergeStagingToSeriesTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents,
            DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            return new ValueTask<MergeResult>(new MergeResult());
        }

        public override async ValueTask<MergeResult> MergeStagingToDatasetTable(IImportReferenceableStructure referencedStructure,
            IList<IKeyValue> attributes, CodeTranslator codeTranslator, DbTableVersion tableVersion, StagingRowActionEnum action,
            bool includeSummary, DateTime dateTime, CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow dataFlow)
            {
                var metadataTableName = $"[{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataDataSetTable((char)tableVersion)}]";

                return await MergeStagingToDatasetTable(dataFlow.Dsd, dataFlow.DbId, metadataTableName,
                    attributes, action, includeSummary, dateTime, cancellationToken);
            }
            else
            {
                var dsd = referencedStructure as Dsd;
                var metadataTableName = $"[{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataSetTable((char)tableVersion)}]";

                return await MergeStagingToDatasetTable(dsd, -1, metadataTableName,
                    attributes, action, includeSummary, dateTime, cancellationToken);
            }
        }

        protected override async Task StoreDeleteInstructions(
            IImportReferenceableStructure referencedStructure,
            ReportedComponents reportedComponents,
            IList<DataSetAttributeRow> dataSetAttributeRows,
            DbTableVersion tableVersion,
            DateTime dateTime,
            CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow dataFlow)
            {
                var metadataTable = dataFlow.SqlMetadataDataFlowTable((char)tableVersion);
                await StoreDeleteInstructions(dataFlow.Dsd, dataFlow.DbId, metadataTable, dataSetAttributeRows, tableVersion,
                    dateTime, cancellationToken);
            }
            else
            {
                var dsd = referencedStructure as Dsd;
                var metadataTable = dsd.SqlMetadataDataStructureTable((char)tableVersion);
                await StoreDeleteInstructions(dsd, -1, metadataTable, dataSetAttributeRows, tableVersion,
                    dateTime, cancellationToken);
            }
        }

        private async ValueTask<MergeResult> MergeStagingToDatasetTable(Dsd dsd, int dfId, string metadataTableName, IList<IKeyValue> attributes, 
            StagingRowActionEnum action, bool includeSummary, DateTime dateTime, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();

            if (action is StagingRowActionEnum.Skip or StagingRowActionEnum.DeleteAll)
            {
                return mergeResult;
            }

            if (!attributes.Any() && action != StagingRowActionEnum.Replace)
            {
                return mergeResult;
            }


            var referencedMetadataAttributes = new Dictionary<MetadataAttribute, object>();
            foreach (var attributeKeyValue in attributes)
            {
                var attribute = dsd.Msd.MetadataAttributes.First(a => a.HierarchicalId.Equals(attributeKeyValue.Concept, StringComparison.InvariantCultureIgnoreCase));

                object attributeValue;
                if (string.IsNullOrWhiteSpace(attributeKeyValue.Code))
                {
                    attributeValue = (object)DBNull.Value;
                }
                else
                {
                    attributeValue = action == StagingRowActionEnum.Delete
                        ? DbExtensions.ColumnPresentDbValue.ToString()
                        : attributeKeyValue.Code;
                }

                referencedMetadataAttributes[attribute] = attributeValue;
            }

            var allEmptyOrNull = referencedMetadataAttributes.Values.Distinct().Count() == 1;

            StagingRowActionEnum realAction;
            if (action == StagingRowActionEnum.Delete && 
                (referencedMetadataAttributes.Count == 0 || (allEmptyOrNull && referencedMetadataAttributes.Count == dsd.Msd.MetadataAttributes.Count)))
            {
                realAction = StagingRowActionEnum.Delete;
            }
            else
            {
                realAction = action == StagingRowActionEnum.Replace
                    ? StagingRowActionEnum.Replace
                    : StagingRowActionEnum.Merge;
            }

            if (realAction == StagingRowActionEnum.Delete)
            {
                if (includeSummary)
                {
                    mergeResult.DeleteCount += (int)await DotStatDb.ExecuteScalarSqlAsync(
                        $"DELETE FROM {metadataTableName} WHERE [DF_ID] = {dfId}; SELECT @@ROWCOUNT AS [ROW_COUNT];", cancellationToken);
                    mergeResult.TotalCount =
                        mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync(
                        $"DELETE FROM {metadataTableName} WHERE [DF_ID] = {dfId}", cancellationToken);
                }

                return mergeResult;
            }

            string updateDiffCheck = null;
            string attrColumnsSelect = null;
            string attrColumnsInsert = null;
            string attrColumnsUpdate = null;

            switch (action)
            {
                case StagingRowActionEnum.Merge:
                    attrColumnsSelect = string.Join(",", referencedMetadataAttributes.Select(kvp => $"@Comp{kvp.Key.DbId} AS {kvp.Key.SqlColumn()}"));
                    attrColumnsInsert = string.Join(",", referencedMetadataAttributes.Select(kvp => kvp.Key.SqlColumn()));
                    attrColumnsUpdate = string.Join(",\n", referencedMetadataAttributes
                        .Select(a => $"[ME].{a.Key.SqlColumn()} = CASE WHEN [Import].{a.Key.SqlColumn()} IS NOT NULL THEN [Import].{a.Key.SqlColumn()} ELSE [ME].{a.Key.SqlColumn()} END"));
                    updateDiffCheck = string.Join(" OR\n", referencedMetadataAttributes
                        .Select(a => $"([Import].{a.Key.SqlColumn()} IS NOT NULL AND ([ME].{a.Key.SqlColumn()} <> [Import].{a.Key.SqlColumn()} {a.Key.SqlCollate()} OR [ME].{a.Key.SqlColumn()} IS NULL))")); 
                    break;
                case StagingRowActionEnum.Delete:
                    attrColumnsSelect = string.Join(",", referencedMetadataAttributes.Select(kvp => $"@Comp{kvp.Key.DbId} AS {kvp.Key.SqlColumn()}"));
                    attrColumnsInsert = string.Join(",", referencedMetadataAttributes.Select(kvp => kvp.Key.SqlColumn()));
                    attrColumnsUpdate = string.Join(",\n", referencedMetadataAttributes
                        .Select(a => $"[ME].{a.Key.SqlColumn()} = CASE WHEN [Import].{a.Key.SqlColumn()} IS NOT NULL THEN NULL ELSE [ME].{a.Key.SqlColumn()} END"));
                    updateDiffCheck = string.Join(" OR\n", referencedMetadataAttributes
                        .Select(a => $"([Import].{a.Key.SqlColumn()} IS NOT NULL AND [ME].{a.Key.SqlColumn()} IS NOT NULL)"));
                    break;
                case StagingRowActionEnum.Replace:
                    var allAttributesWithInclusion = dsd.Msd.MetadataAttributes
                        .Select(attribute => (referencedMetadataAttributes.Any(ra => ra.Key.Code.Equals(attribute.Code, StringComparison.InvariantCultureIgnoreCase)), attribute)).ToList();

                    attrColumnsSelect = string.Join(",", allAttributesWithInclusion
                            .Select(a => a.Item1 ? $"@Comp{a.attribute.DbId} AS {a.attribute.SqlColumn()}" : $"NULL AS {a.attribute.SqlColumn()}"));
                    attrColumnsInsert = allAttributesWithInclusion.Select(a => a.attribute).ToColumnList();
                    attrColumnsUpdate = string.Join(",\n", allAttributesWithInclusion
                        .Select(a => @$"[ME].{a.attribute.SqlColumn()} =  {(a.Item1 ? $"[Import].{a.attribute.SqlColumn()}" : "NULL")}"));
                    updateDiffCheck = string.Join(" OR\n", allAttributesWithInclusion
                        .Select(a => a.Item1 ?
                            $"(([Import].{a.attribute.SqlColumn()} IS NOT NULL AND [ME].{a.attribute.SqlColumn()} IS NULL) OR ([Import].{a.attribute.SqlColumn()} IS NULL AND [ME].{a.attribute.SqlColumn()} IS NOT NULL) OR [ME].{a.attribute.SqlColumn()} <> [Import].{a.attribute.SqlColumn()} {a.attribute.SqlCollate()})" 
                            : $"[ME].{a.attribute.SqlColumn()} IS NOT NULL"));
                    break;
            };

            if (!string.IsNullOrEmpty(updateDiffCheck))
                updateDiffCheck = $"AND ({updateDiffCheck})";

            //Merge in memory values to DsdAttr table
            var actionStatement = action == StagingRowActionEnum.Delete
                ? //Delete actions
                    $@"WHEN MATCHED {updateDiffCheck} THEN --Soft Delete set values to NULL
                    UPDATE SET 
                    {attrColumnsUpdate}
                    ,[{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
                    {(includeSummary ? ", @updated = @updated + 1" : "")};"
                : // Replace and Merge actions
                    $@"WHEN MATCHED {updateDiffCheck} THEN --Updates
                    UPDATE SET 
                    {attrColumnsUpdate}
                    ,[{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
                    {(includeSummary ? ", @updated = @updated + 1" : "")} 
                    WHEN NOT MATCHED THEN -- Inserts
                    INSERT ([DF_ID], {attrColumnsInsert}, [{DbExtensions.LAST_UPDATED_COLUMN}] )
                    VALUES ([DF_ID], {attrColumnsInsert}, @CurrentDateTime );";


            var sqlCommand = $@"
                {(includeSummary ? "declare @updated as int = 0;" : string.Empty)}
                MERGE INTO {metadataTableName} AS [ME]
                USING (SELECT @DfId AS [DF_ID], {attrColumnsSelect}) AS [Import]
                ON [ME].[DF_ID] = [Import].[DF_ID]
                {actionStatement}
                {(includeSummary ? "SELECT @@ROWCOUNT - @updated AS [INSERTED], @updated as [UPDATED], 0 as [DELETED], @@ROWCOUNT as [TOTAL];" : "SELECT CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END;")}";

            var sqlParameters = new List<DbParameter>
            {
                new SqlParameter("CurrentDateTime", SqlDbType.DateTime) { Value = dateTime }
            };

            sqlParameters.AddRange(referencedMetadataAttributes
                .Select(kvp =>
                    new SqlParameter($"Comp{kvp.Key.DbId}", kvp.Key.GetSqlType())
                    {
                        Value = kvp.Value
                    } as DbParameter)
                .Concat(new[] {new SqlParameter("DfId", SqlDbType.Int) { Value = dfId },}).ToList());

            return await ExecuteMerge(sqlCommand, sqlParameters, includeSummary, cancellationToken);
        }

        protected override async Task DeleteAll(IImportReferenceableStructure referencedStructure,
            DbTableVersion tableVersion, bool includeSummary, ImportSummary importSummary,
            ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow dataFlow)
                await DeleteAll(dataFlow, tableVersion, includeSummary, importSummary, codeTranslator, dateTime,
                    cancellationToken);
            else
                await DeleteAll(referencedStructure as Dsd, tableVersion, includeSummary, importSummary, codeTranslator, dateTime,
                    cancellationToken);
        }

        private async Task DeleteAll(Dataflow dataflow, DbTableVersion tableVersion, bool includeSummary, ImportSummary importSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            var allDimConstraint = Predicate.BuildWhereForObservationLevel(dataQuery: null, dataflow, codeTranslator, useExternalValues: false, nullTreatment: NullTreatment.IncludeNullsAndSwitchOff, includeTimeConstraints: true);
            if (!string.IsNullOrEmpty(allDimConstraint))
            {
                allDimConstraint = "WHERE " + allDimConstraint;
            }

            var mergeResult = await DeleteAll(dataflow.SqlMetadataDataFlowTable((char)tableVersion), allDimConstraint, includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.ObservationLevelMergeResult.Add(mergeResult);

            mergeResult = await DeleteAll(dataflow.Dsd.SqlMetadataDataSetTable((char)tableVersion), null, includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.DataFlowLevelMergeResult.Add(mergeResult);

            Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteAllMetadataActionPerformed));

            await InsertDeletedAllRow(dataflow.DbId, dataflow.Dsd.SqlDeletedMetadataTable((char)tableVersion), dateTime, cancellationToken);

        }

        private async Task DeleteAll(Dsd dsd, DbTableVersion tableVersion, bool includeSummary, ImportSummary importSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            var mergeResult = await DeleteAll(dsd.SqlMetadataDataStructureTable((char)tableVersion), filterAllowedContentConstraint:"", includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.ObservationLevelMergeResult.Add(mergeResult);
            
            mergeResult = await DeleteAll(dsd.SqlMetadataDataSetTable((char)tableVersion), null, includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.DataFlowLevelMergeResult.Add(mergeResult);

            Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteAllMetadataActionPerformed));

            await InsertDeletedAllRow(dataFlowId: -1, dsd.SqlDeletedMetadataTable((char)tableVersion), dateTime, cancellationToken);
        }

        public async Task UpdateStatisticsOfMetadataTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlAsync($@"UPDATE STATISTICS [{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataStructureTable((char)tableVersion)}] WITH FULLSCAN, ALL", cancellationToken);
        }

        public async Task DropMetadataTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DropMetadataTables(dsd, DbTableVersion.A, cancellationToken);
            await DropMetadataTables(dsd, DbTableVersion.B, cancellationToken);
        }

        public async Task DropMetadataTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlMetadataDataStructureTable((char)tableVersion), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlMetadataDataSetTable((char)tableVersion), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDeletedMetadataTable((char)tableVersion), cancellationToken);
        }
        
        public async Task TruncateMetadataTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlMetadataDataStructureTable((char)tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlMetadataDataSetTable((char)tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlDeletedMetadataTable((char)tableVersion), cancellationToken);
        }
        
        #region MappingSetParams

        public MappingSetParams GetMappingSetParams(Dataflow dataflow, char? tableVersion)
        {
            var emptyMetaQuery = this.GetEmptyMetadataViewQuery(dataflow);

            return new MappingSetParams()
            {
                IsMetadata = true,
                ActiveQuery = tableVersion.HasValue ? GetMetadataViewQuery(dataflow, tableVersion.Value) : emptyMetaQuery,
                ReplaceQuery = tableVersion.HasValue ? GetMetadataViewQuery(dataflow, tableVersion.Value) : emptyMetaQuery,
                DeleteQuery = tableVersion.HasValue ? GetDeletedMetadataViewQuery(dataflow, tableVersion.Value) : emptyMetaQuery,
                IncludeHistoryQuery = null,
                OrderByTimeAsc = null,
                OrderByTimeDesc = null
            };
        }


        public async Task TruncateMetadataTables(Dataflow dataFlow, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dataFlow.SqlMetadataDataFlowTable((char)tableVersion), cancellationToken);
        }

        public string GetMetadataViewQuery(Dataflow dataFlow, char tableVersion)
        {
            if (dataFlow.Dsd.Msd == null)
                throw new ArgumentException($"There is no linked MSD for the DSD of the parameter 'dataFlow'");

            var columns = new List<string>();

            foreach (var dim in dataFlow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"[TIME_PERIOD] AS [{dim.Code}]");
                    columns.Add($"[{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"[{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"[{dim.Code}]");
                }
            }

            columns.AddRange(dataFlow.Dsd.Msd.MetadataAttributes.Select(attr => $"[{attr.HierarchicalId}]"));
            columns.Add($"[{DbExtensions.LAST_UPDATED_COLUMN}]");

            return $"SELECT {string.Join(",", columns)} FROM [{DotStatDb.DataSchema}].[{dataFlow.SqlMetadataDataFlowViewName(tableVersion)}]";
        }

        public string GetDeletedMetadataViewQuery(Dataflow dataFlow, char tableVersion)
        {
            if (dataFlow.Dsd.Msd == null)
                throw new ArgumentException($"There is no linked MSD for the DSD of the parameter 'dataFlow'");

            var columns = new List<string>();

            foreach (var dim in dataFlow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"[TIME_PERIOD] AS [{dim.Code}]");
                    columns.Add($"[{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"[{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"[{dim.Code}]");
                }
            }

            columns.AddRange(dataFlow.Dsd.Msd.MetadataAttributes.Select(attr => $"[{attr.HierarchicalId}]"));
            columns.Add($"[{DbExtensions.LAST_UPDATED_COLUMN}]");

            return $"SELECT {string.Join(",", columns)} FROM [{DotStatDb.DataSchema}].[{dataFlow.SqlDeletedMetadataDataFlowViewName(tableVersion)}]";
        }

        public string GetEmptyMetadataViewQuery(Dataflow dataFlow)
        {
            if (dataFlow.Dsd.Msd == null)
                throw new ArgumentException($"There is no linked MSD for the DSD of the parameter 'dataFlow'");

            var columns = new List<string>();

            foreach (var dim in dataFlow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"NULL AS [{dim.Code}]");
                    columns.Add($"NULL AS [{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"NULL AS [{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"NULL AS [{dim.Code}]");
                }
            }

            columns.AddRange(dataFlow.Dsd.Msd.MetadataAttributes.Select(attr => $"NULL AS [{attr.HierarchicalId}]"));
            columns.Add($"NULL as [{DbExtensions.LAST_UPDATED_COLUMN}]");

            return "SELECT TOP 0 " + string.Join(",", columns);
        }

        #endregion

        public async Task<bool> MetadataTablesExist(Dsd dsd, CancellationToken cancellationToken)
        {
            return await DotStatDb.TableExists(dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A), cancellationToken) || await DotStatDb.TableExists(dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B), cancellationToken);
        }
        public async Task<bool> DataFlowMetadataTablesExist(Dataflow dataflow, CancellationToken cancellationToken)
        {
            return await DotStatDb.TableExists(dataflow.SqlMetadataDataFlowTable((char)DbTableVersion.A), cancellationToken) || await DotStatDb.TableExists(dataflow.SqlMetadataDataFlowTable((char)DbTableVersion.B), cancellationToken);
        }

        protected override async Task<bool> CleanUpFullyEmptyObservations(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            var dataFlow = referencedStructure as Dataflow;
            var dsd = referencedStructure.Dsd;

            if (!dsd.Msd.MetadataAttributes.Any())
            {
                return false;
            }

            var attributeColumns = dsd.Msd.MetadataAttributes.ToColumnListWithIsNull();

            if (!string.IsNullOrEmpty(attributeColumns))
            {
                if (dataFlow is not null)
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM [{DotStatDb.DataSchema}].[{dataFlow.SqlMetadataDataFlowTable((char)tableVersion)}] WHERE {attributeColumns}", cancellationToken);
                    await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataDataSetTable((char)tableVersion)}] WHERE {attributeColumns} AND [DF_ID]= {dataFlow.DbId}", cancellationToken);
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM [{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataStructureTable((char)tableVersion)}] WHERE {attributeColumns}", cancellationToken);
                    await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM [{DotStatDb.DataSchema}].[{dsd.SqlMetadataDataSetTable((char)tableVersion)}] WHERE {attributeColumns} AND [DF_ID]= -1", cancellationToken);
                }
            }

            return false;
        }

        private async ValueTask BuildMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            if (dsd?.Msd == null)
                return;

            var tableName = dsd.SqlMetadataStagingTable();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced.
            var dimensionColumns = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension)
                .ToColumnList(true, true);
            var metadataAttributeColumns = dsd.Msd.MetadataAttributes.OrderBy(a => a.DbId).ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metadataAttributeColumns))
            {
                metadataAttributeColumns += ',';
            }

            var rowId = dsd.Dimensions.Count > 32 ?
                $"[ROW_ID] AS ({dsd.SqlBuildRowIdFormula()}) PERSISTED NOT NULL,"
                : null;

            var timeDim = dsd.TimeDimension;
            // NULL is stored when time is not referenced by the metadata.
            var sdmxDimTimeColumn = timeDim != null
                ? new[] { timeDim }.ToColumnList(true, true) + "," : null;

            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();
            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, true),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, true)
                ) + ","
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync($@"CREATE TABLE [{DotStatDb.DataSchema}].[{tableName}](
                    {rowId}
                    {timeDimensionColumns}
                    {sdmxDimTimeColumn}
                    {dimensionColumns},
                    {metadataAttributeColumns}
                    [REQUESTED_ACTION] [TINYINT] NOT NULL,
                    [REAL_ACTION] [TINYINT] NOT NULL,
                    [BATCH_NUMBER] [INT] NULL
                )", cancellationToken);
        }

        private void OnSqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            var batchNumber = e.RowsCopied / DotStatDb.DataSpace.NotifyImportBatchSize;
            Log.Notice(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ProcessingMetadataAttributes),
                e.RowsCopied, batchNumber));
        }

        private async Task<MergeResult> ExecuteMerge(string sqlCommand, List<DbParameter> sqlParams, bool includeSummary, CancellationToken cancellationToken, Dsd dsd = null, ReportedComponents reportedComponents = null)
        {
            var mergeResult = new MergeResult();
            if (string.IsNullOrEmpty(sqlCommand))
                return mergeResult;

            try
            {
                if (includeSummary)
                {
                    await using var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow, parameters: sqlParams.ToArray());

                    if (await dbDataReader.ReadAsync(cancellationToken))
                    {
                        mergeResult.InsertCount = dbDataReader.GetInt32(0);
                        mergeResult.UpdateCount = dbDataReader.GetInt32(1);
                        mergeResult.DeleteCount = dbDataReader.GetInt32(2);
                        mergeResult.TotalCount = dbDataReader.GetInt32(3);
                    }
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, sqlParams.ToArray());
                }
            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 2627://duplicates in the staging table insert
                        mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInMetadataStagingTable });
                        break;
                    case 8672://Overlapping rows or duplicates
                        mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.MultipleRowsModifyingTheSameRegionInStagingTable });
                        break;
                    default:
                        throw;

                }
            }

            return mergeResult;
        }

    }
}