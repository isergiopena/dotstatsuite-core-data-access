﻿using DotStat.Db.DB;
using DotStat.Db.Repository;

namespace DotStat.DB.Repository
{
    public interface IUnitOfWork
    {
        public string DataSpace { get; }
        public int MaxDataImportTimeInMinutes { get; }
        public int DatabaseCommandTimeout { get; }

        public IDotStatDb DotStatDb { get; }
        public IArtefactRepository ArtefactRepository { get; }
        public IAttributeRepository AttributeRepository { get; }
        //public IAuthorizationRepository AuthorizationRepository { get; }
        public IDataStoreRepository DataStoreRepository { get; }
        public IMetadataComponentRepository MetaMetadataComponentRepository { get; }
        public IMetadataStoreRepository MetadataStoreRepository { get; }
        public IObservationRepository ObservationRepository { get; }
        public ITransactionRepository TransactionRepository { get; }
        public IComponentRepository ComponentRepository { get; }
        public ICodelistRepository CodeListRepository { get; }
    }
    
}
