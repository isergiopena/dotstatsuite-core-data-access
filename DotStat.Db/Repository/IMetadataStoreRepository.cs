﻿using DotStat.Db.Util;
using DotStat.DB.Repository;
using DotStat.DB.Util;
using DotStat.Domain;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Db.Repository
{
    public interface IMetadataStoreRepository : IDatabaseRepository, IDataMerger
    {
        Task<BulkImportResult> BulkInsertMetadata(
            IAsyncEnumerable<ObservationRow> observations, 
            ReportedComponents reportedComponents, 
            ICodeTranslator translator, IImportReferenceableStructure referencedStructure, 
            bool fullValidation, bool isTimeAtTimeDimensionSupported,
            CancellationToken cancellationToken
            );
        
        Task RecreateMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken);

        Task AddIndexMetadataStagingTable(Dsd dsd, CancellationToken cancellationToken);
        Task DropMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken);
        Task DropMetadataStagingTables(int dsdDbId, CancellationToken cancellationToken);
        Task AddUniqueIndexMetadataStagingTable(Dsd dsd, CancellationToken cancellationToken);

        Task DeleteDsdMetadata(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);
        Task DeleteDataFlowMetadata(Dataflow dataFlow, DbTableVersion tableVersion, CancellationToken cancellationToken);

        ValueTask CopyDsdMetadataToNewVersion(Dsd dsd, DbTableVersion sourceTableVersion, DbTableVersion targetTableVersion, CancellationToken cancellationToken);
        ValueTask CopyDataFlowMetadataToNewVersion(Dataflow dataFlow, DbTableVersion sourceTableVersion, DbTableVersion targetTableVersion, CancellationToken cancellationToken);


        Task UpdateStatisticsOfMetadataTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);

        Task DropMetadataTables(Dsd dsd, CancellationToken cancellationToken);

        string GetMetadataViewQuery(Dataflow dataFlow, char tableVersion);
        string GetDeletedMetadataViewQuery(Dataflow dataFlow, char tableVersion);

        string GetEmptyMetadataViewQuery(Dataflow dataFlow);
        Task DropMetadataTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);
        MappingSetParams GetMappingSetParams(Dataflow dataflow, char? tableVersion);
        Task<bool> MetadataTablesExist(Dsd dsd, CancellationToken cancellationToken);
        Task<bool> DataFlowMetadataTablesExist(Dataflow dataflow, CancellationToken cancellationToken);
        Task TruncateMetadataTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);
    }
}
