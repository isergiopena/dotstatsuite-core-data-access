﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine
{
    public abstract class DataflowEngineBase<TDotStatDb> : ArtefactEngineBase<Dataflow, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected DataflowEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<bool> CreateDynamicDbObjects(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Set A
            await BuildDynamicDataDataFlowView(dataflow, (char) DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicDataReplaceDataFlowView(dataflow, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDeletedView(dataflow, (char)DbTableVersion.A, dotStatDb, cancellationToken);

            //Set B
            await BuildDynamicDataDataFlowView(dataflow, (char) DbTableVersion.B, dotStatDb, cancellationToken);
            await BuildDynamicDataReplaceDataFlowView(dataflow, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            await BuildDeletedView(dataflow, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            
            if (!dataflow.Dsd.KeepHistory) return true;
            
            await CreateHistoryViews(dataflow, dotStatDb, cancellationToken);

            return true;
        }

        public override async Task CleanUp(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (dataflow.DbId > 0)
            {
                await DeleteFromArtefactTableByDbId(dataflow.DbId, dotStatDb, cancellationToken);
            }
        }

        public async Task CreateHistoryViews(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Set A
            await BuildDynamicDataIncludeHistoryDataFlowView(dataflow, (char)DbTableVersion.A, dotStatDb, cancellationToken);

            //Set B
            await BuildDynamicDataIncludeHistoryDataFlowView(dataflow, (char)DbTableVersion.B, dotStatDb, cancellationToken);
        }

        protected abstract Task BuildDynamicDataDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        protected abstract Task BuildDynamicDataReplaceDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        
        protected abstract Task BuildDeletedView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        
        protected abstract Task BuildDynamicDataIncludeHistoryDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
    }
}