﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlMetadataDataflowEngine : MetadataDataflowEngineBase<SqlDotStatDb>
    {

        public SqlMetadataDataflowEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task<int> InsertToArtefactTable(Dataflow artefact, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
        protected override async Task BuildDynamicMetadataDataFlowTable(Dataflow dataflow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"CL_{dataflow.Dsd.Dimensions.First(x => !x.Base.TimeDimension).Codelist.DbId}", cancellationToken))
                return;

            //The table already exists
            if (await dotStatDb.TableExists($"{dataflow.SqlMetadataDataFlowTable(targetVersion)}", cancellationToken))
            {
                return;
            }

            var dimensions = dataflow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced
            var dimensionColumnsWithType = dimensions.ToColumnList(true);
            var dimensionColumns = dimensions.ToColumnList();
            var table = dataflow.SqlMetadataDataFlowTable(targetVersion);

            var timeDim = dataflow.Dsd.TimeDimension;
            var supportsDateTime = dataflow.Dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    // NULL is stored when time is not referenced by the metadata.
                    new[] { timeDim }.ToColumnList(true, true),
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, false),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, false)
                ) + ","
                : null;

            var timeConstraint =
                timeDim != null ? $", [{DbExtensions.SqlPeriodStart()}], [{DbExtensions.SqlPeriodEnd()}] DESC" : null;

            string rowId = null;
            var uIndex = dimensionColumns + timeConstraint;

            //Time dimension requires two columns (period_start and period_end)
            if (dataflow.Dsd.Dimensions.Count > (timeDim != null ? 30 : 32))
            {
                rowId = $"[ROW_ID] AS ({dataflow.Dsd.SqlBuildRowIdFormula()}) PERSISTED NOT NULL,";
                uIndex = "[ROW_ID]";
            }

            var metaAttributeColumns = dataflow.Dsd.Msd.MetadataAttributes
                .ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var constraint = dataflow.Dsd.DataCompression != DataCompressionEnum.NONE ? "" : $", CONSTRAINT [PK_{table}] PRIMARY KEY CLUSTERED ({uIndex})";

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        {rowId}
	                    {dimensionColumnsWithType},
                        {timeDimensionColumns}
                        {metaAttributeColumns}
                        [{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
                        {constraint}
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dataflow.Dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{table}
ON [{dotStatDb.DataSchema}].[{table}]
WITH(DATA_COMPRESSION = {dataflow.Dsd.DataCompression})", cancellationToken);
            }

        }

        protected override async Task<bool> BuildDynamicMetadataDataFlowView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dataFlow.SqlMetadataDataFlowTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dataFlow.Dsd.SqlMetadataDataSetTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dataFlow.SqlMetadataDataFlowViewName(targetVersion)}", cancellationToken))
                return false;

            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            var dfSelectStatement = new StringBuilder();
            var dsSelectStatement = new StringBuilder();
            var joinDsdDf = new StringBuilder();

            //dimensions
            selectStatement.Append(
                string.Join(", ", dimensions.Select(d => d.SqlColumn(externalColumn: true))));

            dfSelectStatement.Append(string.Join(", ", dimensions.Select(d =>
                    d.Base.HasCodedRepresentation() ?
                        $"CASE WHEN [CL_{d.Code}].[ID] IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [CL_{d.Code}].[ID] END AS [{d.Code}]"
                        : $"CASE WHEN [ME].{d.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [ME].{d.SqlColumn()} END AS [{d.Code}]")));

            dsSelectStatement.Append(string.Join(", ", dimensions.Select(d => $"'{DbExtensions.DimensionSwitchedOff}' AS [{d.Code}]")));
            joinDsdDf.Append(string.Join(" AND ", dimensions.Select(d => $"DF.[{d.Code}] = DSD.[{d.Code}]")));

            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var (sqlMin, sqlMax) = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MinValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.ToString("yyyy-MM-dd"));

            if (timeDim != null)
            {
                selectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        @$"[{dataFlow.Dsd.Base.TimeDimension.Id}],
                        [{DbExtensions.SqlPeriodStart()}],
                        [{DbExtensions.SqlPeriodEnd()}]"
                    ));

                dfSelectStatement
                    .Append(",")
                .Append(string.Join(",",
                        $"CASE WHEN {timeDim.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE {timeDim.SqlColumn()} END AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()
                    ));

                dsSelectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        $"'{DbExtensions.DimensionSwitchedOff}' AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        $"'{sqlMax}' AS [{DbExtensions.SqlPeriodStart()}]",
                        $"'{sqlMin}' AS [{DbExtensions.SqlPeriodEnd()}]"
                    ));

                joinDsdDf.Append(
                    $" AND DF.[{dataFlow.Dsd.Base.TimeDimension.Id}] = DSD.[{dataFlow.Dsd.Base.TimeDimension.Id}]");
            }

            //metadata attributes
            if (dataFlow.Dsd.Msd.MetadataAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"[{a.HierarchicalId}]"
                )));

                dfSelectStatement
                    .Append(",")
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} AS [{a.HierarchicalId}]"
                    )));

                dsSelectStatement
                    .Append(',')
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"[ME].{a.SqlColumn()} AS [{a.HierarchicalId}]"
                )));
            }

            // LAST_UPDATED
            var updatedAfterColumns = $", [{DbExtensions.LAST_UPDATED_COLUMN}]";

            // JOIN ------------------------------------------------------------------
            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [ME].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            ));

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlMetadataDataFlowViewName(targetVersion)} 
AS 
WITH [DF_LEVEL] AS ( 
    SELECT {dsSelectStatement}{updatedAfterColumns}
    FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlMetadataDataSetTable(targetVersion)} ME
    WHERE DF_ID = {dataFlow.DbId}
    UNION ALL 
    SELECT {dfSelectStatement}{updatedAfterColumns}
    FROM [{dotStatDb.DataSchema}].{dataFlow.SqlMetadataDataFlowTable(targetVersion)} ME
    {joinStatement}
)
 SELECT {selectStatement}{updatedAfterColumns} FROM DF_LEVEL DF
 UNION ALL
 SELECT {selectStatement}{updatedAfterColumns} FROM [{dotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetaDataDsdViewName(targetVersion)}] DSD
 WHERE NOT EXISTS ( 
		SELECT TOP 1 1 FROM DF_LEVEL DF 
        WHERE {joinDsdDf}
)";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            return true;
        }

        protected override Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override async Task<bool> BuildDeletedView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dataFlow.Dsd.SqlDeletedMetadataTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dataFlow.SqlDeletedMetadataDataFlowViewName(targetVersion)}", cancellationToken))
                return false;

            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions
                .Where(dim => !dim.Base.TimeDimension)
                .ToList();

            //Time dimension columns
            var innerTimeDimensionColumns = dataFlow.Dsd.TimeDimension != null
                    ? ", " + string.Join(", ",
                            $"[{DbExtensions.TimeDimColumn}]",
                            DbExtensions.SqlPeriodStart(),
                            DbExtensions.SqlPeriodEnd()
                        )
                    : string.Empty;

            var innerTimeDimensionColumns4GroupBy = innerTimeDimensionColumns;

            var (sqlMax, sqlMaxMinOne) = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MaxValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.AddYears(-1).ToString("yyyy-MM-dd"));

            var outerTimeDimensionColumns = dataFlow.Dsd.TimeDimension != null
                ? ", " + string.Join(", ",
                    @$"CASE 
                            WHEN [DEL].[{DbExtensions.SqlPeriodStart()}] = '{sqlMax}' THEN '{DbExtensions.DimensionSwitchedOff}'
                            WHEN [DEL].[{DbExtensions.SqlPeriodStart()}] = '{sqlMaxMinOne}' THEN '{DbExtensions.DimensionWildCarded}'
                            ELSE [DEL].[{DbExtensions.TimeDimColumn}]
                      END AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                    $"[{DbExtensions.SqlPeriodStart()}]",
                    $"[{DbExtensions.SqlPeriodEnd()}]")
                : string.Empty;

            //Dimensions
            var innerDimensionsColumns = dimensions.Any()
                        ? ", " + string.Join(", ", dimensions.Select(d => $"{d.SqlColumn()}"))
                        : string.Empty;

            var outerDimensionsColumns = dimensions.Any()
                ? ", " +
                  string.Join(
                      ", ",
                      dimensions.Select(d =>
                          (d.Base.HasCodedRepresentation()
                              ? @$"CASE
                            WHEN [DEL].{d.SqlColumn()} = {DbExtensions.DimensionSwitchedOffDbValue} THEN '{DbExtensions.DimensionSwitchedOff}'
                            WHEN [DEL].{d.SqlColumn()} = {DbExtensions.DimensionWildCardedDbValue} THEN '{DbExtensions.DimensionWildCarded}'
                            ELSE [CL_{d.Code}].[ID]
                        END"
                              : $"{d.SqlColumn()}") + $" AS [{d.Code}]"
                      )
                  )
                : string.Empty;

            //Attributes
            var innerAttributeColumns4SomeComp = dataFlow.Dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"MAX( {a.SqlColumn()} ) AS [{a.HierarchicalId}]"))
                : string.Empty;

            var innerAttributeColumns4NoComp = dataFlow.Dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"NULL AS [{a.HierarchicalId}]"))
                : string.Empty;

            var outerAttributeColumns = dataFlow.Dsd.Msd.MetadataAttributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $" [{a.HierarchicalId}]"))
                : string.Empty;

            var joinStatements = string.Join(Environment.NewLine, dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [DEL].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            );

            var innerSelectPartCommon =
$@"SELECT MAX( {DbExtensions.LAST_UPDATED_COLUMN} ) AS {DbExtensions.LAST_UPDATED_COLUMN}
    {innerTimeDimensionColumns}
    {innerDimensionsColumns}";

            var innerFromPart = $@"FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDeletedMetadataTable(targetVersion)} ";

            var innerWherePart4NoComp = $@"WHERE [DF_ID] = {dataFlow.DbId} " +
                                        (dataFlow.Dsd.Msd.MetadataAttributes.Any()
                                            ? Environment.NewLine + "AND " + string.Join(" AND ",
                                                dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} IS NULL"))
                                            : string.Empty) + Environment.NewLine;

            var innerWherePart4SomeComp = $@"WHERE [DF_ID] = {dataFlow.DbId} " +
                                          "AND (" +
                                          (dataFlow.Dsd.Msd.MetadataAttributes.Any()
                                              ? Environment.NewLine + string.Join(" OR ",
                                                  dataFlow.Dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} IS NOT NULL"))
                                              : "1 = 1") + " )";

            var innerGroupByPart = $@"GROUP BY {innerDimensionsColumns.TrimStart(',')}{innerTimeDimensionColumns4GroupBy} ";

            var innerUnionSql =
                $@"{innerSelectPartCommon}{innerAttributeColumns4SomeComp}
   {innerFromPart} 
   {innerWherePart4SomeComp}
   {innerGroupByPart}
UNION
   {innerSelectPartCommon}{innerAttributeColumns4NoComp}
   {innerFromPart}
   {innerWherePart4NoComp}
   {innerGroupByPart}";

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDeletedMetadataDataFlowViewName(targetVersion)} 
                   AS
                       SELECT {DbExtensions.LAST_UPDATED_COLUMN}
                            {outerTimeDimensionColumns}
                            {outerDimensionsColumns}
                            {outerAttributeColumns}
                       FROM ( {innerUnionSql} ) AS [DEL]
                            {joinStatements}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            return true;
        }

    }
}