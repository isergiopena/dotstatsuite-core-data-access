﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine
{
    public abstract class DimensionEngineBase<TDotStatDb> : ComponentEngineBase<Dimension, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected DimensionEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task CleanUp(Dimension component, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (component.DbId < 1)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DimensionInvalidDbId),
                    component.Code, component.DbId)
                ); 
            }

            await DeleteFromComponentTableByDbId(component.DbId, dotStatDb , cancellationToken);
        }
    }
}
