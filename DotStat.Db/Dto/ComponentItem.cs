﻿using DotStat.Common.Enums;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using System;
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Db.Dto
{
    [ExcludeFromCodeCoverage]
    public class ComponentItem
    {
        public int DbId { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public int DsdId { get; set; }
        public int? CodelistId { get; set; }
        public AttributeAttachmentLevel AttributeAssignmentlevel { get; set; }
        public AttributeAssignmentStatus AttributeStatus { get; set; }
        public long? EnumId { get; set; }
        public string AttributeGroup { get; set; }
        public int? MinLength { get; set; }
        public int? MaxLength { get; set; }
        public string Pattern { get; set; }
        public int? MinValue { get; set; }
        public int? MaxValue { get; set; }

        public bool IsMandatory => AttributeStatus == AttributeAssignmentStatus.Mandatory;
        public bool IsCoded => CodelistId.HasValue;

        public bool IsDimension =>
            Type.Equals(DbTypes.GetDbType(SDMXArtefactType.Dimension), StringComparison.InvariantCultureIgnoreCase) ||
            Type.Equals(DbTypes.GetDbType(SDMXArtefactType.TimeDimension), StringComparison.InvariantCultureIgnoreCase);
        public bool IsAttribute => Type.Equals(DbTypes.GetDbType(SDMXArtefactType.DataAttribute), StringComparison.InvariantCultureIgnoreCase);
        public bool IsPrimaryMeasure => Type.Equals(DbTypes.GetDbType(SDMXArtefactType.PrimaryMeasure), StringComparison.InvariantCultureIgnoreCase);
        public bool IsTimeDimension => Type.Equals(DbTypes.GetDbType(SDMXArtefactType.TimeDimension), StringComparison.InvariantCultureIgnoreCase);
    }
}
