using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Dto;
using DotStat.Db.Exception;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.DB.Repository;
using DotStat.MappingStore;
using Transaction = DotStat.Domain.Transaction;
using TransactionStatus = DotStat.Domain.TransactionStatus;
using DotStat.Common.Exceptions;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model;
using Constraint = DotStat.Domain.Constraint;
using Dataflow = DotStat.Domain.Dataflow;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;

namespace DotStat.Db.Service
{
    public delegate IDotStatDbService DotStatDbServiceResolver(string dataSpace);

    [ExcludeFromCodeCoverage]
    public class DotStatDbService : IDotStatDbService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGeneralConfiguration _generalConfiguration;

        public DotStatDbService(IUnitOfWork unitOfWork, IGeneralConfiguration generalConfiguration)
        {
            _unitOfWork = unitOfWork;
            _generalConfiguration = generalConfiguration;
        }

        public async Task<bool> TryNewTransaction(Transaction transaction, IImportReferenceableStructure importReferenceStructure, IMappingStoreDataAccess mappingStoreDataAccess,
                CancellationToken cancellationToken, bool onlyLockTransaction = false)
        {
            if (importReferenceStructure == null)
            {
                throw new ArgumentNullException(nameof(importReferenceStructure));
            }

            var initialTableVersion = await InitDsdArtifact(transaction, importReferenceStructure.Dsd, !onlyLockTransaction, cancellationToken);

            //Could not get or assign the target table version
            if (initialTableVersion is null)
                return false;

            var transactionLocked = await LockTransaction(transaction, (DbTableVersion)initialTableVersion, cancellationToken, onlyLockTransaction);
            if (onlyLockTransaction) return transactionLocked;

            await SetLockedDbTableVersion(transaction, importReferenceStructure.Dsd, (DbTableVersion)initialTableVersion, cancellationToken);

            var isRollbackOrRestore = transaction.Type == TransactionType.Restore ||
                                      transaction.Type == TransactionType.Rollback;
            try
            {
                if (!isRollbackOrRestore)
                {
                    using (var transactionScope = _unitOfWork.DotStatDb.GetTransactionScope(true)) // TODO: to be reviewed at https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/335
                    {
                        await this.FillDbIdsAndCreateMissingDbObjects(importReferenceStructure, mappingStoreDataAccess, cancellationToken);
                        transactionScope.Complete();
                    }
                }
                else
                {
                    await FillIdsFromDisseminationDb(importReferenceStructure, cancellationToken);
                }
            }
            catch (DBConcurrencyException ex)
            {
                //Concurrent process creating and filling a code list reference by the current importReferenceStructure.
                Log.Error(ex);
                return false;
            }
            catch (ChangeInDsdException ex)
            {
                //Change detected in DSD between metastore and management database 
                Log.Error(ex);
                return false;
            }
            catch (System.Exception ex)
            {
                Log.Error(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates a transaction without a DSD reference. Applies -1 as DSD db id in TRANSACTION table.
        /// </summary>
        /// <param name="transaction">Transaction</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> TryNewTransactionWithNoDsd(Transaction transaction, CancellationToken cancellationToken, bool onlyLockTransaction = false)
        {
            return await TryNewSimpleTransaction(transaction, -1, null, onlyLockTransaction, cancellationToken);
        }

        public async Task<bool> TryNewTransactionForCleanup(Transaction transaction, int dsdId, int? dsdChildDbId, string agency, string id, SdmxVersion version, CancellationToken cancellationToken, bool onlyLockTransaction = false)
        {
            if (string.IsNullOrWhiteSpace(agency))
            {
                throw new ArgumentNullException(nameof(agency));
            }
            if (string.IsNullOrWhiteSpace(agency))
            {
                throw new ArgumentNullException(nameof(agency));
            }

            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            if (version == null)
            {
                throw new ArgumentNullException(nameof(version));
            }

            Log.Debug($"TryNewTransaction for cleanup");

            //var dsdId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(agency, id, version, SDMXArtefactType.Dsd, cancellationToken);

            if (dsdId == -1)
            {
                Log.Debug("Artefact is not in ARTEFACT table, nothing to do");

                return false;
            }
            var dsd = new ArtefactItem() { Agency = agency, Id = id, Version = version.ToString() };

            Log.Debug("Artefact already in ARTEFACT table.");

            return await TryNewSimpleTransaction(transaction, dsdId, dsdChildDbId, onlyLockTransaction, cancellationToken, dsd);
        }

        public async Task<bool> CloseTransaction(
            Transaction transaction,
            IImportReferenceableStructure referencedStructure,
            bool PITRestorationAllowed,
            bool calculateActualContentConstraint,
            bool includeRelatedDataFlows,
            IMappingStoreDataAccess mappingStoreDataAccess,
            CancellationToken cancellationToken)
        {
            if (transaction == null)
            {
                return false;
            }

            //Execute PIT Release if there if current transaction targets PIT and should be release immediately
            if (referencedStructure.Dsd.PITVersion != null //Is there a PIT?
                && (referencedStructure.Dsd.PITReleaseDate != null && referencedStructure.Dsd.PITReleaseDate <= DateTime.Now) //PIT release date has passed
            )
            {
                // Set PIT release date to current time as this the time when the PIT release version is made available as LIVE
                referencedStructure.Dsd.PITReleaseDate = DateTime.Now;

                await ApplyPITRelease(referencedStructure, PITRestorationAllowed, mappingStoreDataAccess);
            }
            else
            {
                var liveVersion = GetDsdLiveVersion(referencedStructure.Dsd);
                var isLiveVersionTargeted = liveVersion == null
                         ? referencedStructure.Dsd.PITVersion == null
                         : liveVersion.Equals(transaction.TableVersion);
                var liveAccStartDate = DateTime.Now;

                if (calculateActualContentConstraint)
                {
                    await UpdateCodeListAvailabilityInfo(
                        referencedStructure,
                        (DbTableVersion)transaction.TableVersion,
                        isLiveVersionTargeted ? TargetVersion.Live : TargetVersion.PointInTime,
                        mappingStoreDataAccess,
                        liveAccStartDate,
                        includeRelatedDataFlows,
                        cancellationToken
                    );
                }

                //create empty ACC for live version when the first upload targets the PIT version
                if (!isLiveVersionTargeted && liveVersion == null)
                {
                    await UpdateCodeListAvailabilityInfo(
                        referencedStructure,
                        DbTableVersions.GetNewTableVersion((DbTableVersion)transaction.TableVersion),
                        TargetVersion.Live,
                        mappingStoreDataAccess,
                        liveAccStartDate,
                        includeRelatedDataFlows,
                        cancellationToken
                    );
                }

                //Manage all mappingSets of the dataFlows belonging to the Dsd and Msd
                await ManageMappingSets(referencedStructure, mappingStoreDataAccess, includeRelatedDataFlows, cancellationToken);
            }

            await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(referencedStructure.Dsd, cancellationToken);
            await _unitOfWork.MetadataStoreRepository.DropMetadataStagingTables(referencedStructure.Dsd.DbId, cancellationToken);
            await _unitOfWork.DataStoreRepository.DropStagingTables(referencedStructure.Dsd, cancellationToken);

            if (!await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, true))
            {
                //there is no transaction with this id
                Log.Warn(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoActiveTransactionWithId),
                    transaction.Id));
                return false;
            }

            Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TransactionSuccessful));

            return true;
        }

        public async Task<bool> TryNewSimpleTransaction(Transaction transaction, int dsdDbId, int? dsdChildDbId, bool lockOnly, CancellationToken cancellationToken, ArtefactItem dsd = null)
        {
            Log.Debug("TryNewSimpleTransaction");

            if (dsdDbId > 0 && dsd is null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            if (dsd != null && dsdDbId <= 0)
            {
                throw new ArgumentException(nameof(dsd));
            }

            var isTransactionWithNoDsd = dsdDbId <= 0 || dsd is null;

            if (isTransactionWithNoDsd)
            {
                Log.Debug("TryNewSimpleTransaction with no DSD specified");
            }

            transaction.ArtefactDbId = dsdDbId;
            transaction.ArtefactChildDbId = dsdChildDbId;

            //Skip if the transaction lock has been acheived previously
            if (!await IsTransactionLocked(transaction.Id, cancellationToken))
            {
                if (await HasActiveTransaction(transaction, isTransactionWithNoDsd, logErrors: !lockOnly, cancellationToken))
                    return false;

                //Rollback transaction lock (set artefact id) If the lock is achieved but there are other blocking transactions
                //IsolationLevel.ReadUncommitted - Volatile data can be read and modified during the transaction. 
                //It allows to see other concurrent requests that are trying to achieve a lock, which have not been committed. 
                using (var transactionScope = _unitOfWork.DotStatDb.GetTransactionScope())
                {
                    if (!await _unitOfWork.TransactionRepository.TryLockNewTransaction(transaction, isTransactionWithNoDsd, DbTableVersion.X, cancellationToken, logErrors: !lockOnly))
                        return false;

                    transactionScope.Complete();
                }
            }

            transaction.TableVersion = DbTableVersion.X;
            return true;
        }

        public async Task CleanUpFailedTransaction(Transaction transaction)
        {
            if (transaction == null)
            {
                //throw new ArgumentNullException(nameof(transaction));
                Log.Debug("Transaction parameter is null.");
                return;
            }

            //Non-Cancellable method
            var cancellationToken = CancellationToken.None;
            if (transaction.ArtefactDbId == null)
            {
                Log.Debug("Transaction marked as completed.");
                await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
                return;
            }

            Log.Debug("Cleanup of failed transaction started.");

            Log.Debug("Dropping staging tables.");

            await _unitOfWork.MetadataStoreRepository.DropMetadataStagingTables((int)transaction.ArtefactDbId, cancellationToken);
            await _unitOfWork.DataStoreRepository.DropStagingTables((int)transaction.ArtefactDbId, cancellationToken);


            Log.Debug("Closing unsuccessful transaction.");

            await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
            Log.Debug("Cleanup completed.");
        }

        public async Task CloseCanceledOrTimedOutTransaction(int transactionId)
        {
            var cancellationToken = CancellationToken.None;
            try
            {
                var transaction = await _unitOfWork.TransactionRepository.GetTransactionById(transactionId, cancellationToken);
                if (transaction == null)
                    return;

                if (_unitOfWork.MaxDataImportTimeInMinutes > 0 && transaction.ExecutionStart != null &&
                (DateTime.Now - (DateTime)transaction.ExecutionStart).TotalMinutes >= _unitOfWork.MaxDataImportTimeInMinutes)
                {
                    await _unitOfWork.TransactionRepository.MarkTransactionAsTimedOut(transaction.Id);
                    Log.Error(new TransactionTimeOutException(
                        string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.TerminatingTimedOutTransaction), transactionId)));
                }
                else
                {
                    await _unitOfWork.TransactionRepository.MarkTransactionAsCanceled(transaction.Id);
                    Log.Error(new TransactionKilledException(
                        string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.TerminatingCanceledTransaction), transactionId)));
                }
            }
            catch (System.Exception e)
            {
                Log.Fatal(e);
            }
        }

        public async Task<TransactionStatus> GetFinalTransactionStatus(int transactionId, CancellationToken cancellationToken)
        {
            try
            {
                var transaction = await _unitOfWork.TransactionRepository.GetTransactionById(transactionId, cancellationToken);
                if (transaction != null && !transaction.Closed)
                {
                    await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
                    transaction.Status = TransactionStatus.Completed;
                }
                return transaction == null ? TransactionStatus.Unknown : transaction.Status;
            }
            catch (System.Exception e)
            {
                Log.Fatal(e);
            }
            return TransactionStatus.Unknown;
        }

        public async Task<bool> InitializeAllMappingSets(Transaction transaction, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            var structDbDsds = mappingStoreDataAccess
                .GetDataStructures(_unitOfWork.DataSpace)
                .ToDictionary(x => x.FullId());

            if (!structDbDsds.Any())
            {
                Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDsdsInMappingStoreDb));
                return true;
            }

            var structDbDataFlows = mappingStoreDataAccess.GetDataflows(_unitOfWork.DataSpace);
            
            if (!structDbDataFlows.Any())
            {
                Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDataflowsInMappingStoreDb));
                return true;
            }

            try
            {
                if (!await TryNewTransactionWithNoDsd(transaction, cancellationToken))
                {
                    await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
                    // Transaction creation attempt failed due to an ongoing 'InitializeAllMappingSets' transaction
                    return false;
                }

                var dataDbDataFlows = (await _unitOfWork.ArtefactRepository.GetListOfArtefacts(cancellationToken, DbTypes.GetDbType(SDMXArtefactType.Dataflow)))
                    .ToDictionary(x => x.ToString());

                var dataDbDsds = (await _unitOfWork.ArtefactRepository.GetListOfArtefacts(cancellationToken, DbTypes.GetDbType(SDMXArtefactType.Dsd)))
                    .ToDictionary(x=>x.ToString());
                
                var failedDataflows = new List<string>();
                var completedDataflows = new List<string>();
                var updatedDataflows = new List<string>();
                
                var migratedMappingSets = mappingStoreDataAccess.GetMigratedMappingSets(_unitOfWork.DataSpace);

                foreach (var structDbDataflow in structDbDataFlows)
                {
                    var dataflowFullId = structDbDataflow.FullId();
                    var dsdFullId = structDbDataflow.DataStructureRef?.FullId();

                    try
                    {
                        // Do nothing when dataflow is external reference
                        if (structDbDataflow.IsExternalReference?.IsTrue ?? false)
                        {
                            Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InitExternalDataflow), dataflowFullId));
                            continue;
                        }

                        if (string.IsNullOrEmpty(dsdFullId) || !structDbDsds.TryGetValue(dsdFullId, out var structDbDsd))
                        {
                            failedDataflows.Add(dataflowFullId);

                            Log.Error(new ArtefactNotFoundException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotSet),
                                structDbDataflow.AgencyId,
                                structDbDataflow.Id,
                                structDbDataflow.Version)));

                            continue;
                        }

                        ArtefactItem dataDbDsd = null;

                        //dataflow already initialized in the data DB
                        if (dataDbDataFlows.TryGetValue(dataflowFullId, out var dataDbDataFlow))
                        {
                            if (!dataDbDsds.TryGetValue(dsdFullId, out dataDbDsd))
                            {
                                failedDataflows.Add(dsdFullId);

                                Log.Error(new ArtefactNotFoundException(string.Format(
                                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotFound),
                                    structDbDataflow.DataStructureRef.AgencyId,
                                    structDbDataflow.DataStructureRef.MaintainableId,
                                    structDbDataflow.DataStructureRef.Version
                                )));

                                continue;
                            }
                        }

                        var dataFlow = mappingStoreDataAccess.GetDataflow(
                            _unitOfWork.DataSpace,
                            structDbDataflow,
                            structDbDsd,
                            null,
                            ResolveCrossReferences.DoNotResolve,
                            false,
                            true
                        );

                        dataFlow.DbId = dataDbDataFlow?.DbId ?? 0;
                        dataFlow.Dsd.DbId = dataDbDsd?.DbId ?? 0;
                        dataFlow.Dsd.LiveVersion = dataDbDsd?.LiveVersion;
                        dataFlow.Dsd.PITVersion = dataDbDsd?.PITVersion;
                        dataFlow.Dsd.PITReleaseDate = dataDbDsd?.PITReleaseDate;

                        if (mappingStoreDataAccess.IsMigrated(dataFlow, migratedMappingSets))
                        {
                            continue;
                        }

                        if (!ManageDataflowMappingsets(dataFlow, mappingStoreDataAccess))
                        {
                            failedDataflows.Add(dsdFullId);
                            continue;
                        }

                        if (dataDbDataFlow != null)
                        {
                            updatedDataflows.Add(dataflowFullId); 
                        }
                        else
                        {
                            completedDataflows.Add(dataflowFullId);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        failedDataflows.Add(dataflowFullId);

                        Log.Error(new DotStatException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnexpectedErrorWithDataflow),
                                dataflowFullId,
                                ex.Message),
                            ex)
                        );
                    }
                }

                if (completedDataflows.Any())
                    Log.Notice(string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.CreationOfMappingsetsSuccessForDataflows),
                        string.Join(", ", completedDataflows)));

                if (updatedDataflows.Any())
                    Log.Notice(string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.UpdatedMappingsetsOfDataflows),
                        string.Join(", ", updatedDataflows)));

                var hasFailedDataflows = failedDataflows.Any();

                if (hasFailedDataflows)
                {
                    Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.CreationOfMappingsetsFailedForDataflows),
                        string.Join(", ", failedDataflows)));
                }

                await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, !hasFailedDataflows);
            }
            catch (System.Exception ex)
            {
                await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
                Log.Error(ex);
                throw;
            }

            return true;
        }

        public async Task<bool> CleanUpDsd(int dsdDbId, bool deleteMsdObjectsOnly, IMappingStoreDataAccess mappingStoreDataAccess, List<ComponentItem> allComponents, List<MetadataAttributeItem> allMetadataAttributeComponents, CancellationToken cancellationToken)
        {
            if (dsdDbId == -1)
            {
                return false;
            }

            var dataflowArtefacts = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dsdDbId, cancellationToken);

            //Delete mappingsets and actual content constraints
            foreach (var df in dataflowArtefacts)
            {
                var dataFlow = mappingStoreDataAccess.GetDataflow(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, false);

                //Dataflow not in mappingstore
                if (dataFlow == null)
                    continue;

                mappingStoreDataAccess.DeleteDataSetsAndMappingSets(_unitOfWork.DataSpace, dataFlow, deleteMsdObjectsOnly);

                if (!deleteMsdObjectsOnly)
                    mappingStoreDataAccess.DeleteAllActualContentConstraints(_unitOfWork.DataSpace, dataFlow);
            }

            using var transactionScope = _unitOfWork.DotStatDb.GetTransactionScope();

            //Delete MSD objects in dataDb
            await _unitOfWork.ArtefactRepository.CleanUpMsdOfDsd(dsdDbId, dataflowArtefacts, allComponents, allMetadataAttributeComponents, cancellationToken);

            if (!deleteMsdObjectsOnly)
            {
                //Delete DSD objects in dataDb
                await _unitOfWork.ArtefactRepository.CleanUpDsd(dsdDbId, dataflowArtefacts, allComponents.ToList(), cancellationToken);
            }

            transactionScope.Complete();

            return true;
        }

        public async Task FillIdsFromDisseminationDb(IImportReferenceableStructure referencedStructure, CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow)
                referencedStructure.DbId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(referencedStructure, cancellationToken, true);

            await _unitOfWork.ArtefactRepository.FillMeta(referencedStructure.Dsd, cancellationToken);

            //Fill MSD info, only when the Dsd has a linked MSD
            if (referencedStructure.Dsd.MsdDbId != null && referencedStructure.Dsd.MsdDbId > 0)
                await _unitOfWork.ArtefactRepository.FillMeta(referencedStructure.Dsd.Msd, cancellationToken);

            // Dimensions
            foreach (var dim in referencedStructure.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                    continue;

                if (dim.Base.HasCodedRepresentation())
                {
                    dim.Codelist.DbId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(dim.Codelist, cancellationToken, true);
                }

                dim.DbId = await _unitOfWork.ComponentRepository.GetComponentDbId(dim, referencedStructure.Dsd.DbId, cancellationToken, true);

                if(referencedStructure is Dataflow dataFlow) 
                    CheckCodesOfAllowedCCOfComponent(dataFlow, dim, dim.Constraint);
            }

            // Primary measure
            var measure = referencedStructure.Dsd.PrimaryMeasure;

            if (measure.Base.HasCodedRepresentation())
            {
                measure.Codelist.DbId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(measure.Codelist, cancellationToken, true);
            }

            measure.DbId = await _unitOfWork.ComponentRepository.GetComponentDbId(measure, referencedStructure.Dsd.DbId, cancellationToken, true);

            // Attributes
            foreach (var attr in referencedStructure.Dsd.Attributes)
            {
                if (attr.Base.HasCodedRepresentation())
                {
                    attr.Codelist.DbId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(attr.Codelist, cancellationToken, true);

                    if (referencedStructure is Dataflow dataFlow)
                        CheckCodesOfAllowedCCOfComponent(dataFlow, attr, attr.Constraint);
                }

                attr.DbId = await _unitOfWork.ComponentRepository.GetComponentDbId(attr, referencedStructure.Dsd.DbId, cancellationToken, true);
            }

            if (referencedStructure.Dsd.Msd == null) return;

            //MetadataAttributes
            foreach (var attr in referencedStructure.Dsd.Msd.MetadataAttributes)
            {
                attr.Dsd = referencedStructure.Dsd;
                attr.DbId = await _unitOfWork.MetaMetadataComponentRepository.GetMetadataAttributeDbId(attr, referencedStructure.Dsd.Msd.DbId, cancellationToken, true);
            }
        }

        public async Task FillDbIdsAndCreateMissingDbObjects(IImportReferenceableStructure referencedStructure, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken, bool skipDsd = true, bool throwConcurrencyError = true)
        {
            if (!skipDsd)
            {
                //TODO: check and create DSD
            }
            else
            {
                if (referencedStructure.Dsd?.DbId <= 0)
                {
                    //Fatal, dbId should have been initialized before
                    throw new System.Exception($"The DbId for the DSD {referencedStructure.Dsd.FullId} is not set.");
                }
            }

            var factTableExists = await _unitOfWork.DataStoreRepository.DataTablesExist(referencedStructure.Dsd, cancellationToken);
            var componentItems = await _unitOfWork.ComponentRepository.GetComponentsOfDsd(referencedStructure.Dsd, cancellationToken);

            //Create missing Db components And fill management information
            await VerifyAndCreateDimensions(referencedStructure, componentItems, factTableExists, throwConcurrencyError, cancellationToken);

            await _unitOfWork.ArtefactRepository.VerifyAndCreateMeasures(referencedStructure.Dsd, componentItems, factTableExists, cancellationToken, throwConcurrencyError);

            await VerifyAndCreateAttributes(referencedStructure, componentItems, factTableExists, throwConcurrencyError, cancellationToken);

            await _unitOfWork.ArtefactRepository.VerifyAndCreateOrUpdateDynamicTables(referencedStructure.Dsd, factTableExists, cancellationToken);

            var dataFlow = referencedStructure as Dataflow;
            if (dataFlow is not null)
            {
                await _unitOfWork.ArtefactRepository.VerifyAndCreateDataflow(dataFlow, cancellationToken);

                await SetKeepHistoryOn(dataFlow, mappingStoreDataAccess, cancellationToken);
            }

            await VerifyAndUpdateDsdMsd(referencedStructure.Dsd, cancellationToken);

            //No linked MSD
            if (referencedStructure.Dsd?.Msd == null || !referencedStructure.Dsd.Msd.MetadataAttributes.Any())
                return;

            var metadataAttributeComponentItems = await _unitOfWork.MetaMetadataComponentRepository.GetMetadataAttributesOfMsd(referencedStructure.Dsd.Msd, cancellationToken);
            var metaTableExists = await _unitOfWork.MetadataStoreRepository.MetadataTablesExist(referencedStructure.Dsd, cancellationToken);
            await VerifyAndCreateMetadataAttributes(referencedStructure.Dsd, metadataAttributeComponentItems, metaTableExists, cancellationToken);

            await _unitOfWork.ArtefactRepository.VerifyAndCreateOrUpdateMetadataDynamicTables(referencedStructure.Dsd, metaTableExists, cancellationToken);

            if (dataFlow is null) return;

            var dataFlowMetaTableExists =
                await _unitOfWork.MetadataStoreRepository.DataFlowMetadataTablesExist(dataFlow, cancellationToken);

            await _unitOfWork.ArtefactRepository.CreateMetaDataFlow(dataFlow, dataFlowMetaTableExists, cancellationToken);
        }

        public async Task<bool> Rollback(IImportReferenceableStructure referencedStructure, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            if (referencedStructure == null)
            {
                throw new ArgumentNullException(nameof(referencedStructure));
            }

            if (referencedStructure is not Dataflow dataFlow)
            {
                //No rollback feature needed for DSD 
                return true;
            }

            if (dataFlow.Dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                    dataFlow.FullId)
                );
            }

            await _unitOfWork.ArtefactRepository.FillMeta(dataFlow.Dsd, cancellationToken);
            var PITVersion = dataFlow.Dsd.PITVersion;
            var PITReleaseDate = dataFlow.Dsd.PITReleaseDate;

            //is there a PIT
            if (PITVersion != null)
            {
                //PIT still not released
                if (PITReleaseDate == null || PITReleaseDate > DateTime.Now)
                {
                    //Delete PIT and restore information from artefact table
                    dataFlow.Dsd.PITVersion = null;
                    dataFlow.Dsd.PITReleaseDate = null;
                    dataFlow.Dsd.PITRestorationDate = null;
                    //Update importReferenceStructure
                    await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataFlow.Dsd, cancellationToken);

                    //Truncate tables containing PIT info
                    await _unitOfWork.DataStoreRepository.DeleteData(dataFlow.Dsd, (DbTableVersion)PITVersion, cancellationToken);
                    await DeleteMetadata(dataFlow.Dsd, (DbTableVersion)PITVersion, mappingStoreDataAccess, cancellationToken);

                    //Force to synchronize mappingsets
                    //Manage all mappingSets of the dataFlows belonging to the Dsd and Msd
                    await ManageMappingSets(dataFlow, mappingStoreDataAccess, includeRelatedDataFlows: true, cancellationToken);

                    var dataflowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dataFlow.Dsd.DbId, cancellationToken);
                    foreach (var df in dataflowsOfDsd)
                    {
                        //Delete actual content constraints of PIT release (all dataflows of DSD)
                        mappingStoreDataAccess.DeleteActualContentConstraint(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, (char)PITVersion, TargetVersion.PointInTime);
                    }

                    return true;
                }
                //throw PIT already released
                throw new PointInTimeReleaseException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.PITReleased),
                    dataFlow.Dsd.FullId));
            }
            //throw non existing PIT
            throw new TableVersionException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NonExistingPITVersion),
                dataFlow.Dsd.FullId));
        }

        public async Task<bool> Restore(IImportReferenceableStructure referencedStructure, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            if (referencedStructure == null)
            {
                throw new ArgumentNullException(nameof(referencedStructure));
            }

            if (referencedStructure is not Dataflow dataFlow)
            {
                //No rollback feature needed for DSD 
                return true;
            }

            if (dataFlow == null)
            {
                throw new ArgumentNullException(nameof(dataFlow));
            }

            if (dataFlow.Dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                    dataFlow.FullId)
                );
            }

            await _unitOfWork.ArtefactRepository.FillMeta(dataFlow.Dsd, cancellationToken);

            var liveVersion = dataFlow.Dsd.LiveVersion;
            var PITVersion = dataFlow.Dsd.PITVersion;
            var PITReleaseDate = dataFlow.Dsd.PITReleaseDate;
            var PITRestorationDate = dataFlow.Dsd.PITRestorationDate;

            //is there a PIT
            if (PITVersion != null)
            {
                //PIT already released
                if (PITReleaseDate != null && PITReleaseDate <= DateTime.Now)
                {
                    var oldLiveVersion = PITVersion;
                    var newLiveVersion = liveVersion;
                    //Delete PIT and restore information from artefact table
                    dataFlow.Dsd.LiveVersion = newLiveVersion;
                    dataFlow.Dsd.PITVersion = null;
                    dataFlow.Dsd.PITReleaseDate = null;
                    dataFlow.Dsd.PITRestorationDate = null;
                    //Update importReferenceStructure
                    await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataFlow.Dsd, cancellationToken);

                    //Truncate tables containing previous LIVE version (in this case PIT version)
                    await _unitOfWork.DataStoreRepository.DeleteData(dataFlow.Dsd, (DbTableVersion)oldLiveVersion, cancellationToken);
                    await DeleteMetadata(dataFlow.Dsd, (DbTableVersion)oldLiveVersion, mappingStoreDataAccess, cancellationToken);

                    //Force to synchronize mappingsets
                    //Manage all mappingSets of the dataFlows belonging to the Dsd and Msd
                    await ManageMappingSets(dataFlow, mappingStoreDataAccess, includeRelatedDataFlows: true, cancellationToken);

                    var dataflowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dataFlow.Dsd.DbId, cancellationToken);
                    foreach (var df in dataflowsOfDsd)
                    {
                        //Delete actual content constraints of PIT release (all dataflows of DSD)
                        mappingStoreDataAccess.DeleteActualContentConstraint(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, (char)PITVersion, TargetVersion.PointInTime);
                    }

                    return true;
                }
                //throw PIT not yet released
                throw new PointInTimeReleaseException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.PITNotReleased),
                    dataFlow.Dsd.FullId));
            }

            //When there is no PIT version then check for Restoration version and make it live if exists
            if (PITRestorationDate != null)
            {
                var newLiveVersion = (liveVersion != null) ? DbTableVersions.GetNewTableVersion((DbTableVersion)liveVersion) : DbTableVersion.A;
                var oldLiveVersion = DbTableVersions.GetNewTableVersion(newLiveVersion);

                var liveAccStartDate = PITRestorationDate.Value;

                //Change live version 
                dataFlow.Dsd.LiveVersion = (char)newLiveVersion;
                //Clear restore info
                dataFlow.Dsd.PITVersion = null;
                dataFlow.Dsd.PITReleaseDate = null;
                dataFlow.Dsd.PITRestorationDate = null;
                //Update importReferenceStructure
                await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataFlow.Dsd, cancellationToken);
                //Truncate tables containing previous live data
                await _unitOfWork.DataStoreRepository.DeleteData(dataFlow.Dsd, oldLiveVersion, cancellationToken);
                await DeleteMetadata(dataFlow.Dsd, oldLiveVersion, mappingStoreDataAccess, cancellationToken);

                //Re-create actual content constraints for restored version (all dataflows of DSD)
                await UpdateCodeListAvailabilityInfo(
                    dataFlow,
                    newLiveVersion,
                    TargetVersion.Live,
                    mappingStoreDataAccess,
                    liveAccStartDate,
                    includeRelatedDataFlows: true,
                    cancellationToken
                );

                //Force to synchronize mappingsets
                //Manage all mappingSets of the dataFlows belonging to the Dsd and Msd
                await ManageMappingSets(dataFlow, mappingStoreDataAccess, includeRelatedDataFlows: true, cancellationToken);

                return true;
            }

            //throw no version to restore
            throw new TableVersionException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoRestoringVersion),
                dataFlow.Dsd.FullId));
        }

        public async Task ApplyPITRelease(IImportReferenceableStructure referencedStructure, bool isRestorationAllowed, IMappingStoreDataAccess mappingStoreDataAccess)
        {
            if (referencedStructure == null)
            {
                throw new ArgumentNullException(nameof(referencedStructure));
            }

            //Non-Cancellable method    
            var cancellationToken = CancellationToken.None;
            //TODO : New localized text for when DSD is referenced
            if (referencedStructure.Dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                    referencedStructure.FullId)
                );
            }

            Log.Debug($"ApplyPITRelease for importReferenceStructure {referencedStructure.FullId}");

            var oldDsdLiveVersion = referencedStructure.Dsd.LiveVersion;
            var liveAccStartDate = referencedStructure.Dsd.PITReleaseDate ?? DateTime.Now;
            referencedStructure.Dsd.LiveVersion = (char)referencedStructure.Dsd.PITVersion;//New version
            //Clear PIT info
            referencedStructure.Dsd.PITVersion = null;
            referencedStructure.Dsd.PITReleaseDate = null;

            //Keep backup for restoration?
            if (!isRestorationAllowed && oldDsdLiveVersion != null)
            {
                referencedStructure.Dsd.PITRestorationDate = null;
                await _unitOfWork.DataStoreRepository.DeleteData(referencedStructure.Dsd, (DbTableVersion)DbTableVersions.GetDbTableVersion((char)oldDsdLiveVersion), cancellationToken);
                await DeleteMetadata(referencedStructure.Dsd, (DbTableVersion)DbTableVersions.GetDbTableVersion((char)oldDsdLiveVersion), mappingStoreDataAccess, cancellationToken);
            }

            if (oldDsdLiveVersion.HasValue)
            {
                var dataflowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(referencedStructure.Dsd.DbId, cancellationToken);
                foreach (var df in dataflowsOfDsd)
                {
                    //Delete actual content constraints of PIT release (all dataflows of DSD)
                    mappingStoreDataAccess.DeleteActualContentConstraint(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version,
                        oldDsdLiveVersion.Value, TargetVersion.Live);
                }
            }

            //Recalculate Actual content constraints for the activated PIT release (now LIVE)
            await UpdateCodeListAvailabilityInfo(
                referencedStructure,
                (DbTableVersion)referencedStructure.Dsd.LiveVersion,
                TargetVersion.Live,
                mappingStoreDataAccess,
                liveAccStartDate,
                includeRelatedDataFlows: true,
                cancellationToken
            );

            //Force to synchronize mappingsets
            //Manage all mappingSets of the dataFlows belonging to the Dsd and Msd
            await ManageMappingSets(referencedStructure, mappingStoreDataAccess,
                includeRelatedDataFlows: true, cancellationToken);

            //Apply PIT Release
            await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(referencedStructure.Dsd, cancellationToken);

        }

        public async Task ManageMappingSets(
            IImportReferenceableStructure referencedStructure,
            IMappingStoreDataAccess mappingStoreDataAccess,
            bool includeRelatedDataFlows,
            CancellationToken cancellationToken)
        {
            var dataFlow = referencedStructure as Dataflow;
            
            if (!includeRelatedDataFlows && dataFlow is not null)
            {
                ManageDataflowMappingsets(dataFlow, mappingStoreDataAccess);
                return;
            }

            //Update all mappingSets both PIT and Live 
            var dataFlowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(referencedStructure.Dsd.DbId, cancellationToken);
            
            foreach (var df in dataFlowsOfDsd)
            {

                var currentDataFlow = dataFlow is not null && df.DbId == dataFlow?.DbId
                    ? dataFlow
                    : new Dataflow(new DataflowObjectCore(new DataflowMutableCore(referencedStructure.Dsd.Base)
                        {
                            AgencyId = df.Agency,
                            Id = df.Id,
                            Version = df.Version
                        }), 
                        referencedStructure.Dsd
                      );

                currentDataFlow.DbId = df.DbId;

                ManageDataflowMappingsets(currentDataFlow, mappingStoreDataAccess);
            }
        }

        public DbTableVersion? GetDsdLiveVersion(Dsd dsd)
        {
            var art = new ArtefactItem
            {
                LiveVersion = dsd.LiveVersion,
                PITVersion = dsd.PITVersion,
                PITReleaseDate = dsd.PITReleaseDate,
                PITRestorationDate = dsd.PITRestorationDate,
            };

            return GetDsdLiveVersion(art);
        }

        public DbTableVersion? GetDsdPITVersion(Dsd dsd)
        {
            var art = new ArtefactItem
            {
                LiveVersion = dsd.LiveVersion,
                PITVersion = dsd.PITVersion,
                PITReleaseDate = dsd.PITReleaseDate,
                PITRestorationDate = dsd.PITRestorationDate,
            };

            return GetDsdPITVersion(art);
        }

        public async Task CompressReferencedStructure(IImportReferenceableStructure referencedStructure, DataCompressionEnum dataCompression, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            if (referencedStructure == null)
            {
                throw new ArgumentNullException(nameof(referencedStructure));
            }

            if (dataCompression is DataCompressionEnum.NONE)
                return;// no compression needed

            if (referencedStructure.Dsd.DataCompression is not DataCompressionEnum.NONE)
                return;// importReferenceStructure already compressed.

            Log.Notice(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.StartCompressingDsd),
                    referencedStructure.Dsd.FullId));

            try
            {
                var tables = new List<string> {
                    $"{referencedStructure.Dsd.SqlFilterTable()}",
                    $"{referencedStructure.Dsd.SqlFactTable((char)DbTableVersion.A)}",
                    $"{referencedStructure.Dsd.SqlFactTable((char)DbTableVersion.B)}",
                    $"{referencedStructure.Dsd.SqlDeletedTable((char)DbTableVersion.A)}",
                    $"{referencedStructure.Dsd.SqlDeletedTable((char)DbTableVersion.B)}"
                };

                //Check if importReferenceStructure has dim group attribute tables
                if (referencedStructure.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    .Any(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)))
                {
                    tables.AddRange(
                        new List<string> {
                            $"{referencedStructure.Dsd.SqlDimGroupAttrTable((char)DbTableVersion.A)}",
                            $"{referencedStructure.Dsd.SqlDimGroupAttrTable((char)DbTableVersion.B)}"
                        });
                }

                if (referencedStructure.Dsd.Msd is not null)
                {
                    tables.AddRange(new List<string> {
                        $"{referencedStructure.Dsd.SqlMetadataDataStructureTable((char)DbTableVersion.A)}",
                        $"{referencedStructure.Dsd.SqlMetadataDataStructureTable((char)DbTableVersion.B)}",
                        $"{referencedStructure.Dsd.SqlMetadataDataSetTable((char)DbTableVersion.A)}",
                        $"{referencedStructure.Dsd.SqlMetadataDataSetTable((char)DbTableVersion.B)}",
                        $"{referencedStructure.Dsd.SqlDeletedMetadataTable((char)DbTableVersion.A)}",
                        $"{referencedStructure.Dsd.SqlDeletedMetadataTable((char)DbTableVersion.B)}",
                    });

                    var dataFlowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(referencedStructure.Dsd.DbId, cancellationToken);
                    foreach (var df in dataFlowsOfDsd)
                    {
                        var currentDataFlow =
                            mappingStoreDataAccess.GetDataflow(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, false);

                        if (currentDataFlow == null)
                            continue;

                        currentDataFlow.DbId = df.DbId;
                        tables.AddRange(new List<string> {
                            $"{currentDataFlow.SqlMetadataDataFlowTable((char)DbTableVersion.A)}",
                            $"{currentDataFlow.SqlMetadataDataFlowTable((char)DbTableVersion.B)}",
                        });
                    }
                }

                // Drop all existing indexes
                using var transactionScope = _unitOfWork.DotStatDb.GetTransactionScope();

                foreach (var table in tables)
                {
                    await _unitOfWork.DotStatDb.DropAllIndexesOfTable(_unitOfWork.DotStatDb.DataSchema, table, cancellationToken);
                }

                transactionScope.Complete();

                // Create CCI indexes
                using var transactionScope2 = _unitOfWork.DotStatDb.GetTransactionScope();

                foreach (var table in tables)
                {
                    await _unitOfWork.DotStatDb.CreateColumnstoreIndex(_unitOfWork.DotStatDb.DataSchema, table, $"CCI_{table}", dataCompression, cancellationToken);
                }

                await _unitOfWork.ArtefactRepository.UpdateDataCompressionOfDsd(referencedStructure.Dsd.DbId, dataCompression, cancellationToken);

                transactionScope2.Complete();
            }
            catch (System.Exception ex)
            {
                Log.Error(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ErrorCompressingDsd),
                        referencedStructure.Dsd.FullId));
                Log.Fatal(ex);
            }

            Log.Notice(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FinishCompressingDsd),
                    referencedStructure.Dsd.FullId));
        }

        public async Task DeleteMetadata(Dsd dsd, DbTableVersion tableVersion, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            if (dsd.MsdDbId > 0)
            {
                await _unitOfWork.MetadataStoreRepository.DeleteDsdMetadata(dsd, tableVersion, cancellationToken);
                var dataFlowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dsd.DbId, cancellationToken);

                foreach (var df in dataFlowsOfDsd)
                {
                    var currentDataFlow =
                        mappingStoreDataAccess.GetDataflow(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version,
                            false);

                    currentDataFlow.DbId = df.DbId;
                    await _unitOfWork.MetadataStoreRepository.DeleteDataFlowMetadata(currentDataFlow, tableVersion, cancellationToken);
                }
            }
        }

        public async ValueTask CopyMetadataToNewVersion(Dsd dsd, DbTableVersion sourceTableVersion,
            DbTableVersion targetTableVersion, IMappingStoreDataAccess mappingStoreDataAccess,
            CancellationToken cancellationToken)
        {
            if (sourceTableVersion == targetTableVersion)
            {
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");
            }

            if (dsd?.Msd == null)
            {
                return;
            }

            if (dsd.MsdDbId > 0)
            {
                await _unitOfWork.MetadataStoreRepository.CopyDsdMetadataToNewVersion(dsd, sourceTableVersion, targetTableVersion, cancellationToken);
                var dataFlowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dsd.DbId, cancellationToken);

                foreach (var df in dataFlowsOfDsd)
                {
                    var dataFlowObjectCore = new DataflowObjectCore(new DataflowMutableCore(dsd.Base)
                    {
                        AgencyId = df.Agency,
                        Id = df.Id,
                        Version = df.Version
                    });

                    var currentDataFlow = new Dataflow(dataFlowObjectCore, dsd)
                    {
                        DbId = df.DbId
                    };

                    await _unitOfWork.MetadataStoreRepository.CopyDataFlowMetadataToNewVersion(currentDataFlow, sourceTableVersion, targetTableVersion, cancellationToken);
                }
            }
        }

        #region Protected methods

        private async Task<DbTableVersion?> InitDsdArtifact(Transaction transaction, Dsd dsd, bool throwConcurrencyError, CancellationToken cancellationToken)
        {
            Log.Debug($"InitDsdArtifactRecords");

            var isRollbackOrRestore = transaction.Type == TransactionType.Restore ||
                                      transaction.Type == TransactionType.Rollback;

            if (!isRollbackOrRestore)
            {
                //Get Dsd values stored in the database
                await _unitOfWork.ArtefactRepository.FillMeta(dsd, cancellationToken, false);
            }

            var artefact = await _unitOfWork.ArtefactRepository.GetArtefact(dsd.AgencyId, dsd.Base.Id, dsd.Version, SDMXArtefactType.Dsd, cancellationToken, false);
            var liveVersion = DbTableVersions.GetDbTableVersion(artefact.LiveVersion.ToString());
            var PITVersion = DbTableVersions.GetDbTableVersion(artefact.PITVersion.ToString());

            var origTransferTargetTableVersion = transaction.FinalTargetVersion == TargetVersion.Live ?
                GetDsdLiveVersion(artefact) : GetDsdPITVersion(artefact);
            Log.Debug($"Orig. table version : {origTransferTargetTableVersion}");

            if (artefact.DbId <= 0) //non-existing DSD
            {
                if (isRollbackOrRestore)
                {
                    throw new TableVersionException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoLiveOrPITVersion),
                        dsd.FullId));
                }

                origTransferTargetTableVersion = DbTableVersion.A;
                dsd.DataCompression = _unitOfWork.DotStatDb.DataSpace.Archive ? DataCompressionEnum.COLUMNSTORE_ARCHIVE : DataCompressionEnum.NONE;

                dsd.KeepHistory = _unitOfWork.DotStatDb.DataSpace.KeepHistory;

                if (!await _unitOfWork.ArtefactRepository.CreateNewDsdAftefact(dsd, cancellationToken, throwConcurrencyError))
                    return null;

                transaction.ArtefactDbId = dsd.DbId;

                dsd.MaxTextAttributeLength = dsd.GetMaxLengthOfTextAttributeFromConfig(_generalConfiguration.MaxTextAttributeLength);
            }
            else
            {
                Log.Debug("DSD already in ARTEFACT table.");
                transaction.ArtefactDbId = artefact.DbId;
                var isFirstLoadOfTargetTable = false;

                //In case there is already a PIT release defined on the DSD, 
                //all transaction on this DSD have to target PIT release irrespectively to the value of this parameter.
                if (transaction.RequestedTargetVersion == TargetVersion.Live && artefact.PITVersion == null)
                    transaction.FinalTargetVersion = TargetVersion.Live;
                else
                    transaction.FinalTargetVersion = TargetVersion.PointInTime;

                //first upload ever
                if (liveVersion == null && PITVersion == null)
                {
                    isFirstLoadOfTargetTable = true;
                    origTransferTargetTableVersion = DbTableVersion.A;

                    if (transaction.FinalTargetVersion == TargetVersion.Live)
                    {
                        liveVersion = DbTableVersion.A;
                        dsd.LiveVersion = (char)liveVersion;
                    }
                    else
                    {
                        PITVersion = DbTableVersion.A;
                        dsd.PITVersion = (char)PITVersion;
                    }
                }

                //No uploads ever to the live version
                if (liveVersion == null)
                {
                    liveVersion = DbTableVersions.GetNewTableVersion((DbTableVersion)DbTableVersions.GetDefaultLiveVersion());
                    dsd.LiveVersion = (char)liveVersion;
                    isFirstLoadOfTargetTable = true;
                }


                if (origTransferTargetTableVersion == null)
                {
                    //Rollback.- PIT version does not exist
                    //Restore.- Live version  does not exist
                    if (isRollbackOrRestore)
                    {
                        if (transaction.FinalTargetVersion == TargetVersion.PointInTime)
                        {
                            throw new TableVersionException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NonExistingPITVersion),
                                dsd.FullId));
                        }

                        throw new TableVersionException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NonExistingLiveVersion),
                            dsd.FullId));
                    }

                    origTransferTargetTableVersion = transaction.FinalTargetVersion == TargetVersion.PointInTime ?
                        DbTableVersions.GetNewTableVersion((DbTableVersion)liveVersion) :
                        DbTableVersions.GetNewTableVersion((DbTableVersion)PITVersion);

                    isFirstLoadOfTargetTable = true;
                }

                dsd.DbId = artefact.DbId;

                if (isFirstLoadOfTargetTable)
                {
                    await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dsd, cancellationToken);
                }
            }

            Log.Debug($"Dsd : {dsd.DbId} [{dsd.Base.Id}]");
            Log.Debug($"Dsd new data table version : {origTransferTargetTableVersion}");

            return origTransferTargetTableVersion;
        }

        private async Task<bool> LockTransaction(Transaction transaction, DbTableVersion dbTableVersion, CancellationToken cancellationToken, bool onlyLockTransaction)
        {
            if (_unitOfWork.DotStatDb.DataSpace.KeepHistory &&
                transaction.RequestedTargetVersion == TargetVersion.PointInTime)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .PITAndKeepHistoryNotSupportedError),
                    _unitOfWork.DataSpace)
                );
            }

            //Skip if the transaction lock has been acheived previously
            if (!await IsTransactionLocked(transaction.Id, cancellationToken))
            {
                if (await HasActiveTransaction(transaction, false, logErrors: !onlyLockTransaction, cancellationToken))
                    return false;

                //Rollback transaction lock (set artefact id) If the lock is acheived but there are other blocking transactions
                //IsolationLevel.ReadUncommitted - Volatile data can be read and modified during the transaction. 
                //It allows to see other concurrent requests that are trying to acheive a lock, which have not been commited. 
                using (var transactionScope = _unitOfWork.DotStatDb.GetTransactionScope())
                {
                    if (!await _unitOfWork.TransactionRepository.TryLockNewTransaction(transaction, false,
                            dbTableVersion, cancellationToken, logErrors: !onlyLockTransaction))
                        return false;

                    transactionScope.Complete();
                }
            }

            return true;
        }

        private async Task SetLockedDbTableVersion(Transaction transaction, Dsd dsd, DbTableVersion initialDbTableVersion, CancellationToken cancellationToken)
        {
            //check if importReferenceStructure data version has not changed since the beginning
            var dsdDbValues = await _unitOfWork.ArtefactRepository.GetArtefactByDbId(dsd.DbId, cancellationToken);

            //first transaction ever for the artefact
            if (dsdDbValues.LiveVersion is null && dsdDbValues.PITVersion is null)
            {
                dsdDbValues.LiveVersion = (char)DbTableVersion.A;
            }

            var currentTableVersionInDb = transaction.FinalTargetVersion == TargetVersion.Live ?
                GetDsdLiveVersion(dsdDbValues) : GetDsdPITVersion(dsdDbValues);

            //first transaction for the target version
            if (currentTableVersionInDb == null)
            {
                currentTableVersionInDb = transaction.FinalTargetVersion == TargetVersion.PointInTime
                    ? DbTableVersions.GetNewTableVersion((DbTableVersion)dsdDbValues.LiveVersion)
                    : DbTableVersions.GetNewTableVersion((DbTableVersion)dsdDbValues.PITVersion);
            }

            dsd.PITRestorationDate = dsdDbValues.PITRestorationDate;

            var finalDbTableVersion = initialDbTableVersion;
            if (currentTableVersionInDb != initialDbTableVersion)
            {
                finalDbTableVersion = (DbTableVersion)currentTableVersionInDb;
                Log.Debug($"Data table set changed before creation of the transaction from [{initialDbTableVersion}] to [{finalDbTableVersion}].");
                
                await _unitOfWork.TransactionRepository.UpdateTableVersionOfTransaction(transaction.Id, finalDbTableVersion, cancellationToken);
            }

            if (transaction.FinalTargetVersion != transaction.RequestedTargetVersion)
            {
                await _unitOfWork.TransactionRepository.UpdateFinalTargetVersionOfTransaction(transaction.Id, (TargetVersion)transaction.FinalTargetVersion, cancellationToken);
            }

            //Set the table version of the transaction.
            transaction.TableVersion = finalDbTableVersion;
        }

        private async Task<bool> HasActiveTransaction(Transaction transaction, bool isTransactionWithNoDsd, bool logErrors, CancellationToken cancellationToken)
        {
            //Check if there are running transactions for the same artefact
            var transactionsInProgress = _unitOfWork.TransactionRepository.GetInProgressTransactions(transaction.Id, (int)transaction.ArtefactDbId, cancellationToken);

            var hasActiveTransactions = false;
            await foreach (var transactionInProgress in transactionsInProgress)
            {
                //there is already an ongoing transaction for the same DSD
                Log.Debug($"Concurrent transaction detected : {transactionInProgress.Id}, timeout threshold: {_unitOfWork.MaxDataImportTimeInMinutes}");

                if ((DateTime.Now - (DateTime)transactionInProgress.ExecutionStart).TotalMinutes < _unitOfWork.MaxDataImportTimeInMinutes)
                {
                    if (transaction.ArtefactChildDbId != null && transactionInProgress.ArtefactChildDbId != null)
                    {
                        continue;
                    }

                    if (logErrors)
                    {
                        //the existing transaction is considered to be active
                        var errorMessage = isTransactionWithNoDsd
                            ? string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .TransactionAbortedDueToConcurrentTransactionWithNoDsd),
                                transaction.Id,
                                transactionInProgress.Id)
                            : string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .TransactionAbortedDueToConcurrentTransaction),
                                transaction.Id,
                                transaction.ArtefactFullId,
                                transactionInProgress.Id);

                        Log.Error(new DotStatException(errorMessage));
                    }

                    hasActiveTransactions = true;
                }
                else
                {
                    //Cleanup Timed out transaction

                    // Set log4net LogicalThreadContext to previous transaction
                    Log.SetTransactionId(transactionInProgress.Id);
                    Log.Error(new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.TerminatingTimedOutTransaction),
                        transactionInProgress.Id)));

                    await CleanUpFailedTransaction(transactionInProgress);

                    //the existing transaction has timed out
                    await _unitOfWork.TransactionRepository.MarkTransactionAsTimedOut(transactionInProgress.Id);

                    // Reset log4net LogicalThreadContext to current transaction
                    Log.SetTransactionId(transaction.Id);
                    var message = isTransactionWithNoDsd
                        ? string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .PreviousTransactionWithNoDsdTimedOut),
                            transactionInProgress.Id)
                        : string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .PreviousTransactionTimedOut),
                            transactionInProgress.Id,
                            transaction.ArtefactFullId);

                    Log.Warn(message);
                }
            }

            return hasActiveTransactions;
        }

        private async Task<bool> IsTransactionLocked(int transactionId, CancellationToken cancellationToken)
        {
            var transaction = await _unitOfWork.TransactionRepository.GetTransactionById(transactionId, cancellationToken);

            if (transaction?.ArtefactDbId != null)
                return true;

            return false;
        }

        private async Task UpdateCodeListAvailabilityInfo(
            IImportReferenceableStructure referencedStructure,
            DbTableVersion dbTableVersion,
            TargetVersion targetVersion,
            IMappingStoreDataAccess mappingStoreDataAccess,
            DateTime liveAccStartDate,
            bool includeRelatedDataFlows,
            CancellationToken cancellationToken
        )
        {
            if (referencedStructure == null || referencedStructure.DbId == 0 || referencedStructure.Dsd.DbId == 0)
                throw new ArgumentException(nameof(referencedStructure));

            var dataFlow = referencedStructure as Dataflow;
            if (!includeRelatedDataFlows && dataFlow is not null)
            {
                await CalculateActualContentConstraint(dataFlow, dbTableVersion, targetVersion, mappingStoreDataAccess, liveAccStartDate, cancellationToken);
                return;
            }

            var dsdDataflows = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(referencedStructure.Dsd.DbId, cancellationToken);

            foreach (var df in dsdDataflows)
            {
                var currentDataflow = dataFlow is not null && dataFlow.DbId == df.DbId ?
                    dataFlow :
                    mappingStoreDataAccess.GetDataflow(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, false);

                if (currentDataflow == null)
                {
                    // If a importReferenceStructure is not be present in structure database, it means it has been deleted but this deletion hasn't been yet synchronized to ARTEFACT table in data database 
                    Log.Debug($"Dataflow artefact {df}) not found in structure database and will be deleted from ARTEFACT table.");

                    // Delete importReferenceStructure from ARTEFACT table if does not exist in structure database
                    await _unitOfWork.ArtefactRepository.DeleteArtefact(df.DbId, cancellationToken);

                    continue;
                }

                if (currentDataflow.DbId == 0 || currentDataflow.Dsd.DbId == 0)
                {
                    await FillIdsFromDisseminationDb(currentDataflow, cancellationToken);
                    //Copy PIT related info from the target importReferenceStructure of the current transfer transaction (these are saved just at the end of the transaction into database)
                    currentDataflow.Dsd.LiveVersion = referencedStructure.Dsd.LiveVersion;
                    currentDataflow.Dsd.PITVersion = referencedStructure.Dsd.PITVersion;
                    currentDataflow.Dsd.PITReleaseDate = referencedStructure.Dsd.PITReleaseDate;
                }

                // ----------------------------------------------
                await CalculateActualContentConstraint(currentDataflow, dbTableVersion, targetVersion, mappingStoreDataAccess, liveAccStartDate, cancellationToken);
            }
        }
        
        private async Task CalculateActualContentConstraint(
            Dataflow dataflow,
            DbTableVersion dbTableVersion,
            TargetVersion targetVersion,
            IMappingStoreDataAccess mappingStoreDataAccess,
            DateTime liveAccStartDate,
            CancellationToken cancellationToken
        )
        {
            long obsCount = 0;
            var cubeRegion = new List<IKeyValuesMutable>();

            // If all constrainted dimensions have valid codes then calculate availability,
            // in case there is at least one component without valid codes then create "empty" actual CC
            if (dataflow.Dsd.Dimensions.All(d => d.Constraint?.HasValidCodes ?? true) &&
                dataflow.Dsd.Attributes.All(d => d.Constraint?.HasValidCodes ?? true))
            {
                //create sql WHERE clause that defines the content of the dataFlow
                var dataQuery = new DataQueryImpl(
                    dataStructure: dataflow.Dsd.Base,
                    lastUpdated: null,
                    dataQueryDetail: null,
                    firstNObs: null,
                    lastNObs: null,
                    dataProviders: null,
                    dataflow: dataflow.Base,
                    dimensionAtObservation: null,
                    selectionGroup: null)
                { IncludeHistory = false };

                var dataFlowWhereClause = Predicate.BuildWhereForObservationLevel(dataQuery, dataflow, codeTranslator: null, filterLastUpdated: false);
                var availability = await _unitOfWork.ArtefactRepository.CreateDataAvailabilityOfDataFlow(dataflow, dataFlowWhereClause, dbTableVersion, cancellationToken);
                obsCount = await _unitOfWork.ObservationRepository.GetObservationCount(dataflow, dbTableVersion, dataFlowWhereClause, cancellationToken);

                foreach (var dim in dataflow.Dsd.Dimensions)
                {
                    cubeRegion.Add(availability.GetAvailabilityByDimension(dim));
                }
            }

            // Save new content constraint
            mappingStoreDataAccess.SaveContentConstraint(_unitOfWork.DataSpace, dataflow, cubeRegion, true, (char)dbTableVersion, targetVersion, liveAccStartDate, obsCount);
        }


        // TODO should the mappingstore methods be aynchronous?
        private bool ManageDataflowMappingsets(
            Dataflow dataFlow, 
            IMappingStoreDataAccess mappingStoreDataAccess)
        {
            if (dataFlow.Dsd.Msd != null && !IsAllComponentsHaveUniqueId(dataFlow.Dsd, dataFlow.Dsd.Msd, out var collision))
            {
                Log.Error(new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NonUniqueComponentIdAcrossDsdAndMsd),
                    dataFlow.Dsd.FullId,
                    dataFlow.Dsd.Msd.FullId,
                    collision
                )));

                return false;
            }

            var (_, isAnyUserCreatedMappingSet) = mappingStoreDataAccess.HasMappingSet(_unitOfWork.DataSpace, dataFlow);

            if (isAnyUserCreatedMappingSet)
            {
                Log.Error(new DataFlowForDisseminationException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .DataFlowForDisseminationError), dataFlow.FullId)));
                
                return false;
            }

            var liveVersion = dataFlow.Dsd.LiveVersion;
            var pitVersion = dataFlow.Dsd.PITVersion;
            var isPitEmpty = dataFlow.Dsd.PITVersion == null;
            var isLiveEmpty = dataFlow.Dsd.LiveVersion == null;

            var releaseDate = dataFlow.Dsd.PITReleaseDate ?? ((DateTime)SqlDateTime.MaxValue).AddSeconds(-1);
            //FIX for the NSI WS 8.1.0 when trying to find the PIT mapping set with max datetime
            if (releaseDate > ((DateTime)SqlDateTime.MaxValue).AddSeconds(-1))
                releaseDate = releaseDate.AddSeconds(-1);

            //Initialize versions for mappingSets
            switch (liveVersion)
            {
                //No data has been loaded
                case null when isPitEmpty:
                    liveVersion = (char)DbTableVersion.A;
                    pitVersion = (char)DbTableVersion.B;
                    break;
                //Only data has been loaded to the PIT version
                case null:
                    liveVersion = (char)DbTableVersions.GetNewTableVersion(
                        (DbTableVersion)DbTableVersions.GetDbTableVersion((char)pitVersion));
                    break;
                //Only data has been loaded to the Live version
                default:
                    pitVersion = (char)DbTableVersions.GetNewTableVersion(
                        (DbTableVersion)DbTableVersions.GetDbTableVersion((char)liveVersion));
                    break;
            }

            //Synchronize (create or update) Dsd mappingsets 
            var result = mappingStoreDataAccess.CreateOrUpdateMappingSet(
                _unitOfWork.DataSpace, 
                dataFlow, 
                (char)liveVersion,
                null, 
                releaseDate,
                _unitOfWork.DataStoreRepository.GetMappingSetParams(dataFlow, isLiveEmpty ? null : liveVersion)
            );

            result = result && mappingStoreDataAccess.CreateOrUpdateMappingSet(
                _unitOfWork.DataSpace, 
                dataFlow, 
                (char)pitVersion,
                releaseDate, 
                null,
                _unitOfWork.DataStoreRepository.GetMappingSetParams(dataFlow, isPitEmpty ? null : pitVersion)
            );

            //Synchronize (create or update) Msd mappingsets 
            if (dataFlow.Dsd.Msd != null)
            {
                result = result && mappingStoreDataAccess.CreateOrUpdateMappingSet(
                        _unitOfWork.DataSpace,
                        dataFlow,
                        (char)liveVersion,
                        validFrom: null,
                        validTo: releaseDate,
                        _unitOfWork.MetadataStoreRepository.GetMappingSetParams(dataFlow, isLiveEmpty ? null : liveVersion)
                );

                // PIT version
                result = result && mappingStoreDataAccess.CreateOrUpdateMappingSet(
                    _unitOfWork.DataSpace,
                    dataFlow,
                    (char)pitVersion,
                    validFrom: releaseDate,
                    validTo: null,
                    _unitOfWork.MetadataStoreRepository.GetMappingSetParams(dataFlow, isPitEmpty ? null : pitVersion)
                );
            }

            return result;
        }

        private async Task VerifyAndCreateDimensions(IImportReferenceableStructure referencedStructure, IList<ComponentItem> componentItems, bool factTableExists, bool throwConcurrencyError, CancellationToken cancellationToken)
        {
            var dimensionsInDb = componentItems.Where(ci => ci.IsDimension)
                .ToDictionary(ci => ci.Id, StringComparer.InvariantCultureIgnoreCase);

            var isTimeDimensionInDb = await _unitOfWork.ComponentRepository.CheckTimeDimensionOfDsdInDb(referencedStructure.Dsd, DbTableVersion.A, cancellationToken);

            if (factTableExists)
            {
                if (referencedStructure.Dsd.TimeDimension != null && !isTimeDimensionInDb)
                {
                    throw new ChangeInDsdException(referencedStructure.Dsd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdTimeDimensionAdded)));
                }

                if (referencedStructure.Dsd.TimeDimension == null && isTimeDimensionInDb)
                {
                    throw new ChangeInDsdException(referencedStructure.Dsd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdTimeDimensionRemoved)));
                }
            }

            foreach (var dim in referencedStructure.Dsd.Dimensions)
            {
                ComponentItem dimensionInManagementDb = null;

                dimensionsInDb.TryGetValue(dim.Code, out dimensionInManagementDb);

                dimensionsInDb.Remove(dim.Code);

                if (dim.Base.TimeDimension && dimensionInManagementDb != null && !dimensionInManagementDb.IsTimeDimension)
                {
                    throw new ChangeInDsdException(referencedStructure.Dsd.FullId,
                        string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdDimensionToTimeDimension), dim.FullId));
                }

                if (factTableExists && dimensionInManagementDb == null)
                {
                    throw new ChangeInDsdException(referencedStructure.Dsd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdDimensionAdded), dim.FullId));
                }


                if (referencedStructure is Dataflow dataFlow)
                    CheckCodesOfAllowedCCOfComponent(dataFlow, dim, dim.Constraint);

                var dimType = SDMXArtefactType.TimeDimension;
                if (!dim.Base.TimeDimension)
                {
                    await _unitOfWork.ArtefactRepository.VerifyAndCreateCodelistOfComponent(referencedStructure.Dsd, dim, dimensionInManagementDb, factTableExists, cancellationToken, throwConcurrencyError);
                    dimType = SDMXArtefactType.Dimension;
                }

                if (dimensionInManagementDb?.DbId != null)
                    dim.DbId = dimensionInManagementDb.DbId;
                else
                {
                    componentItems.Add(
                        await _unitOfWork.ArtefactRepository.TryCreateDimensionComponent(dim, dimType,
                            cancellationToken));
                }
            }

            if (dimensionsInDb.Count > 0)
            {
                throw new ChangeInDsdException(referencedStructure.Dsd.FullId,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdDimensionRemoved),
                        string.Join(", ", dimensionsInDb.Select(d => d.Value.Id))));
            }
        }

        private async Task VerifyAndCreateAttributes(IImportReferenceableStructure referencedStructure, IList<ComponentItem> componentItems, bool factTableExists, bool throwConcurrencyError, CancellationToken cancellationToken)
        {
            var attributesInDb = componentItems.Where(ci => ci.IsAttribute)
                .ToDictionary(ci => ci.Id, StringComparer.InvariantCultureIgnoreCase);

            foreach (var attr in referencedStructure.Dsd.Attributes)
            {
                attributesInDb.TryGetValue(attr.Code, out var attributeInManagementDb);

                attributesInDb.Remove(attr.Code);

                if (factTableExists && attributeInManagementDb == null)
                {
                    throw new ChangeInDsdException(referencedStructure.Dsd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdAttributeAdded), attr.FullId));
                }

                if (referencedStructure is Dataflow dataFlow)
                    CheckCodesOfAllowedCCOfComponent(dataFlow, attr, attr.Constraint);

                if (attributeInManagementDb != null)
                {
                    if (attributeInManagementDb.IsCoded ^ attr.Base.HasCodedRepresentation())
                    {
                        throw new ChangeInDsdException(referencedStructure.Dsd.FullId,
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(
                                    Localization.ResourceId.ChangeInDsdAttributeCodedRepresentationChanged), attr.FullId,
                                attributeInManagementDb.IsCoded ? "" : "not", attr.Base.HasCodedRepresentation() ? "" : "not"));
                    }

                    // EMBARGO_TIME representation has to be DateTime
                    if (attr.Code.Equals(DbExtensions.EmbargoTime, StringComparison.InvariantCultureIgnoreCase) &&
                        attributeInManagementDb.EnumId != 10)
                    {
                        throw new ChangeInDsdException(referencedStructure.Dsd.FullId,
                            string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdAttributeTextFormatChanged),
                                attr.FullId,
                                TextEnumType.String.ToString(),
                                TextEnumType.DateTime.ToString()));
                    }

                    if (attributeInManagementDb.IsMandatory ^ attr.Base.Mandatory)
                    {
                        throw new ChangeInDsdException(referencedStructure.Dsd.FullId, string.Format(
                            LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.ChangeInDsdAttributeMandatoryStateChanged), attr.FullId,
                                attributeInManagementDb.AttributeStatus,
                            attr.Base.AssignmentStatus));
                    }

                    if (attributeInManagementDb.AttributeAssignmentlevel != attr.Base.AttachmentLevel)
                    {
                        throw new ChangeInDsdException(referencedStructure.Dsd.FullId, string.Format(
                            LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.ChangeInDsdAttributeAttachmentLevelChanged), attr.FullId,
                                attributeInManagementDb.AttributeAssignmentlevel,
                            attr.Base.AttachmentLevel
                        ));
                    }

                    if (attributeInManagementDb.AttributeGroup != attr.Base.AttachmentGroup)
                    {
                        throw new ChangeInDsdException(referencedStructure.Dsd.FullId, string.Format(
                            LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.ChangeInDsdAttributeAttachmentGroupChanged), attr.FullId,
                            attributeInManagementDb.AttributeGroup,
                            attr.Base.AttachmentGroup));
                    }
                }

                if (attr.Base.HasCodedRepresentation())
                {
                    await _unitOfWork.ArtefactRepository.VerifyAndCreateCodelistOfComponent(referencedStructure.Dsd, attr, attributeInManagementDb, factTableExists, cancellationToken, throwConcurrencyError);
                }

                if (attributeInManagementDb?.DbId != null)
                    attr.DbId = attributeInManagementDb.DbId;
                else
                    await _unitOfWork.AttributeRepository.CreateAttribute(attr, referencedStructure.Dsd, componentItems, cancellationToken);
            }

            if (attributesInDb.Count > 0)
            {
                throw new ChangeInDsdException(referencedStructure.Dsd.FullId, string.Format(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.ChangeInDsdAttributeRemoved), string.Join(", ", attributesInDb.Select(a => a.Value.Id))));
            }
        }

        private void CheckCodesOfAllowedCCOfComponent(Dataflow dataFlow, IDotStatCodeListBasedIdentifiable component, Constraint constraint)
        {
            var codelist = component.Codelist;

            if (codelist == null || constraint?.Codes == null)
                return;

            var codelistCodesSet = codelist.Codes.Select(c => c.Code).ToHashSet();

            var invalidConstraintCodes = new List<string>();

            foreach (var constraintCode in constraint.Codes)
            {
                if (!codelistCodesSet.Contains(constraintCode))
                {
                    invalidConstraintCodes.Add(constraintCode);
                }
            }

            if (invalidConstraintCodes.Any())
            {
                Log.Warn(
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InvalidCodeInAllowedContentConstraint),
                        dataFlow.FullId,
                        constraint.FullId,
                        component.Code,
                        string.Join(',', invalidConstraintCodes)
                    ));

                constraint.HasValidCodes = invalidConstraintCodes.Count < constraint.Codes.Count;
            };
        }

        private async Task VerifyAndCreateMetadataAttributes(Dsd dsd, IList<MetadataAttributeItem> metadataAttributeItems, bool metaDataTableExists, CancellationToken cancellationToken)
        {
            var attributesInDb = metadataAttributeItems
                .ToDictionary(ci => ci.Id, StringComparer.InvariantCultureIgnoreCase);

            foreach (var attr in dsd.Msd.MetadataAttributes)
            {
                attr.Dsd = dsd;
                attributesInDb.TryGetValue(attr.HierarchicalId, out var attributeInManagementDb);

                attributesInDb.Remove(attr.HierarchicalId);

                if (metaDataTableExists && attributeInManagementDb == null)
                {
                    throw new ChangeInMsdException(dsd.Msd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInMsdAttributeAdded), attr));
                }

                if (attributeInManagementDb?.DbId != null)
                    attr.DbId = attributeInManagementDb.DbId;
                else
                    await _unitOfWork.ArtefactRepository.CreateMetadataAttribute(attr, cancellationToken);

            }

            if (attributesInDb.Count > 0)
            {
                throw new ChangeInMsdException(dsd.Msd.FullId, string.Format(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.ChangeInMsdAttributeRemoved), string.Join(", ", attributesInDb.Select(a => a.Value.Id))));
            }
        }

        private async Task VerifyAndUpdateDsdMsd(Dsd dsd, CancellationToken cancellationToken)
        {
            //current msd
            var msdDbId = await _unitOfWork.ArtefactRepository.GetDsdMsdDbId(dsd.DbId, cancellationToken);

            //No MSD linked to DSD
            if ((msdDbId == null || msdDbId <= 0) && dsd.Msd == null)
                return;

            //MSD not in DB or not linked to DSD in DB
            if (msdDbId == null || msdDbId <= 0)
            {
                //Get MSD dbId if exists otherwise create the MSD
                await _unitOfWork.ArtefactRepository.GetOrCreateMsd(dsd.Msd, cancellationToken);

                Log.Notice(string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.ChangeInDsdMsdAdded), dsd.Msd.FullId, dsd.FullId));

                //Link MSD to DSD in DB
                dsd.MsdDbId = dsd.Msd.DbId;
                await _unitOfWork.ArtefactRepository.UpdateMsdOfDsd(dsd.DbId, dsd.MsdDbId, cancellationToken);
            }
            else
            {
                var currentDsdMsdInDb = await _unitOfWork.ArtefactRepository.GetArtefactByDbId((int)msdDbId, cancellationToken);
                //MSD removed
                if (dsd.Msd == null)
                {
                    throw new ChangeInDsdException(dsd.FullId, string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdMsdRemoved),
                        currentDsdMsdInDb, dsd.FullId));
                }

                //MSD changed
                if (currentDsdMsdInDb.ToString() != dsd.Msd.FullId)
                {
                    throw new ChangeInDsdException(dsd.FullId, string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.ChangeInDsdMsdChanged), dsd.FullId, currentDsdMsdInDb,
                        dsd.Msd.FullId));
                }
                //Fill msd db internal ids
                dsd.Msd.DbId = currentDsdMsdInDb.DbId;
                dsd.MsdDbId = currentDsdMsdInDb.DbId;
            }
        }

        private DbTableVersion? GetDsdLiveVersion(ArtefactItem art)
        {
            if (art.PITVersion == null) //non existing PITVersion
            {
                return art.LiveVersion != null ? DbTableVersions.GetDbTableVersion((char)art.LiveVersion) : null;
            }

            if (art.PITReleaseDate == null) //existing PIT with no release date set
            {
                return art.LiveVersion != null ? DbTableVersions.GetDbTableVersion((char)art.LiveVersion) : null;
            }

            if (art.PITReleaseDate <= DateTime.Now) //release date passed
            {
                return art.PITVersion != null ? DbTableVersions.GetDbTableVersion((char)art.PITVersion) : null;
            }

            // release date still not passed
            return art.LiveVersion != null ? DbTableVersions.GetDbTableVersion((char)art.LiveVersion) : null;
        }

        private bool IsAllComponentsHaveUniqueId(Dsd dsd, Msd msd, out string collision)
        {
            collision = null;
            var ids = new HashSet<string>(dsd.Dimensions.Select(dim => dim.Code));

            foreach (var attrId in dsd.Attributes.Select(a => a.Code))
                if (!ids.Add(attrId))
                {
                    collision = attrId;
                    return false;
                }

            foreach (var attrId in msd.MetadataAttributes.Select(a => a.HierarchicalId))
                if (!ids.Add(attrId))
                {
                    collision = attrId;
                    return false;
                }

            return true;
        }

        private DbTableVersion? GetDsdPITVersion(ArtefactItem art)
        {
            if (art.PITVersion == null) //non existing PITVersion
            {
                return null;
            }

            if (art.PITReleaseDate == null) //existing PIT with no release date set
            {
                return DbTableVersions.GetDbTableVersion((char)art.PITVersion?.ToString()?[0]);
            }

            if (art.PITReleaseDate <= DateTime.Now) // release date passed
            {
                return null;
            }

            // release date still not passed
            return DbTableVersions.GetDbTableVersion((char)art.PITVersion?.ToString()?[0]);
        }

        private async Task SetKeepHistoryOn(Dataflow dataFlow, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            if(dataFlow == null)
                return;

            //Configuration setting KeepHistory has changed to true
            //Migrate DSD and its dataflows to use temporal tables if it is still not set
            if (!_unitOfWork.DotStatDb.DataSpace.KeepHistory || dataFlow.Dsd.KeepHistory)
                return;

            await _unitOfWork.ArtefactRepository.SetKeepHistoryOn(dataFlow.Dsd, cancellationToken);
            var dsdDataFlows = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dataFlow.Dsd.DbId, cancellationToken);

            foreach (var df in dsdDataFlows)
            {
                //Copy initial DataFlow object to avoid querying the database
                var currentDataFlow = new Dataflow(dataFlow.Base, dataFlow.Dsd)
                {
                    DbId = df.DbId
                };

                await _unitOfWork.ArtefactRepository.SetKeepHistoryOn(currentDataFlow, cancellationToken);
            }
        }
        #endregion
    }
}