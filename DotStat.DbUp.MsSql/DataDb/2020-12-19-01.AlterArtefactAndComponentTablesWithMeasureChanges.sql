﻿DECLARE @ExecutinStart DATETIME = GetDate();

IF (OBJECT_ID('tempdb..#Errors_AlterArtefactAndComponentTablesWithMeasureChanges') IS NOT NULL)
BEGIN
    DROP TABLE #Errors_AlterArtefactAndComponentTablesWithMeasureChanges
END
-- Table to store warning or error messages for logging
CREATE TABLE #Errors_AlterArtefactAndComponentTablesWithMeasureChanges([DATE] DATETIME, [LEVEL] NVARCHAR(50), [MESSAGE] NVARCHAR(MAX), [EXCEPTION] NVARCHAR(MAX));

-- Change data type of ENUMERATIONS.ENUM_ID to bigint, to make it identical to the same table in mapping store database  
BEGIN TRY
IF EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[ENUMERATIONS]') and OBJECTPROPERTY(id, N'IsTable') = 1) AND
   EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[ENUMERATIONS]') and name = N'ENUM_ID')
BEGIN
    ALTER TABLE [management].[ENUMERATIONS] DROP CONSTRAINT PK_ENUMERATIONS;
    ALTER TABLE [management].[ENUMERATIONS] ALTER COLUMN [ENUM_ID] bigint not null;

    ALTER TABLE [management].[ENUMERATIONS] ADD  CONSTRAINT [PK_ENUMERATIONS] PRIMARY KEY CLUSTERED 
    (
	    [ENUM_ID] ASC
    );
END
ELSE BEGIN
   INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'ERROR', N'Table [management].[ENUMERATIONS] or column [management].[ENUMERATIONS].ENUM_ID not found.', '')
END;
END TRY
BEGIN CATCH
   INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'ERROR', N'Unexpected error when altering [management].[ENUMERATIONS] table.', ERROR_MESSAGE())
END CATCH

-- Add new textformat facet columns to component table and change type of ENUM_ID to bigint
BEGIN TRY
IF EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[COMPONENT]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    -- Change type of ENUM_ID to bigint
	ALTER TABLE [management].[COMPONENT] ALTER COLUMN [ENUM_ID] bigint NULL;

	-- Add MIN_LENGTH column
    IF NOT EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'MIN_LENGTH') 
	BEGIN 
		ALTER TABLE [management].[COMPONENT] ADD [MIN_LENGTH] int null;
	END
	ELSE BEGIN
	    INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'WARN', N'Column [management].[COMPONENT].MIN_LENGTH already exists.', '')
	END

	-- Add MIN_LENGTH column
    IF NOT EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'MAX_LENGTH') 
	BEGIN 
		ALTER TABLE [management].[COMPONENT] ADD [MAX_LENGTH] int null;
	END
	ELSE BEGIN
	    INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'WARN', N'Column [management].[COMPONENT].MAX_LENGTH already exists.', '')
	END

	-- Add PATTERN column
    IF NOT EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'PATTERN') 
	BEGIN 
		ALTER TABLE [management].[COMPONENT] ADD [PATTERN] nvarchar(4000);
	END
	ELSE BEGIN
	    INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'WARN', N'Column [management].[COMPONENT].PATTERN already exists.', '')
	END

	-- Add MIN_VALUE column
    IF NOT EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'MIN_VALUE') 
	BEGIN 
		ALTER TABLE [management].[COMPONENT] ADD [MIN_VALUE] int null;
	END
	ELSE BEGIN
	    INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'WARN', N'Column [management].[COMPONENT].MIN_VALUE already exists.', '')
	END

	-- Add MAX_VALUE column
    IF NOT EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'MAX_VALUE') 
	BEGIN 
		ALTER TABLE [management].[COMPONENT] ADD [MAX_VALUE] int null;
	END
	ELSE BEGIN
	    INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'WARN', N'Column [management].[COMPONENT].MAX_VALUE already exists.', '')
	END
END
ELSE BEGIN
   INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'ERROR', N'Table [management].[COMPONENT] not found.', '')
END;
END TRY
BEGIN CATCH
   INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'ERROR', N'Unexpected error when altering [management].[COMPONENT] table.', ERROR_MESSAGE())
END CATCH

BEGIN TRY
-- Migrate existing dsds' data type of measure to component table 
IF EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[COMPONENT]') and OBJECTPROPERTY(id, N'IsTable') = 1) AND
    EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'MIN_LENGTH') AND
    EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'MAX_LENGTH') AND
    EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'PATTERN') AND
    EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'MIN_VALUE') AND
    EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[COMPONENT]') and name = N'MAX_VALUE')
BEGIN
    DECLARE @SQL_COMMAND NVARCHAR(MAX) = 
    'INSERT INTO [management].[COMPONENT]
    ([ID],[TYPE],[DSD_ID],[CL_ID],[ATT_ASS_LEVEL],[ATT_STATUS],[ATT_GROUP_ID],[ENUM_ID],[MIN_LENGTH],[MAX_LENGTH],[PATTERN],[MIN_VALUE],[MAX_VALUE])
    SELECT ''OBS_VALUE'' as ID,
            ''PrimaryMeasure'' as COMPONENT_TYPE, 
            ART_ID as DSD_ID,
            NULL as CL_ID,
            NULL as ATT_ASS_LEVEL,
            NULL as ATT_STATUS,
            NULL as ATT_GROUP_ID,
            7 as ENUM_ID, -- 7 = float data type
            NULL as MIN_LENGTH,
            NULL as MAX_LENGTH,
            NULL as PATTERN,
            NULL as MIN_VALUE,
            NULL as MAX_VALUE
        FROM [management].[ARTEFACT] a
    WHERE TYPE = ''DSD''
	  AND NOT EXISTS(SELECT * FROM [management].[COMPONENT] c WHERE c.DSD_ID = a.ART_ID AND c.TYPE = ''PrimaryMeasure'')';

    EXEC(@SQL_COMMAND)
END
ELSE BEGIN
   INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'ERROR', N'Error when inserting primary measure compenents into [management].[COMPONENT] table: The table or the new columns added recently not found.', '')
END;
END TRY
BEGIN CATCH
   INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'ERROR', N'Unexpected error when inserting primary measure compenents into [management].[COMPONENT] table.', ERROR_MESSAGE())
END CATCH
	
BEGIN TRY    
-- Drop ARTEFACT.DSD_OBS_VALUE_DATA_TYPE column (not used for measure type)
IF EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[ARTEFACT]') and OBJECTPROPERTY(id, N'IsTable') = 1) AND
   EXISTS (SELECT 1 FROM [dbo].[syscolumns] WHERE id = object_id(N'[management].[ARTEFACT]') and name = N'DSD_OBS_VALUE_DATA_TYPE')
BEGIN
    ALTER TABLE [management].[ARTEFACT] DROP COLUMN [DSD_OBS_VALUE_DATA_TYPE];
END
ELSE BEGIN
    INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'WARN', N'Column [management].[ARTEFACT].DSD_OBS_VALUE_DATA_TYPE already dropped.', '')
END
END TRY
BEGIN CATCH
   INSERT INTO #Errors_AlterArtefactAndComponentTablesWithMeasureChanges VALUES (GETDATE(), N'ERROR', N'Unexpected error when inserting primary measure compenents into [management].[COMPONENT] table.', ERROR_MESSAGE())
END CATCH

-- If there were ERRORs or WARNings captured, insert them into LOG table
IF EXISTS(SELECT * FROM #Errors_AlterArtefactAndComponentTablesWithMeasureChanges)
BEGIN 
	DECLARE @Transaction_ID int;

	-- Obtain a transaction ID
	SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];

	-- Insert collected messages to LOGS table
    INSERT INTO [management].[LOGS] ([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
    SELECT @Transaction_ID, [Date], [Level], 'MSSQL SERVER:'+@@SERVERNAME,'2020-12-19-01.AlterArtefactAndComponentTablesWithMeasureChanges.sql', [Message], [Exception], 'dotstatsuite-dbup' FROM #Errors_AlterArtefactAndComponentTablesWithMeasureChanges

	-- Create transaction entry
	INSERT INTO [management].[DSD_TRANSACTION] 	([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[ARTEFACT_FULL_ID],[USER_EMAIL])
	VALUES (@Transaction_ID, -1, 'X', @ExecutinStart, GETDATE(), 0, 'DB_UPGRADE:AlterArtefactAndComponentTablesWithMeasureChanges(20201219.01)',NULL)
END

DECLARE @HasError BIT = 0;
DECLARE @ErrorMsg NVARCHAR(2048) = NULL

-- Set flag indication if there was error logged
SELECT @HasError = 1, @ErrorMsg = 'Error occured when exectuing 2020-12-19-01.AlterArtefactAndComponentTablesWithMeasureChanges.sql. Please check the LOGS table for TRANSACTION_ID = ' + CONVERT(NVARCHAR, @Transaction_ID)
 WHERE EXISTS(SELECT * FROM #Errors_AlterArtefactAndComponentTablesWithMeasureChanges WHERE LEVEL = 'ERROR')

IF (OBJECT_ID('tempdb..#Errors_AlterArtefactAndComponentTablesWithMeasureChanges') IS NOT NULL)
BEGIN
    DROP TABLE #Errors_AlterArtefactAndComponentTablesWithMeasureChanges
END

-- If there was an ERROR, raise exception to prevent execution of futher scripts by DbUp 
-- WARNings were created when the state of db is OK, i.e. the required change was already done, but not by this execution of the script
IF @HasError = 1 THROW 51000, @ErrorMsg , 1;  
