﻿-- Executed only on Azure SQL Database because not all pricing tiers support columnstore indexes used by .Stat Suite.
-- If the pricing tier is Basic, or Standard S0, S1 or S2 then the script sets the pricing tier to Standard S3.
-- Standard S3 is the minimum pricing tier required for columnstore indexes.
-- Further info https://docs.microsoft.com/en-us/azure/azure-sql/database/service-tiers-dtu#compare-the-dtu-based-service-tiers
-- Azure SQL Managed Instance supports columnstore indexes: https://docs.microsoft.com/en-us/azure/azure-sql/database/features-comparison
IF SERVERPROPERTY('EngineEdition') = 5
BEGIN
    DECLARE @sql NVARCHAR(4000) =
        'DECLARE @edition NVARCHAR(128),
                @serviceObjective NVARCHAR(128);

        SELECT @edition = edition,
                @serviceObjective = service_objective
            FROM sys.database_service_objectives

        IF (@edition = ''Basic'')
        BEGIN
            ALTER DATABASE [$dbName$] MODIFY (EDITION = ''Standard'', SERVICE_OBJECTIVE=''S3'')
        END
        ELSE IF (@edition = ''Standard'')
        BEGIN
            IF @serviceObjective IN (''S0'', ''S1'', ''S2'')
            BEGIN
                ALTER DATABASE [$dbName$] MODIFY (SERVICE_OBJECTIVE=''S3'')
            END
        END';

    EXECUTE sp_executesql @sql
END;