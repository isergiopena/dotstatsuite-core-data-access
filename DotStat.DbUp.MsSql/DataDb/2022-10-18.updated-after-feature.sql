SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
  
DECLARE 	
	@DSD_ID int,
	@DF_ID int,

	@sql_migration NVARCHAR(MAX),	
	@sql_view NVARCHAR(MAX),

	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10),
	@U_DSD_VERSION_ID varchar(20),
	@U_DF_VERSION_ID varchar(20),
	@Transaction_ID int,
	@HAS_TIME_DIM bit;

DECLARE 
	@VERSIONS TABLE ([version] char(1));
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY);

DECLARE
	@DF_IDS TABLE(ART_ID int PRIMARY KEY);

INSERT into @DSD_IDS 
	SELECT ART_ID FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID

--------------------------------------------------------
--Create new transaction for DSD
SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
INSERT 
	INTO [management].[DSD_TRANSACTION]([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
	VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)
--------------------------------------------------------
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID = ART_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
	print 'Migrating DSD: ' + CAST(@DSD_ID AS VARCHAR)

--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			SET @U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;

			IF EXISTS(SELECT 1 FROM sys.columns WHERE [Name] = N'LAST_STATUS' AND Object_ID = Object_ID(N'data.FACT_' + @U_DSD_VERSION_ID))
			BEGIN
				print @U_DSD_VERSION_ID + ' already migrated, skipped';
				CONTINUE
			END
			
			BEGIN TRAN

			-- add LAST_STATUS column
			set @sql_migration = 'ALTER table data.FACT_{U_DSD_VERSION_ID} add
LAST_STATUS char(1) CONSTRAINT DF_FACT_{U_DSD_VERSION_ID}_LAST_STATUS default ''A'' not null';
			set @sql_migration = REPLACE(@sql_migration, '{U_DSD_VERSION_ID}', @U_DSD_VERSION_ID)
		
			exec sp_executesql @sql_migration;

			-- Drop data.FACT_{ID}_Deleted table 

			IF EXISTS(SELECT 1 FROM sys.tables WHERE [object_id]=Object_ID(N'data.FACT_' + @U_DSD_VERSION_ID + '_DELETED'))
			BEGIN
				set @sql_migration = 'DROP table data.FACT_' + @U_DSD_VERSION_ID + '_DELETED'
				exec sp_executesql @sql_migration;
			END

			-- RECREATE Indexes

			set @sql_migration = 'alter table [data].FACT_{U_DSD_VERSION_ID} drop constraint PK_DSD_{U_DSD_VERSION_ID}'
			set @sql_migration = REPLACE(@sql_migration, '{U_DSD_VERSION_ID}', @U_DSD_VERSION_ID)
			exec sp_executesql @sql_migration;

			-- CHECK IF TIME DIM EXISTS
			SELECT @HAS_TIME_DIM = count(*) FROM sys.columns WHERE [Name] = N'PERIOD_SDMX' AND Object_ID = Object_ID(N'data.FACT_' + @U_DSD_VERSION_ID)

			set @sql_migration = 'create unique clustered index I_CLUSTERED_DSD_{U_DSD_VERSION_ID} on [data].FACT_{U_DSD_VERSION_ID} ([LAST_STATUS],[SID] {TIME_COLUMMNS});
alter table [data].FACT_{U_DSD_VERSION_ID} add constraint PK_DSD_{U_DSD_VERSION_ID} PRIMARY KEY NONCLUSTERED([SID] {TIME_COLUMMNS});'
			set @sql_migration = REPLACE(@sql_migration, '{U_DSD_VERSION_ID}', @U_DSD_VERSION_ID)
			set @sql_migration = REPLACE(@sql_migration, '{TIME_COLUMMNS}', case @HAS_TIME_DIM when 1 then ',[PERIOD_START],[PERIOD_END] DESC' else '' end)
			exec sp_executesql @sql_migration;

			-- Handle dsd view
			select @sql_view = OBJECT_DEFINITION(object_id) from sys.views where [type]='V' and [name]='VI_CurrentDataDsd_' + @U_DSD_VERSION_ID;
			SELECT @sql_view = REPLACE(@sql_view, SUBSTRING(@sql_view, patindex('%CREATE%',@sql_view), patindex('%VIEW%',@sql_view)-patindex('CREATE',@sql_view) + 3), 'CREATE OR ALTER VIEW');
			SELECT @sql_view = REPLACE(@sql_view, 'FROM [data]', ',[FA].LAST_UPDATED,[FA].LAST_STATUS FROM [data]');

			exec sp_executesql @sql_view;

			-- Handle dataflow views
			DELETE @DF_IDS;
			INSERT into @DF_IDS 
			select ART_ID from management.ARTEFACT where [TYPE]='DF' and DF_DSD_ID=@DSD_ID

			WHILE (Select Count(*) FROM @DF_IDS) > 0
			BEGIN
				SELECT TOP 1 @DF_ID = ART_ID FROM @DF_IDS
				DELETE @DF_IDS WHERE ART_ID = @DF_ID

				SET @U_DF_VERSION_ID = CAST(@DF_ID AS VARCHAR) + '_' + @table_version;
				
				print 'Migrating DF: ' + @U_DF_VERSION_ID;

				select @sql_view = OBJECT_DEFINITION(object_id) from sys.views where [type]='V' and [name]='VI_CurrentDataDataFlow_'+ @U_DF_VERSION_ID
				SELECT @sql_view = REPLACE(@sql_view, SUBSTRING(@sql_view, patindex('%CREATE%',@sql_view), patindex('%VIEW%',@sql_view)-patindex('CREATE',@sql_view) + 3), 'CREATE OR ALTER VIEW');
				SELECT @sql_view = REPLACE(@sql_view, 'FROM [data]', ',LAST_UPDATED,LAST_STATUS FROM [data]');
			
				exec sp_executesql @sql_view;				
			END
			
			COMMIT TRAN
		END		
	END TRY  
	BEGIN CATCH 
		IF (XACT_STATE()) <> 0 ROLLBACK TRAN
		
		DECLARE 
			@DSD nvarchar(100),
			@msg nvarchar(1000);
			
		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'
			
		SELECT @msg= 'The following error was found while trying to update Views for the DSD ['+ @DSD +'] (The update of the view will be skiped):'
			+ @NewLineChar + 'ErrorLine:' + CAST(ERROR_LINE() AS VARCHAR)
			+ @NewLineChar + 'ErrorNumber:' + CAST(ERROR_NUMBER() AS VARCHAR)
			+ @NewLineChar + 'ErrorMessage:' + ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2022-10-18.updated-after-feature.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
				
	END CATCH

END

--update transaction info
UPDATE  [management].[DSD_TRANSACTION] SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
WHERE [TRANSACTION_ID] = @Transaction_ID