﻿using DotStat.Common.Enums;
using DotStat.Common.Localization;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

namespace DotStat.Domain
{
    public class Dimension : CodeListBasedIdentifiableDotStatObject<IDimension>
    {
        public int Index { get; protected set; }
        public Constraint Constraint { get; }

        public override Localization.ResourceId ResourceIdOfCodelistCodeRemoved =>
            Localization.ResourceId.ChangeInDsdDimensionCodelistCodeRemoved;
        public override Localization.ResourceId ResourceIdOfCodelistCodeAdded =>
            Localization.ResourceId.ChangeInDsdDimensionCodelistCodeAdded;
        public override Localization.ResourceId ResourceIdOfCodelistChanged =>
            Localization.ResourceId.ChangeInDsdDimensionCodelistChanged;

        public Dimension(int index, IDimension @base, Codelist codelist = null, Constraint constraint = null) : base(@base, codelist)
        {
            DbType = @base.TimeDimension ? DbTypes.GetDbType(SDMXArtefactType.TimeDimension) : DbTypes.GetDbType(SDMXArtefactType.Dimension);
            Index       = index;
            Constraint  = constraint;
        }
    }
}