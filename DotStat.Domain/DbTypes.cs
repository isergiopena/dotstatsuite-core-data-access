﻿using System;
using System.Collections.Generic;
using DotStat.Common.Enums;

namespace DotStat.Domain
{
    public static class DbTypes
    {
        private static readonly IDictionary<SDMXArtefactType, string> Items = new Dictionary<SDMXArtefactType, string>();

        static DbTypes()
        {
            Items.Add(SDMXArtefactType.Dsd, "DSD");
            Items.Add(SDMXArtefactType.Msd, "MSD");
            Items.Add(SDMXArtefactType.Dataflow, "DF");
            Items.Add(SDMXArtefactType.CodeList, "CL");
            Items.Add(SDMXArtefactType.Dimension, "Dimension");
            Items.Add(SDMXArtefactType.TimeDimension, "TimeDimension");
            Items.Add(SDMXArtefactType.DataAttribute, "Attribute");
            Items.Add(SDMXArtefactType.MetadataAttribute, "MetadataAttribute");
            Items.Add(SDMXArtefactType.PrimaryMeasure, "PrimaryMeasure");
        }

        public static string GetDbType(SDMXArtefactType sdmxArtefactType)
        {
            if (Items.ContainsKey(sdmxArtefactType))
            {
                return Items[sdmxArtefactType];
            }

            throw new NotImplementedException();
        }
    }
}
