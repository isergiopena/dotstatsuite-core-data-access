﻿using DotStat.Common.Enums;
using DotStat.Common.Localization;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

namespace DotStat.Domain
{
    public class PrimaryMeasure : CodeListBasedIdentifiableDotStatObject<IPrimaryMeasure>
    {
        public readonly bool IsMissingRepresentation;
        public ICrossReference RepresentationReference { get; }
        public ITextFormat TextFormat { get; }

        public override Localization.ResourceId ResourceIdOfCodelistCodeRemoved =>
            Localization.ResourceId.ChangeInDsdMeasureCodelistCodeRemoved;
        public override Localization.ResourceId ResourceIdOfCodelistCodeAdded =>
            Localization.ResourceId.ChangeInDsdMeasureCodelistCodeAdded;
        public override Localization.ResourceId ResourceIdOfCodelistChanged =>
            Localization.ResourceId.ChangeInDsdMeasureCodelistChanged;

        public PrimaryMeasure(IPrimaryMeasure @base, Codelist codelist = null) : base(@base, codelist)
        {
            DbType = DbTypes.GetDbType(SDMXArtefactType.PrimaryMeasure);

            IsMissingRepresentation = @base.Representation == null;

            var representation = @base.Representation ??
                                 new RepresentationCore(
                                     new RepresentationMutableCore()
                                     {
                                         TextFormat = new TextFormatMutableCore()
                                         {
                                             TextType = TextType.GetFromEnum(TextEnumType.String)
                                         },
                                         StructureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.LocalRepresentation)
                                     }, @base);

            RepresentationReference = representation.Representation;
            
            TextFormat = representation.TextFormat;
        }
    }
}