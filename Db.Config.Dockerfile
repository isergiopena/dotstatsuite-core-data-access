FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY DotStat.Devops.Db.Config/*.csproj ./DotStat.Devops.Db.Config/
RUN dotnet restore DotStat.Devops.Db.Config

# Copy everything else and build
COPY DotStat.Devops.Db.Config ./DotStat.Devops.Db.Config/
RUN dotnet publish DotStat.Devops.Db.Config -c Release -o /out -r linux-musl-x64

# Build runtime image
FROM mcr.microsoft.com/dotnet/runtime:6.0-alpine AS runtime-env
WORKDIR /app
COPY --from=build-env /out .

# Single parameter
ENV EXECUTION_PARAMETERS=--help

ENTRYPOINT dotnet DotStat.Devops.Db.Config.dll $EXECUTION_PARAMETERS