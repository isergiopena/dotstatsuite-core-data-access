﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Engine.SqlServer;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class SqlMsdEngineTests : BaseDbIntegrationTests
    {
        private SqlMsdEngine _engine;
        private Dataflow _dataflow;

        public SqlMsdEngineTests() : base("sdmx/CsvV2.xml")
        {
            _engine = new SqlMsdEngine(Configuration);
            _dataflow = this.GetDataflow();
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var id = await _engine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;

            var transactionId = UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken).Result;
            var transaction = UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.Dsd.FullId, null, null, "dataSource", TransactionType.Import, targetVersion, serviceId: null, CancellationToken).Result;

            // create dataflow DB objects 
            var tryNewTransactionResult =
                DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken).Result;
            Assert.IsTrue(tryNewTransactionResult, "Creation of the transaction failed.");

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            var success = DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, true,
                MsAccess, CancellationToken).Result;
            Assert.IsTrue(success);

            Assert.IsTrue(_dataflow.Dsd.Msd.DbId > 0);

            var id = await _engine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);
             
            Assert.AreEqual(_dataflow.Dsd.Msd.DbId, id);
            Assert.AreEqual(_dataflow.Dsd.MsdDbId, id);
        }

        [Test, Order(3)]
        public async Task Check_Msd_Meta_Tables_Created()
        {
            // Create Meta tables ...
            //await _engine.CreateDynamicDbObjects(_dataflow.Dsd, DotStatDb, CancellationToken);

            var expectedTables = new[]
            {
                $"{_dataflow.Dsd.SqlMetadataDataStructureTable('A')}",
                $"{_dataflow.Dsd.SqlMetadataDataStructureTable('B')}",
                $"{_dataflow.Dsd.SqlMetadataDataSetTable('A')}",
                $"{_dataflow.Dsd.SqlMetadataDataSetTable('B')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataTable('A')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataTable('B')}",
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
            
            var expectedViews = new[]
            {
                $"{_dataflow.Dsd.SqlMetaDataDsdViewName('A')}",
                $"{_dataflow.Dsd.SqlMetaDataDsdViewName('B')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataDsdViewName('A')}",
                $"{_dataflow.Dsd.SqlDeletedMetadataDsdViewName('B')}"
            };

            foreach (var view in expectedViews)
                //Views not created because the base table views are not created by this engine
                Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken), $"View {view} have not been found");

            var allDbComponents = UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken).Result.ToList();
            var allMsdDbComponents = UnitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(CancellationToken).Result.ToList();

            DotStatDbService.CleanUpDsd(_dataflow.Dsd.DbId, false, this.MsAccess, allDbComponents, allMsdDbComponents, this.CancellationToken);
        }
    }
}
