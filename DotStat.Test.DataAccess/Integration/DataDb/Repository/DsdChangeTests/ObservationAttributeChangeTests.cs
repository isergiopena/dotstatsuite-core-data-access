﻿using System.Threading.Tasks;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Repository.DsdChangeTests
{
    [TestFixture, Parallelizable(ParallelScope.Children)]
    [TestFixture(false)]
    [TestFixture(true)]
    public class ObservationAttributeChangeTests : BaseDsdChangeTests
    {
        private IImportReferenceableStructure _originalStructure;
        private IImportReferenceableStructure _originalStructure4NewCodeTest;
        private readonly bool _useDsdAsReferencedStructure;

        public ObservationAttributeChangeTests(bool useDsdAsReferencedStructure)
        {
            _useDsdAsReferencedStructure = useDsdAsReferencedStructure;
        }

        [OneTimeSetUp]
        public async Task SetUp()
        {
            _originalStructure = await InitOriginalStructure("ATTR_TYPE_DIFF_TEST.xml", useDsdReference: _useDsdAsReferencedStructure);
            _originalStructure4NewCodeTest = await InitOriginalStructure("ATTR_TYPE_DIFF_TEST_OBS_2.xml", useDsdReference: _useDsdAsReferencedStructure);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_originalStructure);
            await CleanUpStructure(_originalStructure4NewCodeTest);
        }

        [Test]
        public void ObservationAttributeAddedTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_ATTR_ADDED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_NEW", ex.Message);
        }

        [Test]
        public void ObservationAttributeRemovedTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_ATTR_REMOVED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("ATTR_OBS_STRING", ex.Message);
        }

        [Test]
        public void ObservationAttributeCodedChangedToNonCodedTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_ENUMERATED_TO_STRING.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_ENUMERATED", ex.Message);
        }

        [Test]
        public void ObservationAttributeNonCodedChangedToCodedTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_STRING_TO_ENUMERATED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
        }

        [Test]
        public void ObservationAttributeMandatoryToConditional()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_MANDATORY_TO_CONDITIONAL.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_ENUMERATED", ex.Message);
            StringAssert.Contains("from Mandatory to Conditional", ex.Message);
        }

        [Test]
        public void ObservationAttributeConditionalToMandatory()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_CONDITIONAL_TO_MANDATORY.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
            StringAssert.Contains("Conditional to Mandatory", ex.Message);
        }

        [Test]
        public void ObservationAttributeChangedToDatasetLevelTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_TO_DATASET.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
            StringAssert.Contains("from Observation to DataSet", ex.Message);
        }

        [Test]
        public void ObservationAttributeChangedToDimensionLevelTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_TO_DIMENSION.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
            StringAssert.Contains("from Observation to DimensionGroup", ex.Message);
        }

        [Test]
        public void ObservationAttributeChangedToGroupLevelTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_TO_GROUP.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
            StringAssert.Contains("from Observation to Group", ex.Message);
        }


        [Test]
        public void ObservationAttributeCodelistChangeTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_NEW_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_ENUMERATED", ex.Message);
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New code list
        }

        [Test]
        public void ObservationAttributeCodelistHasNewCodeTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_2_NEW_CODE_IN_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure4NewCodeTest.Dsd.DbId;

            Assert.DoesNotThrowAsync(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

        }

        [Test]
        public void ObservationAttributeCodelistHasRemovedCodeTest()
        {
            var referencedStructure =
                GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_CODE_REMOVED_FROM_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;
            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("T2, T3", ex.Message); // codes removed
            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_ENUMERATED", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_TEST_OBS(11.0)", ex.Message); //affected codelist
        }
    }
}

