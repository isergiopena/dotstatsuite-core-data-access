﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;

namespace DotStat.Test.DataAccess.Unit
{
    /// <summary>
    /// DF_SDG_GLB has following TimeRange constraints:
    /// <common:StartPeriod isInclusive="true">2013-01-01T00:00:00</common:StartPeriod>
    /// <common:EndPeriod isInclusive="true">2017-01-01T00:00:00</common:EndPeriod>
    /// </summary>
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class ConstraintTimeRangeTests : SdmxUnitTestBase
    {
        private readonly Dataflow _dataflow;
        private readonly ICodeTranslator _codeTranslator;

        /// <summary>
        /// Tests Predicate construction for Dataflow with Time range Constraints
        /// </summary>
        public ConstraintTimeRangeTests() : base("sdmx/DF_SDG_GLB (with time range constraints).xml")
        {
            _dataflow = GetDataflow();
            var codeListRepository = new TestCodeListRepository(_dataflow);

            _codeTranslator = new CodeTranslator(codeListRepository);
        }

        [OneTimeSetUp]
        public async Task Init()
        {
            await _codeTranslator.FillDict(_dataflow, CancellationToken);
        }

        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all", "([FREQ]='A') AND ([REPORTING_TYPE]='G') AND ([REF_AREA]='KG') AND ([OCCUPATION]='_T') AND ([CUST_BREAKDOWN]='_T') AND ([COMPOSITE_BREAKDOWN]='_T') AND ([DISABILITY_STATUS]='_T') AND ([ACTIVITY]='_T') AND ([PRODUCT]='_T') AND (PERIOD_END>='2013-01-01') AND (PERIOD_START<'2017-01-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all", "([FREQ]='A' OR [FREQ] IS NULL) AND ([REPORTING_TYPE]='G' OR [REPORTING_TYPE] IS NULL) AND ([REF_AREA]='KG' OR [REF_AREA] IS NULL) AND ([OCCUPATION]='_T' OR [OCCUPATION] IS NULL) AND ([CUST_BREAKDOWN]='_T' OR [CUST_BREAKDOWN] IS NULL) AND ([COMPOSITE_BREAKDOWN]='_T' OR [COMPOSITE_BREAKDOWN] IS NULL) AND ([DISABILITY_STATUS]='_T' OR [DISABILITY_STATUS] IS NULL) AND ([ACTIVITY]='_T' OR [ACTIVITY] IS NULL) AND ([PRODUCT]='_T' OR [PRODUCT] IS NULL) AND (PERIOD_END>='2013-01-01' OR PERIOD_END IS NULL) AND (PERIOD_START<'2017-01-01' OR PERIOD_START IS NULL)", true, null , true)]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all", "([FREQ]='A' OR [FREQ] IS NULL OR [FREQ] = '~') AND ([REPORTING_TYPE]='G' OR [REPORTING_TYPE] IS NULL OR [REPORTING_TYPE] = '~') AND ([REF_AREA]='KG' OR [REF_AREA] IS NULL OR [REF_AREA] = '~') AND ([OCCUPATION]='_T' OR [OCCUPATION] IS NULL OR [OCCUPATION] = '~') AND ([CUST_BREAKDOWN]='_T' OR [CUST_BREAKDOWN] IS NULL OR [CUST_BREAKDOWN] = '~') AND ([COMPOSITE_BREAKDOWN]='_T' OR [COMPOSITE_BREAKDOWN] IS NULL OR [COMPOSITE_BREAKDOWN] = '~') AND ([DISABILITY_STATUS]='_T' OR [DISABILITY_STATUS] IS NULL OR [DISABILITY_STATUS] = '~') AND ([ACTIVITY]='_T' OR [ACTIVITY] IS NULL OR [ACTIVITY] = '~') AND ([PRODUCT]='_T' OR [PRODUCT] IS NULL OR [PRODUCT] = '~') AND (PERIOD_END>='2013-01-01' OR PERIOD_END IS NULL OR PERIOD_END = '0001-01-01') AND (PERIOD_START<'2017-01-01' OR PERIOD_START IS NULL OR PERIOD_START = '9999-12-31')", true, null, true, true)]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014&endPeriod=2015", "([FREQ]='A') AND ([REPORTING_TYPE]='G') AND ([REF_AREA]='KG') AND ([OCCUPATION]='_T') AND ([CUST_BREAKDOWN]='_T') AND ([COMPOSITE_BREAKDOWN]='_T') AND ([DISABILITY_STATUS]='_T') AND ([ACTIVITY]='_T') AND ([PRODUCT]='_T') AND (PERIOD_END>='2014-01-01') AND (PERIOD_START<'2016-01-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014&endPeriod=2015", "([FA].[DIM_101]=1) AND ([FA].[DIM_102]=1) AND ([FA].[DIM_104]=439) AND ([FA].[DIM_110]=1) AND ([FA].[DIM_111]=1) AND ([FA].[DIM_112]=1) AND ([FA].[DIM_113]=1) AND ([FA].[DIM_114]=1) AND ([FA].[DIM_115]=1) AND ([FA].PERIOD_END>='2014-01-01') AND ([FA].PERIOD_START<'2016-01-01')", false, "FA")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014&endPeriod=2015", "([FA].[DIM_101]=1 OR [FA].[DIM_101] IS NULL OR [FA].[DIM_101] = 0) AND ([FA].[DIM_102]=1 OR [FA].[DIM_102] IS NULL OR [FA].[DIM_102] = 0) AND ([FA].[DIM_104]=439 OR [FA].[DIM_104] IS NULL OR [FA].[DIM_104] = 0) AND ([FA].[DIM_110]=1 OR [FA].[DIM_110] IS NULL OR [FA].[DIM_110] = 0) AND ([FA].[DIM_111]=1 OR [FA].[DIM_111] IS NULL OR [FA].[DIM_111] = 0) AND ([FA].[DIM_112]=1 OR [FA].[DIM_112] IS NULL OR [FA].[DIM_112] = 0) AND ([FA].[DIM_113]=1 OR [FA].[DIM_113] IS NULL OR [FA].[DIM_113] = 0) AND ([FA].[DIM_114]=1 OR [FA].[DIM_114] IS NULL OR [FA].[DIM_114] = 0) AND ([FA].[DIM_115]=1 OR [FA].[DIM_115] IS NULL OR [FA].[DIM_115] = 0) AND ([FA].PERIOD_END>='2014-01-01' OR [FA].PERIOD_END IS NULL OR [FA].PERIOD_END = '0001-01-01') AND ([FA].PERIOD_START<'2016-01-01' OR [FA].PERIOD_START IS NULL OR [FA].PERIOD_START = '9999-12-31')", false, "FA", true, true)]
        public void ValidQueries(string restQuery, string expectedSql, bool useExternalValues = true, string tableAlias = null, bool includeNulls = false, bool includeNullsAndSwitchOff = false)
        {
            var query = GetQuery(restQuery);
            var nullTreatment = includeNullsAndSwitchOff ? NullTreatment.IncludeNullsAndSwitchOff : includeNulls ? NullTreatment.IncludeNulls : NullTreatment.ExcludeNulls;
            var sql = Predicate.BuildWhereForObservationLevel(query, _dataflow, _codeTranslator,
                nullTreatment: nullTreatment, 
                useExternalValues: useExternalValues, tableAlias: tableAlias);
            
            Assert.AreEqual(expectedSql, sql);
        }

        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all", "(PERIOD_END>='2013-01-01') AND (PERIOD_START<'2017-01-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/............../all", "(PERIOD_END>='2013-01-01') AND (PERIOD_START<'2017-01-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2012", "(PERIOD_END>='2013-01-01') AND (PERIOD_START<'2017-01-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?endPeriod=2018", "(PERIOD_END>='2013-01-01') AND (PERIOD_START<'2017-01-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014", "(PERIOD_END>='2014-01-01') AND (PERIOD_START<'2017-01-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?endPeriod=2015", "(PERIOD_END>='2013-01-01') AND (PERIOD_START<'2016-01-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?endPeriod=2015-02", "(PERIOD_END>='2013-01-01') AND (PERIOD_START<'2015-03-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014&endPeriod=2015-Q2", "(PERIOD_END>='2014-01-01') AND (PERIOD_START<'2015-07-01')")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014&endPeriod=2015-Q2", "(PERIOD_END>='2014-01-01' OR PERIOD_END IS NULL OR PERIOD_END = '0001-01-01') AND (PERIOD_START<'2015-07-01' OR PERIOD_START IS NULL OR PERIOD_START = '9999-12-31')", null, true, true)]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014&endPeriod=2015-Q2", "(PERIOD_END>='2014-01-01' OR PERIOD_END IS NULL) AND (PERIOD_START<'2015-07-01' OR PERIOD_START IS NULL)", null, true)]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014&endPeriod=2015-Q2", "([Import].PERIOD_END>='2014-01-01') AND ([Import].PERIOD_START<'2015-07-01')", "Import")]
        public void TimeRange(string restQuery, string expectedSql, string tableAlias = null, bool includeNulls = false, bool includeNullsAndSwitchOff = false)
        {
            var query = GetQuery(restQuery);
            var where = new List<string>();
            var nullTreatment = includeNullsAndSwitchOff ? NullTreatment.IncludeNullsAndSwitchOff : includeNulls ? NullTreatment.IncludeNulls : NullTreatment.ExcludeNulls;

            Predicate.AddTimePredicate(
                where,
                _dataflow,
                query?.SelectionGroups?.FirstOrDefault(),
                tableAlias,
                nullTreatment
            );

            Assert.AreEqual(expectedSql, string.Join(" AND ", where));
        }

        [Test]
        public void DimensionWithoutConstraintTest()
        {
            var dim = _dataflow.Dimensions.FirstOrDefault(d => d.Constraint == null);

            Assert.IsNotNull(dim);
            Assert.IsTrue(dim.IsAllowed("test"));
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ConstraintObjTest(bool include)
        {
            var constraint = new ContentConstraintMutableCore();
            constraint.Id = "TEST";
            constraint.AgencyId = "TEST";
            constraint.Version = "1.0";
            constraint.AddName("en", "TEST");

            var c = new Constraint(constraint.ImmutableInstance, include, new[] { "A", "B", "C" }, null);

            Assert.AreEqual(include, c.IsAllowed("A"));
            Assert.AreEqual(!include, c.IsAllowed("X"));
        }

        [Test]
        public void TimeRangeMapping()
        {
            var timeDim = _dataflow.Dimensions.FirstOrDefault(x => x.Base.TimeDimension);

            Assert.IsNotNull(timeDim);
            Assert.IsNotNull(timeDim.Constraint);
            Assert.IsNotNull(timeDim.Constraint.TimeRange);
        }
    }
}
