﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Domain;
using DotStat.Test.Moq;
using Moq;
using NUnit.Framework;
using Dataflow = DotStat.Domain.Dataflow;
using DotStat.Db.Dto;
using DotStat.Db.Exception;
using DotStat.Common.Localization;
using System.Data;
using DotStat.Common.Exceptions;

namespace DotStat.Test.DataAccess.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public sealed class SqlMetadataComponentRepositoryTests : SdmxUnitTestBase
    {
        private readonly SqlMetadataComponentRepository _repository;
        private readonly Mock<SqlDotStatDb> _dotStatDbMock;

        public SqlMetadataComponentRepositoryTests()
        {
            _dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            _dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            _dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            _repository = new SqlMetadataComponentRepository(_dotStatDbMock.Object, Configuration);
        }

        [TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("sdmx/CsvV2.xml")]
        public async Task GetMetadataAttributeDbId(string file)
        {
            var dataflow = GetDataflow(file);
            dataflow.Dsd.DbId = 1;
            var dbId = 1;

            //Measures
            var compItem = new ComponentItem
            {
                DbId = dbId++,
                Id = dataflow.Dsd.PrimaryMeasure.Code,
                Type = dataflow.Dsd.PrimaryMeasure.DbType,
                DsdId = 1,
                CodelistId = dataflow.Dsd.PrimaryMeasure.Base.HasCodedRepresentation() ? dbId++ : null
            };

            SetDotStatDbMock(new List<ComponentItem> { compItem });
            var ex = Assert.ThrowsAsync<DotStatException>(
                async() => await _repository.GetMetadataAttributeDbId(dataflow.Dsd.PrimaryMeasure, dataflow.Dsd.DbId, CancellationToken));

            var expectedErrorMsg = string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotMetadataAttributeComponentType),
                    compItem.Id);
            Assert.AreEqual(expectedErrorMsg, ex.Message);

            //Dimensions
            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                compItem = new ComponentItem
                {
                    DbId = dbId++,
                    Id = dim.Code,
                    Type = dim.DbType,
                    DsdId = 1,
                    CodelistId = dim.Base.HasCodedRepresentation()? dbId++ : null,
                };

                SetDotStatDbMock(new List<ComponentItem> { compItem });
                ex = Assert.ThrowsAsync<DotStatException>(
                    async () => await _repository.GetMetadataAttributeDbId(dim, dataflow.Dsd.DbId, CancellationToken));

                expectedErrorMsg = string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotMetadataAttributeComponentType),
                        compItem.Id);
                Assert.AreEqual(expectedErrorMsg, ex.Message);
            }

            //Attributes
            foreach (var attr in dataflow.Dsd.Attributes)
            {
                compItem = new ComponentItem
                {
                    DbId = dbId++,
                    Id = attr.Code,
                    Type = attr.DbType,
                    DsdId = 1,
                    CodelistId = attr.Base.HasCodedRepresentation() ? dbId++ : null
                };

                SetDotStatDbMock(new List<ComponentItem> { compItem });
                ex = Assert.ThrowsAsync<DotStatException>(
                    async () => await _repository.GetMetadataAttributeDbId(attr, dataflow.Dsd.DbId, CancellationToken));

                expectedErrorMsg = string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotMetadataAttributeComponentType),
                        compItem.Id);
                Assert.AreEqual(expectedErrorMsg, ex.Message);
            }

            //Metadata Attributes
            var metadataAttributes = dataflow.Dsd.Msd != null ? dataflow.Dsd.Msd.MetadataAttributes : new List<MetadataAttribute>();   
            foreach (var attr in metadataAttributes)
            {
                var attrItem = new MetadataAttributeItem
                {
                    DbId = dbId++,
                    Id = attr.Code,
                    MsdId = 1,
                    Parent = dbId -1,
                    CodelistId = null
                };

                SetDotStatDbMock(new List<MetadataAttributeItem> { attrItem });
                var compDbId = await _repository.GetMetadataAttributeDbId(attr, dataflow.Dsd.DbId, CancellationToken);
                Assert.AreEqual(attrItem.DbId, compDbId);
            }
        }

        [TestCase]
        public async Task GetComponentDbIdNotInDb()
        {
            var dataflow = GetDataflow("sdmx/CsvV2.xml");
            dataflow.Dsd.DbId = 1;
            SetDotStatDbMock(new List<MetadataAttributeItem>());

            var compDbId = await _repository.GetMetadataAttributeDbId(dataflow.Dsd.Msd.MetadataAttributes[0], dataflow.Dsd.DbId, CancellationToken);
            Assert.AreEqual(-1, compDbId);

            Assert.ThrowsAsync<ArtefactNotFoundException>(async () =>
                await _repository.GetMetadataAttributeDbId(dataflow.Dsd.Msd.MetadataAttributes[0], dataflow.Dsd.DbId, CancellationToken, true));
        }

        [TestCase]
        public async Task GetComponentDsdDbIdNotInitialized()
        {
            var dataflow = GetDataflow("sdmx/CsvV2.xml");
            dataflow.Dsd.DbId = -1;
            var dbId = 1;
            var attr = dataflow.Dsd.Msd.MetadataAttributes[0];
            var attrItem = new MetadataAttributeItem
            {
                DbId = dbId++,
                Id = attr.Code,
                MsdId = 1,
                Parent = dbId - 1,
                CodelistId = null
            };

            SetDotStatDbMock(new List<MetadataAttributeItem> { attrItem });
            var compDbId = await _repository.GetMetadataAttributeDbId(attr, dataflow.Dsd.Msd.DbId, CancellationToken);
            Assert.AreEqual(-1, compDbId);
        }

        [TestCase]
        public void GetComponentUnknownComponent()
        {
            var dataflow = GetDataflow("sdmx/CsvV2.xml");

            //Dimension
            Assert.ThrowsAsync<DotStatException>(async () =>
                await _repository.GetMetadataAttributeDbId(
                    dataflow.Dsd.Dimensions[0], 1, CancellationToken));

            //Attribute
            Assert.ThrowsAsync<DotStatException>(async () =>
                await _repository.GetMetadataAttributeDbId(
                    dataflow.Dsd.Attributes[0], 1, CancellationToken));

            //Measure
            Assert.ThrowsAsync<DotStatException>(async () =>
                await _repository.GetMetadataAttributeDbId(
                    dataflow.Dsd.PrimaryMeasure, 1, CancellationToken));
            //Dsd
            Assert.ThrowsAsync<DotStatException>(async () =>
                await _repository.GetMetadataAttributeDbId(dataflow.Dsd, 1, CancellationToken));

            //Dataflow
            Assert.ThrowsAsync<DotStatException>(async () =>
                await _repository.GetMetadataAttributeDbId(dataflow, 1, CancellationToken));
        }

        [TestCase]
        public async Task GetMetadataAttributesOfMsd()
        {
            var dataflow = GetDataflow("sdmx/CsvV2.xml");
            dataflow.Dsd.Msd.DbId = 1;
            var dbId = 1;
            var mockComponents = new List<MetadataAttributeItem>();

            foreach(var attr in dataflow.Dsd.Msd.MetadataAttributes)
            {
                var attrItem = new MetadataAttributeItem
                {
                    DbId = dbId++,
                    Id = attr.Code,
                    MsdId = 1,
                    Parent = dbId - 1,
                    CodelistId = null
                };
                mockComponents.Add(attrItem);
            }

            SetDotStatDbMock(mockComponents);

            var componentsInDb = await _repository.GetMetadataAttributesOfMsd(dataflow.Dsd.Msd, CancellationToken);

            Assert.AreEqual(mockComponents.Count, componentsInDb.Count);

            var i = 0;
            foreach (var component in mockComponents)
            {
                Assert.AreEqual(component.DbId, componentsInDb[i].DbId);
                Assert.AreEqual(component.Id, componentsInDb[i].Id);
                Assert.AreEqual(component.MsdId, componentsInDb[i].MsdId);
                Assert.AreEqual(component.Parent, componentsInDb[i].Parent);
                Assert.AreEqual(component.CodelistId, componentsInDb[i].CodelistId);
                i++;
            }
        }

        [TestCase]
        public async Task GetComponentsOfDsdDbIdNotInitialized()
        {
            var dataflow = GetDataflow("sdmx/CsvV2.xml");
            dataflow.Dsd.Msd.DbId = -1;
            var dbId = 1;

            var attr = dataflow.Dsd.Msd.MetadataAttributes[0];
            var attrItem = new MetadataAttributeItem
            {
                DbId = dbId++,
                Id = attr.Code,
                MsdId = 1,
                Parent = dbId - 1,
                CodelistId = null
            };
            SetDotStatDbMock(new List<MetadataAttributeItem> { attrItem });
            var dbComponents = await _repository.GetMetadataAttributesOfMsd(dataflow.Dsd.Msd, CancellationToken);
            Assert.AreEqual(0, dbComponents.Count);
        }

        [TestCase]
        public async Task GetAllComponents()
        {
            var dbId = 1;
            var mockComponents = new List<MetadataAttributeItem>();

            var dataflows = new List<Dataflow>
            {
                GetDataflow("sdmx/CsvV2.xml"),
                GetDataflow("sdmx/CsvV2.xml")
            };

            
            foreach (var dataflow in dataflows)
            {
                dataflow.Dsd.DbId = dbId++;

                foreach (var attr in dataflow.Dsd.Msd.MetadataAttributes)
                {
                    var attrItem = new MetadataAttributeItem
                    {
                        DbId = dbId++,
                        Id = attr.Code,
                        MsdId = 1,
                        CodelistId = null,
                        Parent = dbId - 1,
                        MinOccurs = 1,
                        MaxOccurs = 100,
                        IsRepresentational = 1,
                        EnumId = 2
                    };
                    mockComponents.Add(attrItem);
                }
            }
            SetDotStatDbMock(mockComponents);

            var allMetadataComponentsInDb = await _repository.GetAllMetadataAttributeComponents(CancellationToken);

            Assert.AreEqual(mockComponents.Count, allMetadataComponentsInDb.Count);

            var i = 0;
            foreach (var component in mockComponents)
            {
                Assert.AreEqual(component.DbId, allMetadataComponentsInDb[i].DbId);
                Assert.AreEqual(component.Id, allMetadataComponentsInDb[i].Id);
                Assert.AreEqual(component.MsdId, allMetadataComponentsInDb[i].MsdId);
                Assert.AreEqual(component.CodelistId, allMetadataComponentsInDb[i].CodelistId);
                Assert.AreEqual(component.Parent, allMetadataComponentsInDb[i].Parent);
                Assert.AreEqual(component.MinOccurs, allMetadataComponentsInDb[i].MinOccurs);
                Assert.AreEqual(component.MaxOccurs, allMetadataComponentsInDb[i].MaxOccurs);
                Assert.AreEqual(component.IsRepresentational, allMetadataComponentsInDb[i].IsRepresentational);
                Assert.AreEqual(component.EnumId, allMetadataComponentsInDb[i].EnumId);

                i++;
            }
        }

        private void SetDotStatDbMock(IList<ComponentItem> components)
        {
            _dotStatDbMock.Reset();
            var componentsInDb = new List<object[]>();
            if (components != null)
            {
                for (var i = 0; i < components.Count; i++)
                {
                    componentsInDb.Add(new object[] {
                        components[i].DbId,
                        components[i].Id,
                        components[i].DsdId,
                        components[i].CodelistId,
                        components[i].AttributeAssignmentlevel.ToString(),
                        components[i].AttributeStatus.ToString(),
                        components[i].EnumId,
                        components[i].AttributeGroup,
                        components[i].MinLength,
                        components[i].MaxLength,
                        components[i].Pattern,
                        components[i].MinValue,
                        components[i].MaxValue });
                }
            } 
        }

        private void SetDotStatDbMock(IList<MetadataAttributeItem> components)
        {
            _dotStatDbMock.Reset();
            var componentsInDb = new List<object[]>();
            if (components != null)
            {
                for (var i = 0; i < components.Count; i++)
                {
                    componentsInDb.Add(new object[] {
                    components[i].DbId,
                    components[i].Id,
                    components[i].MsdId,
                    components[i].CodelistId,
                    components[i].Parent,
                    components[i].MinOccurs,
                    components[i].MaxOccurs,
                    components[i].IsRepresentational,
                    components[i].EnumId });
                }
            }

            DbDataReader dbDataReader = new TestDbDataReader(componentsInDb);

            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));
        }

    }
}