﻿using System.Data.Common;
using System.Data.SqlClient;
using CommandLine;

namespace DotStat.DbUp.Options
{
    [Verb("upgrade", HelpText = "Upgrade the specified database, e.g.: DotStat.DbUp.exe upgrade --connectionString \"Server=.; Database=MyDataDb; Trusted_connection=true\" --dataDb --loginName dataWriter --loginPwd 123456abcdef!@#$ --force")]
    public class UpgradeOption : BaseOption
    {
        private string _masterConnectionString;

        [Option("force", Required = false, Default = false, HelpText = "Forces execution of upgrade without asking user confirmation")]
        public bool Force { get; set; }

        [Option("loginName", Required = false, Default = "dataWriter", HelpText = "The name of the login")]
        public string LoginName { get; set; }

        [Option("loginPwd", Required = false, Default = "1234abcd!@#$", HelpText = "The password of the login")]
        public string LoginPwd { get; set; }

        [Option("dropDb", Required = false, Default = false, HelpText = "Drops the database before creating it")]
        public bool DropDb { get; set; }

        [Option("alterPassword", Required = false, Default = false, HelpText = "Changes the passwords of existing *loginName* login in database")]
        public bool AlterPassword { get; set; }

        [Option("ROloginName", Required = false, HelpText = "The name of the optional read-only login. When not provided, no read-only user is created.")]
        public string ROloginName { get; set; }

        [Option("ROloginPwd", Required = false, Default = "1234abcd!@#$", HelpText = "The password of the optional read-only login")]
        public string ROloginPwd { get; set; }

        public bool IsROloginNameValid() => !string.IsNullOrWhiteSpace(ROloginName);

        public string MasterConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_masterConnectionString))
                {
                    var connectionBuilder = new SqlConnectionStringBuilder(ConnectionString)
                    {
                        InitialCatalog = "master"
                    };

                    _masterConnectionString = connectionBuilder.ConnectionString;
                }

                return _masterConnectionString;
            }
        }

        [Option("withoutDbaScripts", Required = false, Default = false, HelpText = "The DBA scripts are not executed: it will not create logins / users, won't put the DB in restricted mode while running")]
        public bool WithoutDbaScripts { get; set; }

        [Option("googleProjectId", Required = false, HelpText = "Google.Cloud log projectId")]
        public string GoogleProjectId { get; set; }

        [Option("googleLogId", Required = false, HelpText = "Google.Cloud log logId")]
        public string GoogleLogId { get; set; }
    }
}