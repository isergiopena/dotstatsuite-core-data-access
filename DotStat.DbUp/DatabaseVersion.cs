﻿using System;

namespace DotStat.DbUp
{
    public static class DatabaseVersion
    {
        public static Version CommonDbVersion = new Version(3, 8);

        public static Version DataDbVersion = new Version(8, 1, 6);
    }
}
